# Simple ML

Simple ML experiments, used to get a better sense to how models work + what
hyperparameters impact what

## Experiments

### scripts.transformer\_shuffle\_experiments

#### Summary

Figure out what hyperparameters impact performance when trying to predict
shuffled data

In general, the following variables are used in comments
- N: Batch size/batch dimension
- T: Time dimension
- n: Number of heads
- D: Feature dimension


#### Notes/Worklog

- Global default hyperparameters
    - shuffled sequence length: 10
    - number of training batches: r00
    - batch size: 32
    - best possible loss: 1.28
    - learning rate: 1e-3

Default lowest seems to be roughly 1.42

Decreasing learning rate to 1e-4 and increasing iterations to 1000
    lowest loss is now 1.42

Trying with query dim 64: 1.44
trying with num layers 6: 1.43
trying with num heads 8: 1.43
trying with feed forward dim 256: 1.42
trying with both query dim and value dim 64
trying with batch size 128: 1.43

trying with custom learning rate schedule:
    1e-5 for 0-50
    1e-3 for 50-150
    1e-4 for 150-450
    1e-5 for 450-
    result: 1.43

trying with custom learning rate schedule:
    1e-5 for 0-50
    1e-3 for 50-150
    1e-4 for 150-450
    3e-3 for 450-
    result: 1.43

Realized miscalculated optimal error

### scripts.transformer\_text\_modelling

All images are from data/findings top level directory

#### Summary

Figure out some questions I had about how transformers work.

Basic Questions (questions will be referred to as Q# and answers referred to as
A#):
  - Q1: what is a typical loss that i expect?
    - plot in normal\_loss\_curve.png
  - Q2: what are typical attention matrix values?
    - generally ~87% of attention values in a half trained transformer are less
      than 0.01
      - transformer trained using text modelling
    - overall distribution plot image
      - data/findings/log\_attention\_value\_histogram.png
  - Q3: do the maximum attention values tend to dominate?
    - checked max norm -- looks like it. min attention values tend to
      contribution at most a value of 0.05 to resulting attention
  - Q4: is 0.05 per value a small change or large change for the model
    - tried training model adding uniform random 0.05 vector to value net
    - new plot: loss\_curve\_with\_random\_permutations.png
    - pretty much identical
  - Q5: is there a bug in the transformer code? why does it not work when i remove
    the mask?
    - weird, behavior is as expected when i remove mask (loss went to zero)
    - loss\_with\_wrong\_mask.pgf
  - Q6: do the statistics of the transformer (e.g. sparsity) change depending on
    training progress?
    - before training:
      - num elements < 0.01: ~0.49
      - l0 norm when keeping only small elements (< 0.01): 0.00
    - after training
      - num elements < 0.01: ~0.89
      - l0 norm when keeping only small elements (< 0.01): 0.05
    - suspect that sparsity generally increases
    - confirmed that this is true in first 10 iterations with graph
      - data/findings/small\_value\_influence\_over\_training\_period.png
  - Q7: how quickly can a normal transformer generate new values?
    - time slows roughly quadratically as expected
      - plot: random\_sampling\_brute\_force\_vs\_stateful\_timing.png
    - confirmed that using state does increase time linearly
  - Q9: are there any efficient nearest neighbor methods out there that support
    inner products + updating the tree dynamically?
    - possibly i can just use faiss + ivfpq index method for incremental
      indexing
      - ref: https://stackoverflow.com/a/49810384
  - Q8: can l2 norm be used? does it hurt performance?
    - looks like l2 norm doesn't affect too much
    - see: l2\_norm\_attention.png

High level questions
  - is it feasible to use kd-tree for language modelling?
    - is it faster?
      - A7 implies it will be
      - relatively confident about this
    - is it possible to implement? how?
      - existing library?
        - faiss looks good, but not sure how to use
      - rust extension?
        - could, but will require lots of setup + verification that updating a
          kd tree with occasional rebuilds is sufficiently fast
        - adding update feature to kd tree:
          - issue is that algorithm *must* rebuild tree as it grows
            exponentially
          - not sure how likely this is, but has possible terrible worst case
            performance
      - scipy?
        - not easily, as scipy doesnt have updating spatial algorithm
        - scipy not bad actually as you can just rebuild tree periodically
        - dont think there exists an updating spatial algorithm as of yet
    - can i train using the kd-tree?
  - how would i adapt the training method to deal with long sequences anyways?
    - can't train on whole datasets at a time
