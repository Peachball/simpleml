"""
Analyzes kd tree balance

Found that although balance is worse with random choice, balance isn't why the
algorithm is so slow in general
"""
import matplotlib.pyplot as plt
import argparse
import numpy as np
from pathlib import Path
import re


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("logs", type=Path)
    args = parser.parse_args()

    data = []
    with args.logs.open() as f:
        for l in f:
            match = re.fullmatch(r"depth (\d+) nodes (\d+)", l.strip())
            data.append({"depth": int(match.group(1)), "nodes": int(match.group(2))})
    plt.scatter([d["nodes"] for d in data], [d["depth"] for d in data])
    plt.xscale("log")
    plt.show()


if __name__ == "__main__":
    main()
