# KD Transformer

Experiment with using a transformer that uses a kd-tree for log(n) lookup to to
determine which values to pull.

## Background + Motivation

Currently sequence models are limited in context length. The best methods tend
to follow one of two approaches:

1.  Use a normal transformer, but do something funky with the attention matrix.
    E.g. Linformer [1] uses a low rank approximation, and MEGA [2] seems to use
    moving averages.
2.  Use a state space model. These are effectively elmann RNN's except using
    ideas from control theory [3] to parameterize the recurrent weights correctly.
    Current SOTA in this space is MAMBA [4] and S5 [5]

Each approach has failed to become popular for the following reasons [6]:

1.  Scaling issues
2.  Accuracy issues. Approximations just are worse than the original

### Similar work

The approach this project proposes has been investigated here [9]. They found
around a 0.2-0.3 perplexity improvement from the best no memory model. However,
they do not provide any way to update the key vectors.

- Train a decision tree to get query result [7].
- Use KNN in output prediction (?) [8]

## Things to investigate

- performance of spatial index
  - memory usage
    * much faster if we use one layer instead of the segment tree
    * time index is almost never relevant
  - construction time
    * segment tree sucks
  - query speed
    * kd tree sucks in higher dimensions (as the literature explains)
- optimal hyperparameters
  - number of random things to pull
  - k in top k values to pull
  - aux loss + divergence loss weights
- best gradient propagation method
  * in general these options require a ton of work
  - convent style -- treat attention as creation of dynamic convnet + propagate
    gradient down completely
  - store gradient at position + apply/propagate gradient when training at the
    step
    * currently doing this
- why is lstm performance so much worse? bug?

### done

- whether training can be improved by using same spatial index nearest neighbors
  across multiple iterations + prepping data to avoid dram slowdowns
  * it's certainly faster
- performance of other model types
  - normal transformer
    - roughly 1.15 log loss
  - lstm
    - roughly 2.5 log loss

## Todo

### Currently working on

- try just using kmeans/rvq
- use index v2
- verify splits in pca projections make sense

### General

- index v3
  - implement kmeans index construction

### backlog

- knowledge
  - try using purely ff network with positional embedding as baseline

- misc
  - performance analysis
    - graph 2d key + query positions to better understand shape for projection kd
      tree
      * 2d is not representative of high dimensions
    - use faiss library
      * blocked because don't trust faiss + time filtering will go well
        - experiments with kd tree indicate NO time filtering significantly slows
          things down
    - use techniques for *approximate* nn search instead of exact
      * will try inverted index via kd tree -- but hnsw in general is pretty
        complicated and is a commitment
      - inverted index
      - hnsw
  - verify splits in pca projections make sense
    * doubt this matters
  - check if using mean instead of median for splits works better
    * highly doubt this matters
  - fix edge cases around start of sequence in find nearest
  - avoid storing keys matrix on both rust side and on python side
    - maybe can return actual distance instead of just distance metric
    - not clear what memory issues there are currently
  - figure out better handling at start of sequence
    - currently am just putting zeros when there aren't enough nearest neighbors
      to be found
    - not sure how much this will matter since most of sequence is not at the
      beginning
  - figure out performance of transformer
    * it's clear that transformer is very good compared to lstm
    * even a basic transformer with 3 layers
    * don't think much investigation needed here
    - todo
      - [ ] figure out perplexity or logloss on full article
      - [ ] try speeding up estimation by passing batches of subsequences

- low value + intuitively don't feel like they'll work
  - try using sqrt n trick + storing keys directly in tree
    * sqrt n trick has been tried <2020 + has not caught on
    * am now storing keys in tree
  - try incorporating ball tree layer when relevant
    * intuitively don't see how this will split up points
  - use k means to estimate clusters + use clusters as balls in ball tree
    * so far, doing any basic calculation over the entire dataset has
      significantly reduced runtime

### Done

- potential spatial index speed/memory improvements
  - try using more than one child node at each stage of the tree
    * this does save a lot of memory + improves speed
- test preloading with previous nearest neighbors instead of recalculating
  nearest neighbors
  * does not improve speed that much
- create larger more realistic dataset (32, 64, 128 dimension, 8 head, 64 neighbors)
  * created
- use index v2
  * done
- write custom closest distance solver based off simplex algorithm
  * done
  * faster than qp, but not fast enough to justify
- initialize qp solver using point in region to query
 * this doesn't change anything
- try using positional embedding for entire dataset with normal transformer
  * this definitely improves performance of normal transformer
- use svd to figure out maximum variance direction for hyperplane split
  instead of axis aligned bounding box
  - perform svd on sample of data at each layer
- try precalculating all nearest neighbors as part of index construction step
- optimize based on total time to find nearest neighbors for all queries
- use principle component analysis to split
- pass index v2 test case (`pytest experiments`)
- finish index v2 benchmark
- aggregate key + value matrix creation parts into rust + optimize that with
  the index tree construction
  * indexv2 does this
- calculate how many nodes are actually being avoided in common queries
  * roughly 10-50%
- add benchmarking system for random data distributions on rust side
  * in `data/` -- various directories with name `qkv_output_*` contain queries,
    keys + values of network at different stages of training
- cache nn lookup for training
- figure out why divergence starts so high
  * positional embedding was not correct -- fixed bug + updated unit tests
  - write unit test for small kd transformer
    * this seems just as error prone as reimplementing the transformer
    - check gradient is correct
      * checked some cases -- in general, random gradients + recent values tend
        to mess this up. more checks can be done, but not sure how to do this
        for now
- figure out why num heads affects performance even though cpu not being used to
  max amount
  * probably because 40-50% of time is spent on indexing no matter what
- figure out how much time is spent in spatial index vs other parts of python
  code
  * around 50%
- add hardcoding for last n time inds
- kd tree optimization (use custom memory arena instead of vecs)
  * uses less memory, but still a lot
- segment tree optimization (make it more like a fenwick tree)
  * uses less memory, but still a lot
- spatial indexing trainer
- figure out if lstm/transformer performance is because of a bug
  - doesn't look like it's caused by metrics calculations at least
  - qualitatively transformer has better output
- figure out performance of lstm
  - seems to be around 2.5

## Project Structure

Directories:
- `spatial-index-rs/`: Rust module with optimized nearest neighbor lookup
  functions
  - Can build into virtual environment via `maturin develop` or `maturin develop
    --release`
- `python/`: Various scripts to test different things:
  - `generate-sequences`: one off script that generates sequence based off
    transformer
  - `kd-transformer-trainer.py`: Special script that runs KD transformer
    training cycle. Note that KD transformer requires significant preprocessing
    + has several auxiliary losses
  - `large-scale-spatial-index.py`: One off script to test spatial index speed
    from python side
  - `models.py`: Pytorch models
  - `test_spatial_index.py`: Pytest stuff for spatial index tests
  - `track-training.py`: Script to plot stats like loss per iteration of
    currently in progress training
  - `trainer.py`: Trains LSTM + Transformers
  - `visualize.py`: Plots attention matrix

## Journal

### KMeans approach

In general, finding exact closest point is too time consuming. Additionally,
majority of attention weights are composed by a large number of low influence
key/query products, even at later stages of training. Instead, we'll treat all
keys close enough together as the same + train accordingly

Existing work:
  - [15] uses kmeans in the decoder part of a mask transformer
    - not completely sure what this is, they don't use kmeans in the entire
      pipeline
### Designing kmeans cluster based approximate NN index

Rationale: Current approximate NN algorithms use many tricks, but the most
crucial one seems to be product quantization, inverted file indexes, and a
custom trick like HNSW, or complex heuristics for building k nearest neighbor
graph. As a result, centering an index around only kmeans is equivalent to
current techniques

Additionally, this strategy can allow efficient retrieval of sum of value
vectors and sum of attention.

Overall design goals
  1. retrieve nearest clusters quickly
  2. retrieve sum of values vectors with time index less than some index for each
     cluster
  3. retrieve count of values in cluster with small time index
  4. efficiently update + retrieve key + value gradients

implementation details
  1. Use residual value quantization to store clusters. Retrieve nearest
     clusters via beam search to start. Have method to extend into other
     strategies
  2. Use segment tree to store data for 2-4. Each node of segment tree should
     store the following:
    a. Sum of value vectors
    b. count of values
    c. end time index
    d. key gradient sum
    e. value gradient sum
  3. Partition key/value vector by cluster to avoid storing map of cluster to
  4. Each cluster should store some metadata:
    a. minimum time index in cluster
  5. should have reset mechanism

training code changes:
  Create setting for v3 and add special handling for these cases:
  1. retrieved key/value gradient propagation -- do not copy but instead store
     to index
  2. do not create new index when reinitializing -- reset index

things to investigate
  - best method to retrieve clusters
    - beam search parameters
    - choose first n clusters
    - hnsw
    - k nearest neighbors graph
  - distance distribution at each cluster at each layer
  - number of neighbors at each layer
  - segment tree leaf node size
  - best kmeans algorithm
    - lloyd
    - exponion [13]
    - yin yang [14]
    - gpu

### Research on what is actually used for nearest neighbors

Notes:
  - biggest gains seem to be using "product quantization" -- quantize sections
    of the vector separately [12].
  - hsnw is very cool extension of skip lists

historical algorithms
  - randomized kd trees (intro of [12])
  - hierarchal k-means (intro [12])
  - locality sensitive hashing (intro [12])

### Custom distance solver

Overall steps:
1. Find point restricted on hyperplane

**IMPLEMENTED**

It's faster than QP, but not by much. Overall, process is about 4x slower.
Likely speed can be improved to 2x slower, but issue is there doesn't seem to be
any benefit. Can't tell if continuing to optimize will yield benefits in future.

### KD tree optimization based on benchmark

Initial runs:

Note we calculate stats about result, which takes roughly 0.5-1.5s

| Index version | QKV model version | Time    |
| -             | -                 | -       |
| 1             | 0                 | 37.29   |
| 1             | 10                | 342.3   |
| 2             | 0                 | 31.56   |
| 2             | 10                | 379.897 |

Sample command output

```
$ cargo build -r --quiet 2>/dev/null && ./target/release/benchmark-queries ../data/qkv_output_long_0 --index-version 1
[4, 757224, 8]
result shape: [757224, 4, 1]
index sum: 556857245404 value sum -682725.1
result shape: [757224, 4, 1]
index sum: 573391563207 value sum -651425.9
result shape: [757224, 4, 1]
index sum: 570764044488 value sum -13901.0
Total time: 37.032
```

Notes
  - highly suspect index v2 construction is *way* faster, which is why it seems
    to be faster in smaller cases
  - later model versions significantly slow down search
  - using projections is difficult because there isn't a straightforward +
    efficient way to compute distance from point to simplex
  - using variance on axis is very efficient

Index version 2 improvements

| improvement                          | iter 0 time | iter 10 time |
| -                                    | -           | -            |
| control                              | 30          | 380          |
| pick dimension with highest variance | 18          | 165          |
| pick dimension with highest variance | 13          | 105          |

time heuristics
  - 1.0: 18
  - 0.9: 13
  - 0.8: 12.29
  - 0.7: 12.339
  - 0.6: 13.747
  - 0.5: 14.361
  - 0.4: 15.749
  - 0.3: 18.45
  - 0.2: 22.435
picking 0.75

looks like for projection strategy finding nearest point is limiting factor.
investigating wolfe's algorithm [11].


Using previous 64 nearest neighbors for top 1 search barely helps -- only up
until finding 4-8 neighbors does the cost of loading the neighbor indicies
actually justify the cost

| num neighbors | Time without prev | Time with prev |
| -             | -                 | -              |
| 2             | 15                | 14.25          |
| 4             | 21                | 17             |
| 8             | 25                | 22             |
| 16            | 37                | 30             |

### Transformer attention investigation

KD tree optimizations must depend on distribution of query and key vectors. One
avenue to consider is what tends to have high attention anyways + especially
when considering longer sequences.

A couple of implicit goals:
  - whatever training pattern we do, we want the resulting attention matrix to
    be sparse with a couple of large values

observations of training a 256 sequence, 8 layer 32 key/value/query dim
transformers
  - dot product transformers have the desired spiky attention in later layers
  - in general, first layer tends to not have strong attention activations
  - neg l2 norm tones down spiky activations
  - empirically, l2 norm squared does as good/slightly better than dot product +
    l2 norm
    - hypothesizing that this is because attention's main role is to be able to
      "choose clusters" -- the overall distribution of key vectors doesn't
      matter, just the relevant ones need to be close together
  - this [10] paper brings up an interesting point about vanishing gradients --
    using l2 norm squared/l2 norm can lead to vanishing gradients if vectors are
    very far from each other
    - not completely sure if this is valid justification because vanishing
      gradients are not based on value -- they are based on gradient at points.
      l2 norm
  - neg log norm performs ok but not as good as negative distance squared
    - hard to tell though, because the raw output values look way more
      reasonable
      - think blurry is generally good
      - other patterns are just solid grids
  - am theorizing log norm perfoms well because it allows most flexibility in
    magnitudes
    - if a bunch of points are close together, then probabilities will change
      drastically with small perturbation
    - if a bunch of points are far apart, then probabilities will change less
      with perturbation
    - hypothesis slightly supported by fact that raw magnitude for log norm
      varies between like 10-70, up to 10-700
  - think norm might've underperformed *only* because gradient was getting
    shrunk (as gradient magnitude proportional to inverse norm)
  - early layers tend to use the entire input, later layers tend to have more
    sparse spikes
    - in general, roughly 60-70% of vectors make up top 90% of sum, and this
      amount drops to 1% over course of training + layer index

Will need to test between log + l2 squared, because although l2 squared has
better performance, the attention pattern looks really suspicious

### KD tree optimization notes

For `data/qkv_output_long_0`, normal 100 nodes takes ~50s, 50 nodes takes ~35s,
25 nodes takes ~31s. Will update default to 64 nodes

### KD transformer training notes

As of writing, the current KD transformer setup does converge with auxiliary
loss + divergence losses, but is roughly 20x times slower.

Positional encoding for the entire sequence causes memorization
(unsurprisingly). Note that the formulation for positional encodings has a
irrational period, so memorization is possible regardless of denominator. In
general, the only way to avoid memorization is to have more complex data. 10k
tokens is an extraordinarily small dataset.

Overall parameters across both transformers:
  - seqlen: 8
  - querydim: 4
  - num heads: 4
  - value dim 8
  - batch size: 64
  - hidden dim: 128
  - feed forward dim: 512

transformer results for 0.1 truncated dataset (~70k tokens)
  - speed: <1s per iteration
  - end logloss: 0

transformer results for non truncated dataset (~700k tokens)
  - speed: 6s per iteration
  - end logloss: ~2.0

kdtransformer results for truncated dataset
  - speed: ??
  - end logloss: 0
  - iteration where this happened: ~50k

kdtransformer results for non truncated dataset
  - speed: ~10 minutes
  - end logloss: ?? less than 2.5

Some thoughts:
  - accuracy with this setup (small query dim, value dim + heads) is actually
    quite good -- can memorize 10% of dataset, but not full dataset
  - performance with the setup is likely limited to memory -- cpu is limiting
    factor if accidentally compiled in debug mode or for later stages of
    training

### Performance analysis

Deeper dive into why performance is so bad

Some interesting numbers:
- 0 nearest neighbors, 128 random, 128 sequence: 5it/s
- 1 nearest neighbors, 128 random, 128 sequence: 2-3it/s
- 0 nearest neighbors, 0 random, 128 sequence: 13it/s
- 0 nearest neighbors, 128 random, 0 sequence: 8it/s

Significant amount of time spent on memory access.

Added stopwatch code based on 1 nearest neighbor, 64 random + 64 sequence.
Time is spent roughly like the following:
- 50% of time spent on kd tree
- 30-40% spent on value indexing
- 10% of time spent on key indexing

### Training kd transformer

Concerns + Issues:
 - there are a lot of sussy gradient manipulations going on, making bugs likely
 - kd tree is not very fast, esp on high dimensional input or when there are a
   lot of neighbors to be found
 - after epoch, loss shoots up (as expected) making me concerned about stability
   of qkv values/moving too much
 - gpu not really utilized even if I don't use kd tree. memory access + loading
   memory into gpu dominates

### Memory usage issues

Spatial index takes up way more memory than expected. One layer for 1M tokens
takes up roughly 0.5GB, and spatial index on top of that takes 2GB more (using
1000 wide segment tree + 16 items per leaf on kd tree).

Some rough numbers and configurations:

| segment tree leaf size | kd tree leaf size | memory usage (mb)  |
| ---------------------- | ----------------- | ------------------ |
| 100                    | 0                 | 6000               |
| 1000                   | 0                 | 4500               |
| 10000                  | 0                 | 3500               |
| 100                    | 10                | 3300               |
| 100                    | 100               | 1700               |

After updating kd tree to use memory arena for index vecs

| segment tree leaf size | kd tree leaf size | memory usage (mb)  |
| ---------------------- | ----------------- | ------------------ |
| 100                    | 0                 | 5000               |
| 100                    | 10                | 3100               |
| 100                    | 100               | 1700               |

After updating segment tree to use fenwick structure

| segment tree leaf size | kd tree leaf size | memory usage (mb)  |
| ---------------------- | ----------------- | ------------------ |
| 100                    | 0                 | 3000               |
| 100                    | 10                | 2000               |
| 100                    | 100               | 1300               |

The algorithm is nonetheless very slow. For the following parameters:
| parameter name  | value |
| -               | -     |
| heads           | 4     |
| sequence length | 10^6  |
| query dimension | 32    |
| batch size      | 64    |
| num neighbors   | 32    |

It takes 30 seconds.

Increasing the number of nodes at each kd leaf speeds it up to 20 seconds.

Using a query dimension of 4 increases time to sub seconds.

Based on the tests, will set defaults as follows

| segment tree leaf size | kd tree leaf size | query dim | num heads |
| -                      | -                 | -         | -         |
| 100                    | 100               | 4         | 32        |

### Q&A for models

**Q: What does attention matrix typically look like? Are there patterns at
all?**

There are absolutely patterns. Many layers don't have patterns, but there are a
couple high level shapes

1.  Band down diagonal
2.  Stripes going down
3.  Random spots in past

See figures under "figures/1-4LayerTransformer-layer[n]-attention.pdf" for
example graphs

**Q: How does an LSTM compare to Transformer?**

With roughly the same training time, (~5 iterations per second), the LSTM is
much slower to train + has much higher loss (~2 vs ~1 log loss). Part of the
reason is because LSTMs have much more plateaus before drops in training,
whereas transformer loss decreases are much more gradual

**Q: How does L2 norm for attention differ from inner product?**

- Around 5x slower
- Accuracy + attention shows similar patterns

## Brainstorming

### Spatial index

Overall: List of tree of trees. Outermost tree contains segment tree, where each node
stores a kd tree with points. More explicitly, the hierarchy is as follows:

- list: Contains structure for each value head
- segment tree: Performs range search
- KD tree: Does the actual nearest neighbor lookup

Additionally, all tree information will refer to a master array

The intended supported operations are:

- find n nearest
  - arguments:
    - query: Ndarray with shape (batch dim, head dim, key dim)
    - `num_neighbors`: Number of nearest neighbors to find
  - returns ndarray with shape (batch, head, ey,

### KD transformer workflow

training

1.  prepare data via random timestamps
2.  Initialize spatial index for each attention layer. Repeat the following steps
    a. for each layer, compute queries, keys, and values for everything
    b. Multiply queries with relevant key + value pair based on spatial index
    c. normalize attention
3.  go through gradient descent loop
    a. compute log loss for batches of time indexes
    b. back propagate treating retrieved keys as inputs
    c. accumulate gradient of keys wrt log loss

generation

1.  initialize mutable kd trees at each layer
2.  for each timestamp, predict by doing the following:
    a. compute qkv
    b. add key to mutable kd tree, store qkv into underlying buffer
    c. use mutable kd tree for lookup based on query
    d. rebalance kd tree if it makes sense to do so. need to investigate
    heuristics

generation logic worst case will still be O(n^2) like transformer, but i'm
hoping for the following benefits with this setup:

1.  Training + generation consistently make use of long term dependencies in
    the same way
2.  On average, generation might be faster since points might tend to
    distribute more evenly than worst case

## References

[1]: https://arxiv.org/abs/2006.04768
[2]: https://arxiv.org/pdf/2209.10655v3.pdf
[3]: https://en.wikipedia.org/wiki/State-space_representation
[4]: https://openreview.net/forum?id=AL1fq05o7H
[5]: https://arxiv.org/pdf/2208.04933v3.pdf
[6]: https://arxiv.org/pdf/2202.10447.pdf
[7]: https://arxiv.org/pdf/2208.09015.pdf
[8]: https://arxiv.org/pdf/1911.00172.pdf
[9]: https://arxiv.org/abs/2203.08913
[10]: https://arxiv.org/pdf/2310.18805.pdf
[11]: http://essay.utwente.nl/81914/1/Petrov_BA_EEMCS.pdf
[12]: https://inria.hal.science/inria-00514462v2/document
[13]: https://link.springer.com/chapter/10.1007/978-3-030-44584-3_8
[14]: http://proceedings.mlr.press/v37/ding15.pdf
[15]: https://arxiv.org/pdf/2207.04044.pdf
