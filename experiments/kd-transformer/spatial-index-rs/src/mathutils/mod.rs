#[cfg(test)]
mod test;

use ndarray::{s, Array1, ArrayView1, ArrayView2, Axis, CowArray};
use ndarray_linalg::{norm::Norm, TruncatedOrder, TruncatedSvd};
use ndarray_rand::{RandomExt, SamplingStrategy};
use osqp::{CscMatrix, Problem, Settings, Status};
use rand::Rng;

use crate::squared_l2_norm;

/// Computes effectively the first vector for PCA -- the vector on which the
/// projected data will have the most variance
///
/// Runs LOBPCG which computes eigenvectors of X^T @ X
///
/// Arguments:
///   X (n, d)
///   sample_size: number of samples to randomly choose from X
pub fn compute_projection_v1(
  x: ArrayView2<f32>,
  sample_size: usize,
  rng: &mut impl Rng,
) -> Array1<f32> {
  let x_sample = if sample_size < x.shape()[0] {
    CowArray::from(x.sample_axis_using(
      Axis(0),
      sample_size,
      SamplingStrategy::WithoutReplacement,
      rng,
    ))
  } else {
    CowArray::from(x)
  };
  let x_mean = x_sample.mean_axis(Axis(0)).unwrap();
  let normalized_x = &x_sample
    - &x_mean
      .broadcast((x_sample.shape()[0], x_mean.shape()[0]))
      .unwrap();
  let result = TruncatedSvd::new(normalized_x, TruncatedOrder::Largest)
    .decompose(1)
    .unwrap();
  let (_, _, vectors) = result.values_vectors();

  vectors.slice(s![0, ..]).to_owned()
}

/// Uses quadratic programming to compute the closest squared distance from a
/// point to a set of projection splits
///
/// Arguments
///   projections (n, d)
///   pivots (n,)
///   less_than (n,)
///   query (d,)
///   previous_problem (d,): used for osqp warmup start
pub fn closest_point_qp(
  projections: ArrayView2<f32>,
  pivots: ArrayView1<f32>,
  less_than: &[bool],
  query: ArrayView1<f32>,
  previous_problem: Option<Vec<f64>>,
) -> (f32, Vec<f64>) {
  let query_dim = query.shape()[0];
  let batch_size = projections.shape()[0];
  debug_assert_eq!(projections.shape()[1], query_dim);
  let u: Vec<_> = (0..batch_size)
    .map(|i| {
      if less_than[i] {
        (pivots[i] - projections.slice(s![i, ..]).dot(&query)) as f64
      } else {
        1000.0
      }
    })
    .collect();
  let l: Vec<_> = (0..batch_size)
    .map(|i| {
      if !less_than[i] {
        (pivots[i] - projections.slice(s![i, ..]).dot(&query)) as f64
      } else {
        -1000.0
      }
    })
    .collect();
  let a = CscMatrix::from_row_iter_dense(
    batch_size,
    query_dim,
    (0..query_dim * batch_size).map(|i| {
      let row = i / query_dim;
      let col = i % query_dim;
      projections[[row, col]] as f64
    }),
  );
  let p = CscMatrix::from_column_iter_dense(
    query_dim,
    query_dim,
    (0..query_dim * query_dim).map(|i| if i % (query_dim + 1) == 0 { 1.0 } else { 0.0 }),
  )
  .into_upper_tri();
  let q = vec![0.0; query_dim];
  let settings = Settings::default().max_iter(25).verbose(false);
  let mut solver = Problem::new(&p, &q, &a, &l, &u, &settings).unwrap();
  if let Some(vec) = previous_problem {
    solver.warm_start_x(&vec);
  }

  let result = solver.solve();
  if let Status::Solved(solution)
  | Status::MaxIterationsReached(solution)
  | Status::SolvedInaccurate(solution) = result
  {
    (2.0 * solution.obj_val() as f32, solution.x().to_vec())
  } else {
    panic!("unable to solve qp");
  }
}

/// Uses custom interior point method to find closest squared distance
///
/// Overall steps
///   1. Figure out which hyperplanes are restricting me
///   2. Find direction torwards q subject to restricting hyperplanes via
///      gram-schmidt
///   3. solve quadratic to determine closest point to q on hyperplane set +
///      update min value
///   4. check if any hyperplanes blocking path to closest point
///
/// Pray there are no floating point issues
///
/// Arguments
///   projections (n, d)
///   pivots (n,)
///   less_than (n,)
///   query (d,)
///   starting_point (d,) a point satisfying the pivot constraints
pub fn closest_point_custom_linalg(
  projections: ArrayView2<f32>,
  pivots: ArrayView1<f32>,
  less_than: &[bool],
  query: ArrayView1<f32>,
  mut starting_point: Array1<f32>,
) -> f32 {
  let mut max_distance = 0f32;
  let planes_excluding_query: Vec<_> = (0..projections.shape()[0])
    .filter(|&i| (projections.slice(s![i, ..]).dot(&query) < pivots[i]) != less_than[i])
    .collect();
  loop {
    let restricting_hyperplane_inds: Vec<_> = planes_excluding_query
      .iter()
      .filter(|&i| {
        float_eq(
          projections.slice(s![*i, ..]).dot(&starting_point),
          pivots[*i],
        )
      })
      .collect();
    let mut direction_to_query = &query - &starting_point;
    for &i in &restricting_hyperplane_inds {
      let proj_vec = projections.slice(s![*i, ..]);
      let squared_norm = proj_vec.iter().map(|v| v * v).sum::<f32>();
      direction_to_query -= &(direction_to_query.dot(&proj_vec) * &proj_vec / squared_norm);
    }
    if direction_to_query.iter().all(|&v| float_eq(v, 0.0)) {
      max_distance = max_distance.max(squared_l2_norm(starting_point.view(), query));
      break;
    }
    let mut optimal_coefficient = -(&starting_point - &query).dot(&direction_to_query)
      / direction_to_query.iter().map(|v| v * v).sum::<f32>();
    let initial_new_point = &starting_point + optimal_coefficient * &direction_to_query;
    max_distance = max_distance.max(squared_l2_norm(initial_new_point.view(), query));

    for i in &planes_excluding_query {
      let proj_vec = projections.slice(s![*i, ..]);
      let proj_dot_direction = direction_to_query.dot(&proj_vec);
      if float_eq(proj_dot_direction, 0.0) {
        continue;
      }
      let intersection_coeff = (pivots[*i] - starting_point.dot(&proj_vec)) / proj_dot_direction;
      if optimal_coefficient > intersection_coeff {
        optimal_coefficient = intersection_coeff;
      }
    }

    let new_point = &starting_point + optimal_coefficient * &direction_to_query;
    if optimal_coefficient < 1e-3 {
      break;
    }
    starting_point = new_point;
  }

  max_distance
}

fn float_eq(a: f32, b: f32) -> bool {
  (a - b).abs() < 1e-5
}
