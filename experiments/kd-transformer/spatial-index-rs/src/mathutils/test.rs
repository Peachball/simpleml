use numpy::array;

use crate::mathutils::closest_point_custom_linalg;

use super::closest_point_qp;

#[test]
fn qp_solver_works_medium_example() {
  let projections = array![
    [0.979197, -0.20291199],
    [-0.17168793, -0.9851514],
    [0.9882383, -0.1529223],
    [-0.14468664, -0.98947763],
    [0.3551256, -0.9348186],
    [-0.8468967, -0.5317576]
  ];
  let pivots = array![
    -0.22931612,
    -0.78376997,
    -0.8685908,
    -1.5282501,
    -1.4525378,
    -0.35148144
  ];
  let less_than = [true, true, false, false, false, true];
  let query = array![-0.3820023, 2.552424];
  let squared_distance = closest_point_qp(
    projections.view(),
    pivots.view(),
    &less_than,
    query.view(),
    None,
  )
  .0;
  dbg!(squared_distance);
  assert!(squared_distance.sqrt() > 1.);
}

#[test]
fn qp_solver_works_small_example() {
  let projections = array![
    [1.4142135623730951, 1.4142135623730951],
    [1.4142135623730951, -1.4142135623730951],
    [1.4142135623730951, 1.4142135623730951],
    [1.4142135623730951, -1.4142135623730951]
  ];
  let pivots = array![
    0.0,
    5.656854249492381,
    -2.8284271247461903,
    2.8284271247461903
  ];
  let less_than = [true, true, false, false];
  let query = array![1.0, 1.0];
  let squared_distance = closest_point_qp(
    projections.view(),
    pivots.view(),
    &less_than,
    query.view(),
    None,
  )
  .0;
  assert!((squared_distance.sqrt() - 2.0).abs() < 1e-3);
}

#[test]
fn qp_warm_start() {
  let projections = array![
    [1.4142135623730951, 1.4142135623730951],
    [1.4142135623730951, -1.4142135623730951],
    [1.4142135623730951, 1.4142135623730951],
    [1.4142135623730951, -1.4142135623730951]
  ];
  let pivots = array![
    0.0,
    5.656854249492381,
    -2.8284271247461903,
    2.8284271247461903
  ];
  let less_than = [true, true, false, false];
  let query = array![1.0, 1.0];
  let (dist, prob) = closest_point_qp(
    projections.view(),
    pivots.view(),
    &less_than,
    query.view(),
    None,
  );

  let projections = array![
    [0.979197, -0.20291199],
    [-0.17168793, -0.9851514],
    [0.9882383, -0.1529223],
    [-0.14468664, -0.98947763],
    [0.3551256, -0.9348186],
    [-0.8468967, -0.5317576]
  ];
  let pivots = array![
    -0.22931612,
    -0.78376997,
    -0.8685908,
    -1.5282501,
    -1.4525378,
    -0.35148144
  ];
  let less_than = [true, true, false, false, false, true];
  let query = array![-0.3820023, 2.552424];
  let squared_distance = closest_point_qp(
    projections.view(),
    pivots.view(),
    &less_than,
    query.view(),
    Some(prob),
  );
}

#[test]
fn custom_linalg_small_example() {
  let projections = array![
    [1.4142135623730951, 1.4142135623730951],
    [1.4142135623730951, -1.4142135623730951],
    [1.4142135623730951, 1.4142135623730951],
    [1.4142135623730951, -1.4142135623730951]
  ];
  let pivots = array![
    0.0,
    5.656854249492381,
    -2.8284271247461903,
    2.8284271247461903
  ];
  let less_than = [true, true, false, false];
  let query = array![1.0, 1.0];
  let squared_distance = closest_point_custom_linalg(
    projections.view(),
    pivots.view(),
    &less_than,
    query.view(),
    array![1.5, -2.0],
  );
  assert!((squared_distance.sqrt() - 2.0).abs() < 1e-3);
}
