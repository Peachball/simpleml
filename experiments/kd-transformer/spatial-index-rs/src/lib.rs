pub mod index_v2;
pub mod index_v3;
pub mod mathutils;
pub mod kmeans;

/// Parent module for spatial indicies
///
/// Contains utilties for basic spatial indicies, but future indicies should be
/// placed in their own directory
#[cfg(test)]
mod test;
use index_v2::{SpatialIndexConfigV2, SpatialIndexV2, SplitStrategyV2};
use index_v3::SpatialIndexV3;

use std::collections::BinaryHeap;

use ndarray::{
  parallel::prelude::*, s, Array1, Array2, Array3, Array4, ArrayView1, ArrayView2, ArrayView3,
  ArrayViewMut1,
};
use numpy::{
  ndarray::Axis, IntoPyArray, PyArray3, PyArray4, PyReadonlyArray1, PyReadonlyArray3,
  PyReadonlyArray4, PyReadwriteArray3,
};
use ordered_float::NotNan;
use pyo3::prelude::*;
use rand::{seq::SliceRandom, thread_rng, Rng};

const ROOT_NODE_IND: usize = 0;

pub fn squared_l2_norm(v1: ArrayView1<f32>, v2: ArrayView1<f32>) -> f32 {
  let diff = &v1 - &v2;
  diff.dot(&diff)
}

fn square(v: f32) -> f32 {
  v * v
}

fn min_squared_distance_to_bounds(
  lower_bound: ArrayView1<f32>,
  upper_bound: ArrayView1<f32>,
  point: ArrayView1<f32>,
) -> f32 {
  let mut total_distance_squared = 0f32;
  for i in 0..lower_bound.shape()[0] {
    if point[i] >= lower_bound[i] && point[i] <= upper_bound[i] {
      continue;
    }
    if point[i] > upper_bound[i] {
      total_distance_squared += square(upper_bound[i] - point[i]);
    } else {
      total_distance_squared += square(lower_bound[i] - point[i]);
    }
  }
  total_distance_squared
}

#[derive(Debug)]
struct NearestNeighborResult {
  heap: BinaryHeap<(NotNan<f32>, usize)>,
  num_neighbors: usize,
  nodes_checked: usize,
}

impl NearestNeighborResult {
  fn new(num_neighbors: usize) -> Self {
    Self {
      heap: BinaryHeap::new(),
      num_neighbors,
      nodes_checked: 0,
    }
  }

  fn try_add(&mut self, distance: f32, index: usize) {
    self.nodes_checked += 1;
    if distance < self.max_distance() {
      self.heap.push((NotNan::new(distance).unwrap(), index));
      while self.heap.len() > self.num_neighbors {
        self.heap.pop();
      }
    }
  }

  fn max_distance(&self) -> f32 {
    if self.heap.len() < self.num_neighbors || self.heap.is_empty() {
      f32::INFINITY
    } else {
      self.heap.peek().map(|v| *v.0).unwrap()
    }
  }
}

#[derive(Debug)]
struct KdTree {
  nodes: Vec<KdTreeNode>,
  ind_buffer: Vec<usize>,
}

impl KdTree {
  /// Arguments
  ///   keys (time, key dim)
  fn new(
    keys: ArrayView2<f32>,
    lower_bound: usize,
    upper_bound: usize,
    config: &SpatialIndexConfig,
  ) -> Self {
    let mut result = Self {
      nodes: Vec::new(),
      ind_buffer: Vec::new(),
    };
    result.initialize(
      keys,
      0,
      &(lower_bound..upper_bound).collect::<Vec<_>>(),
      config,
    );
    result
  }

  fn initialize(
    &mut self,
    keys: ArrayView2<f32>,
    split_dim: usize,
    indicies: &[usize],
    config: &SpatialIndexConfig,
  ) -> usize {
    let node_ind = self.nodes.len();
    self.nodes.push(KdTreeNode {
      split_dim,
      key_ind: usize::MAX,
      inds_start: 0,
      inds_size: 0,
      left: None,
      right: None,
    });
    if indicies.len() <= 1.max(config.min_kd_tree_nodes) {
      self.nodes[node_ind].inds_start = self.ind_buffer.len();
      self.nodes[node_ind].inds_size = indicies.len();
      for ind in indicies {
        self.ind_buffer.push(*ind);
      }
      return node_ind;
    }
    // Choose pivot
    let pivot_ind = match config.pivot_selection_strategy {
      PivotSelectionStrategy::Random => *indicies.choose(&mut thread_rng()).unwrap(),
      PivotSelectionStrategy::Exact => {
        let mut new_indicies = indicies.to_vec();
        new_indicies.sort_by_key(|i| NotNan::new(keys[[*i, split_dim]]).unwrap());
        new_indicies[new_indicies.len() / 2]
      }
    };
    self.nodes[node_ind].key_ind = pivot_ind;

    // Split indicies according to pivot
    let mut left_indicies = Vec::new();
    let mut right_indicies = Vec::new();
    for ind in indicies.iter() {
      if *ind == pivot_ind {
        continue;
      }
      if keys[[*ind, split_dim]] < keys[[pivot_ind, split_dim]] {
        left_indicies.push(*ind);
      } else {
        right_indicies.push(*ind);
      }
    }
    let next_split_dim = (split_dim + 1) % keys.shape()[1];
    if !left_indicies.is_empty() {
      self.nodes[node_ind].left =
        Some(self.initialize(keys, next_split_dim, &left_indicies, config));
    }
    if !right_indicies.is_empty() {
      self.nodes[node_ind].right =
        Some(self.initialize(keys, next_split_dim, &right_indicies, config));
    }

    node_ind
  }

  fn find_n_nearest(
    &self,
    keys: ArrayView2<f32>,
    query: ArrayView1<f32>,
    num_neighbors: usize,
    output: &mut NearestNeighborResult,
  ) {
    self.find_n_nearest_helper(
      keys,
      query,
      num_neighbors,
      ROOT_NODE_IND,
      &mut Array1::from_elem((query.shape()[0],), f32::NEG_INFINITY),
      &mut Array1::from_elem((query.shape()[0],), f32::INFINITY),
      output,
    );
  }

  fn find_n_nearest_helper(
    &self,
    keys: ArrayView2<f32>,
    query: ArrayView1<f32>,
    num_neighbors: usize,
    node_ind: usize,
    lower_bounds: &mut Array1<f32>,
    upper_bounds: &mut Array1<f32>,
    output: &mut NearestNeighborResult,
  ) {
    if output.max_distance()
      < min_squared_distance_to_bounds(lower_bounds.view(), upper_bounds.view(), query)
    {
      return;
    }

    let node = &self.nodes[node_ind];
    if node.inds_size != 0 {
      for ind_i in node.inds_start..node.inds_start + node.inds_size {
        let ind = self.ind_buffer[ind_i];
        let current_distance_squared = squared_l2_norm(keys.slice(s![ind, ..]), query);
        output.try_add(current_distance_squared, ind);
      }
      return;
    }

    let current_distance_squared = squared_l2_norm(keys.slice(s![node.key_ind, ..]), query);
    output.try_add(current_distance_squared, node.key_ind);

    let pivot_value = keys[[node.key_ind, node.split_dim]];

    let prev_upper_bound = upper_bounds[node.split_dim];
    let prev_lower_bound = lower_bounds[node.split_dim];

    let should_go_left = query[node.split_dim] < pivot_value;

    // If condition is very lengthy because too many parameters are being passed
    // to recursive call.
    //
    // Logic is just that we either check the left branch then right branch, or
    // vice versa
    if should_go_left {
      if let Some(left_ind) = node.left {
        upper_bounds[node.split_dim] = pivot_value;
        self.find_n_nearest_helper(
          keys,
          query,
          num_neighbors,
          left_ind,
          lower_bounds,
          upper_bounds,
          output,
        );
        upper_bounds[node.split_dim] = prev_upper_bound;
      }
      if let Some(right_ind) = node.right {
        lower_bounds[node.split_dim] = pivot_value;
        self.find_n_nearest_helper(
          keys,
          query,
          num_neighbors,
          right_ind,
          lower_bounds,
          upper_bounds,
          output,
        );
        lower_bounds[node.split_dim] = prev_lower_bound;
      }
    } else {
      if let Some(right_ind) = node.right {
        lower_bounds[node.split_dim] = pivot_value;
        self.find_n_nearest_helper(
          keys,
          query,
          num_neighbors,
          right_ind,
          lower_bounds,
          upper_bounds,
          output,
        );
        lower_bounds[node.split_dim] = prev_lower_bound;
      }
      if let Some(left_ind) = node.left {
        upper_bounds[node.split_dim] = pivot_value;
        self.find_n_nearest_helper(
          keys,
          query,
          num_neighbors,
          left_ind,
          lower_bounds,
          upper_bounds,
          output,
        );
        upper_bounds[node.split_dim] = prev_upper_bound;
      }
    }
  }

  fn _depth(&self, node_ind: usize) -> usize {
    let node = &self.nodes[node_ind];
    let mut result = 1;
    if let Some(ind) = node.left {
      result = result.max(self._depth(ind) + 1);
    }
    if let Some(ind) = node.right {
      result = result.max(self._depth(ind) + 1);
    }
    result
  }
}

#[derive(Debug)]
struct KdTreeNode {
  /// Which dimension to partition the points into
  split_dim: usize,

  /// Index to the current key
  key_ind: usize,

  /// Group of inds. If this is non empty, then left + right should have no
  /// subnodes
  inds_start: usize,
  inds_size: usize,

  /// Left indicates lower leaf node with <
  /// Note that it's possible for one subtree to be empty with the subtree with
  /// nodes (unlike segment trees)
  left: Option<usize>,
  right: Option<usize>,
}

struct SegmentTree {
  /// Root node is 0
  nodes: Vec<SegmentTreeNode>,
}

impl SegmentTree {
  /// Arguments:
  ///   keys (time, key dim)
  pub fn new(keys: ArrayView2<f32>, config: &SpatialIndexConfig) -> Self {
    let mut result = Self { nodes: Vec::new() };
    result.initialize(keys, 0, keys.shape()[0], config, false);

    result
  }

  fn initialize(
    &mut self,
    keys: ArrayView2<f32>,
    lower_time_bound: usize,
    upper_time_bound: usize,
    config: &SpatialIndexConfig,
    create_kd_tree: bool,
  ) -> usize {
    let node_ind = self.nodes.len();
    self.nodes.push(SegmentTreeNode {
      lower_time_bound,
      upper_time_bound,
      left: None,
      right: None,
      tree: if create_kd_tree {
        Some(KdTree::new(
          keys,
          lower_time_bound,
          upper_time_bound,
          config,
        ))
      } else {
        None
      },
    });
    if upper_time_bound - lower_time_bound <= 1.max(config.min_segment_tree_range) {
      return node_ind;
    }
    let middle_time_bound = (upper_time_bound + lower_time_bound) / 2;
    let left_ind = self.initialize(keys, lower_time_bound, middle_time_bound, config, true);
    let right_ind = self.initialize(keys, middle_time_bound, upper_time_bound, config, false);
    self.nodes[node_ind].left = Some(left_ind);
    self.nodes[node_ind].right = Some(right_ind);
    node_ind
  }

  fn find_n_nearest(
    &self,
    keys: ArrayView2<f32>,
    query: ArrayView1<f32>,
    time_ind: i64,
    num_neighbors: usize,
    mut output: ArrayViewMut1<i64>,
  ) {
    let mut best_found = NearestNeighborResult::new(num_neighbors);
    self.find_n_nearest_helper(
      keys,
      query,
      ROOT_NODE_IND,
      time_ind as usize,
      num_neighbors,
      &mut best_found,
    );
    let _fraction_checked = best_found.nodes_checked as f64 / time_ind as f64;
    for (i, (_, ind)) in best_found.heap.iter().enumerate() {
      output[i] = *ind as i64;
    }
  }

  fn find_n_nearest_helper(
    &self,
    keys: ArrayView2<f32>,
    query: ArrayView1<f32>,
    node_ind: usize,
    time_ind: usize,
    num_neighbors: usize,
    output: &mut NearestNeighborResult,
  ) {
    let node = &self.nodes[node_ind];
    if node.lower_time_bound > time_ind {
      return;
    }
    // reason why we need to compare to time_ind + 1 is because attention can
    // attend to current position (which is represented by time_ind)
    if node.upper_time_bound <= time_ind + 1 {
      if let Some(actual_tree) = &node.tree {
        actual_tree.find_n_nearest(keys, query, num_neighbors, output);
        return;
      }
    }
    if node.left.is_none() && node.right.is_none() {
      // brute force update
      for i in node.lower_time_bound..time_ind + 1 {
        let squared_distance = squared_l2_norm(keys.slice(s![i, ..]), query);
        output.try_add(squared_distance, i);
      }
      return;
    }
    self.find_n_nearest_helper(
      keys,
      query,
      node.left.unwrap(),
      time_ind,
      num_neighbors,
      output,
    );
    self.find_n_nearest_helper(
      keys,
      query,
      node.right.unwrap(),
      time_ind,
      num_neighbors,
      output,
    );
  }

  fn _depth(&self, node_ind: usize) -> usize {
    let node = &self.nodes[node_ind];
    match (node.left, node.right) {
      (Some(left_ind), Some(right_ind)) => self._depth(left_ind).max(self._depth(right_ind)) + 1,
      _ => 1,
    }
  }
}

struct SegmentTreeNode {
  /// inculsive
  /// Represents earliest time idx stored
  lower_time_bound: usize,

  /// exclusive
  /// Represents latest time idx stored
  upper_time_bound: usize,

  tree: Option<KdTree>,

  /// Leaf node if both of these are None
  /// These should either both be None or both with Some
  left: Option<usize>,
  right: Option<usize>,
}

#[pyclass]
#[derive(Clone, Copy)]
pub enum PivotSelectionStrategy {
  Random,
  Exact,
}

#[pyclass]
#[derive(Clone)]
pub struct SpatialIndexConfig {
  #[pyo3(get, set)]
  pub min_segment_tree_range: usize,

  #[pyo3(get, set)]
  pub min_kd_tree_nodes: usize,

  #[pyo3(get, set)]
  pub pivot_selection_strategy: PivotSelectionStrategy,
}

impl Default for SpatialIndexConfig {
  fn default() -> Self {
    Self {
      min_segment_tree_range: 64,
      min_kd_tree_nodes: 64,
      pivot_selection_strategy: PivotSelectionStrategy::Random,
    }
  }
}

#[pymethods]
impl SpatialIndexConfig {
  #[new]
  fn py_new() -> Self {
    Self::default()
  }
}

#[pyclass]
pub struct SpatialIndex {
  /// Contains index for each head
  head_indexes: Vec<SegmentTree>,

  /// Array of (number of heads, time index, key dim)
  key_array: Array3<f32>,
}

impl SpatialIndex {
  pub fn from_array(keys: Array3<f32>, config: SpatialIndexConfig) -> Self {
    let mut head_indexes = Vec::new();
    for head_ind in 0..keys.shape()[0] {
      // construct segment tree
      let segment_tree = SegmentTree::new(keys.slice(s![head_ind, .., ..]), &config);
      head_indexes.push(segment_tree);
    }
    Self {
      key_array: keys,
      head_indexes,
    }
  }

  pub fn find_n_nearest<'py>(
    &self,
    query: ArrayView3<f32>,
    time_inds: ArrayView1<i64>,
    num_neighbors: usize,
    num_random: usize,
    num_recent: usize,
  ) -> Array3<i64> {
    let batch_size = query.shape()[0];
    let num_heads = query.shape()[1];
    let total_indicies = num_neighbors + num_random + num_recent;

    let mut result = Array2::<i64>::zeros((batch_size * num_heads, total_indicies));
    result
      .axis_iter_mut(Axis(0))
      .into_par_iter()
      .enumerate()
      .for_each_init(
        || thread_rng(),
        |rng, (global_ind, mut result_slice)| {
          let batch_ind = global_ind / num_heads;
          let head_ind = global_ind % num_heads;
          let index = &self.head_indexes[head_ind];
          if num_neighbors != 0 {
            index.find_n_nearest(
              self.key_array.slice(s![head_ind, .., ..]),
              query.slice(s![batch_ind, head_ind, ..]),
              time_inds[batch_ind],
              num_neighbors,
              result_slice.slice_mut(s![0..num_neighbors]),
            );
          }
          let mut i = num_neighbors;
          let current_time_ind = time_inds[batch_ind];
          for _ in 0..num_random {
            result_slice[i] = rng.gen_range(0..=current_time_ind);
            i += 1;
          }
          for j in 0..(num_recent as i64) {
            result_slice[i] = if current_time_ind == 0 {
              0
            } else if j > current_time_ind {
              current_time_ind - ((j - 1) % current_time_ind)
            } else {
              current_time_ind - j
            };
            i += 1;
          }
        },
      );
    result
      .into_shape((batch_size, num_heads, total_indicies))
      .unwrap()
  }
}

#[pymethods]
impl SpatialIndex {
  /// Arguments:
  ///   keys (num heads, time, key dim)
  #[staticmethod]
  #[pyo3(name = "from_array")]
  fn py_from_array<'py>(
    keys: PyReadonlyArray3<'py, f32>,
    opt_config: Option<SpatialIndexConfig>,
  ) -> Self {
    let keys_owned = keys.to_owned_array();
    let config = opt_config.unwrap_or(SpatialIndexConfig::default());
    Self::from_array(keys_owned, config)
  }

  /// Arguments:
  ///   query (batch, heads, key dim)
  ///   time inds (batch,)
  ///   num_neighbors: Number of nearest neighbors to find
  ///   num_random: Number of random indices to generate. Random index is
  ///     between 0 and time ind inclusive
  ///
  /// Output:
  ///   n nearest indicies for each input (batch, heads, num_neighbors + num_random)
  #[pyo3(name="find_n_nearest", signature = (query, time_inds, num_neighbors, num_random=0, num_recent=0))]
  fn py_find_n_nearest<'py>(
    &self,
    py: Python<'py>,
    query: PyReadonlyArray3<'py, f32>,
    time_inds: PyReadonlyArray1<'py, i64>,
    num_neighbors: usize,
    num_random: usize,
    num_recent: usize,
  ) -> &'py PyArray3<i64> {
    let query_view = query.as_array();
    let time_ind_view = time_inds.as_array();
    // make sure head count is the same
    assert_eq!(query_view.shape()[1], self.key_array.shape()[0]);
    // make sure query features and key features match
    assert_eq!(query_view.shape()[2], self.key_array.shape()[2]);

    let result = self.find_n_nearest(
      query_view,
      time_ind_view,
      num_neighbors,
      num_random,
      num_recent,
    );
    result.into_pyarray(py)
  }
}

/// Function used to optimize code in python/models.py -- has no other purpose
///
/// Arguments
///   destination (n, T, d) array
///   source (N, n, neighbors, d) array
///   nearest_neighbors (N, n, neighbors) array
#[pyfunction]
fn copy_gradients(
  mut destination: PyReadwriteArray3<f32>,
  source: PyReadonlyArray4<f32>,
  nearest_neighbors: PyReadonlyArray3<i64>,
) {
  let source_view = source.as_array();
  let mut dest_view = destination.as_array_mut();
  let nearest_neighbors_view = nearest_neighbors.as_array();

  let num_heads = dest_view.shape()[0];
  let feature_dim = dest_view.shape()[2];
  let batch_size = source_view.shape()[0];
  let num_neighbors = source_view.shape()[2];
  assert_eq!(source_view.shape()[1], num_heads);
  assert_eq!(source_view.shape()[3], feature_dim);
  assert_eq!(nearest_neighbors_view.shape()[0], batch_size);
  assert_eq!(nearest_neighbors_view.shape()[1], num_heads);
  assert_eq!(nearest_neighbors_view.shape()[2], num_neighbors);

  for batch_ind in 0..batch_size {
    for head_ind in 0..num_heads {
      for neighbor_ind in 0..num_neighbors {
        for feature_ind in 0..feature_dim {
          let found_neighbor = nearest_neighbors_view[[batch_ind, head_ind, neighbor_ind]];
          dest_view[[head_ind, found_neighbor as usize, feature_ind]] +=
            source_view[[batch_ind, head_ind, neighbor_ind, feature_ind]];
        }
      }
    }
  }

  /*
  # this is very slow, but problem is that its possible that neighbor
  # indexes are not distinct. Might be useful to write a module function
  # to speed this up
  for batch_ind in range(last_k_grad.shape[0]):
      for head_ind in range(last_k_grad.shape[1]):
          for neighbor_ind in range(last_k_grad.shape[2]):
              self.stored_k_grad[head_ind, neighbor_ind, :] += last_k_grad[
                  batch_ind, head_ind, neighbor_ind, :
              ]
              self.stored_v_grad[head_ind, neighbor_ind, :] += last_v_grad[
                  batch_ind, head_ind, neighbor_ind, :
              ]
              */
}

/// Arguments
///   stored_values (n, T, d): array of stored values
///   neighbors (N, n, neighbors)
///
/// Returns
///   nearest neighbors indexed values (N, n, neighbors, d)
#[pyfunction]
fn index_nearest_neighbors<'py>(
  py: Python<'py>,
  stored_values: PyReadonlyArray3<'py, f32>,
  neighbors: PyReadonlyArray3<'py, i64>,
) -> &'py PyArray4<f32> {
  let num_heads = stored_values.shape()[0];
  let feature_dim = stored_values.shape()[2];
  let batch_size = neighbors.shape()[0];
  assert_eq!(neighbors.shape()[1], num_heads);
  let num_neighbors = neighbors.shape()[2];

  let stored_value_view = stored_values.as_array();
  let neighbor_view = neighbors.as_array();

  let mut result = Array4::zeros((batch_size, num_heads, num_neighbors, feature_dim));
  for batch_ind in 0..batch_size {
    for head_ind in 0..num_heads {
      for neighbor_ind in 0..num_neighbors {
        result
          .slice_mut(s![batch_ind, head_ind, neighbor_ind, ..])
          .assign(&stored_value_view.slice(s![
            head_ind,
            neighbor_view[[batch_ind, head_ind, neighbor_ind]] as usize,
            ..
          ]))
      }
    }
  }

  result.into_pyarray(py)
}

#[pymodule]
fn spatial_index_rs(_py: Python, m: &PyModule) -> PyResult<()> {
  m.add_class::<SpatialIndex>()?;
  m.add_class::<SpatialIndexConfig>()?;
  m.add_class::<PivotSelectionStrategy>()?;
  m.add_class::<SpatialIndexV2>()?;
  m.add_class::<SpatialIndexConfigV2>()?;
  m.add_class::<SplitStrategyV2>()?;
  m.add_class::<SpatialIndexV3>()?;
  m.add_wrapped(wrap_pyfunction!(copy_gradients))?;
  m.add_wrapped(wrap_pyfunction!(index_nearest_neighbors))?;
  Ok(())
}
