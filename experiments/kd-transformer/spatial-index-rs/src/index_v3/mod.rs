use ndarray::{s, Array3, ArrayViewMut2};
use numpy::{PyArray3, PyReadonlyArray1, PyReadonlyArray3};
use pyo3::prelude::*;

use crate::kmeans::kmeans_basic_lloyd;

struct SubIndexBuilder<'a> {
  /// shape: (time, key dim)
  keys: ArrayViewMut2<'a, f32>,
  /// shape: (time, value dim)
  values: ArrayViewMut2<'a, f32>,
}

impl<'a> SubIndexBuilder<'a> {
  fn build(self) {
  }
}

struct Node {
}

#[pyclass]
pub struct SpatialIndexV3 {
  nodes: Vec<Vec<Node>>
}

impl SpatialIndexV3 {
  fn from_arrays(
    mut keys: Array3<f32>,
    mut values: Array3<f32>,
    config: SpatialIndexConfigV3,
  ) -> Self {
    let &[num_heads, time_dim, key_dim] = keys.shape();
    assert_eq!(values.shape()[0], num_heads);
    assert_eq!(values.shape()[1], time_dim);

    for head_ind in 0..num_heads {
      let (cluster_inds, clusters) = kmeans_basic_lloyd(keys.slice(s![head_ind, .., ..]));
    }
    todo!()
  }
}

#[pymethods]
impl SpatialIndexV3 {
  /// Arguments:
  ///   keys (num heads, time, key dim)
  ///   values (num heads, time, value dim)
  #[staticmethod]
  #[pyo3(name = "from_arrays")]
  fn py_from_arrays<'py>(
    keys: PyReadonlyArray3<'py, f32>,
    values: PyReadonlyArray3<'py, f32>,
    config: Option<SpatialIndexConfigV3>,
  ) -> Self {
    Self::from_arrays(
      keys.to_owned_array(),
      values.to_owned_array(),
      config.unwrap_or_default(),
    )
  }

  #[pyo3(name = "reset")]
  fn py_reset<'py>(
    &mut self,
    keys: PyReadonlyArray3<'py, f32>,
    values: PyReadonlyArray3<'py, f32>,
  ) -> Self {
    todo!()
  }

  /// Arguments:
  ///   query (batch, heads, key dim)
  ///   time inds (batch,)
  ///   num_clusters: Number of nearest neighbors to find
  ///   num_recent: Number of recent indicies to use. If higher than relevant
  ///     time ind, then repeats will happen (in general this case is not
  ///     supported)
  ///
  /// Output:
  ///   n nearest indicies for each input (batch, heads, num_neighbors + num_random)
  ///   keys (batch, heads, num neighbors + num random, key dim)
  ///   values (batch, heads, num neighbors + num random, value dim)
  #[pyo3(name="find_n_nearest",
         signature = (query, time_inds, num_clusters, num_recent=0))]
  fn py_find_n_nearest<'py>(
    &self,
    py: Python<'py>,
    query: PyReadonlyArray3<'py, f32>,
    time_inds: PyReadonlyArray1<'py, i64>,
    num_clusters: usize,
    num_recent: usize,
  ) -> PyNearestNeighborResult {
    todo!()
  }
}

#[pyclass(name = "NearestNeighborResultV3")]
struct PyNearestNeighborResult {
  #[pyo3(get, set)]
  keys: Py<PyArray3<f32>>,

  #[pyo3(get, set)]
  values: Py<PyArray3<f32>>,
}

#[pyclass]
#[derive(Clone)]
struct SpatialIndexConfigV3 {
  #[pyo3(get, set)]
  clusters_per_layer: usize,
}

impl Default for SpatialIndexConfigV3 {
  fn default() -> Self {
    Self {
      clusters_per_layer: 1
    }
  }
}
