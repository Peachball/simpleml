use std::{
  collections::{BinaryHeap, HashSet},
  mem,
};

use ndarray::{
  azip, parallel::prelude::*, s, Array1, Array2, Array3, Array4, ArrayView1, ArrayView2,
  ArrayView3, ArrayViewMut1, ArrayViewMut2, ArrayViewMut3, Axis, Zip,
};
use ndarray_rand::{RandomExt, SamplingStrategy};
use numpy::{IntoPyArray, PyArray3, PyArray4, PyReadonlyArray1, PyReadonlyArray3};
use ordered_float::NotNan;
use osqp::Problem;
use pyo3::prelude::*;
use rand::{
  rngs::{SmallRng, ThreadRng},
  thread_rng, Rng, SeedableRng,
};

use crate::{
  mathutils::{closest_point_custom_linalg, closest_point_qp, compute_projection_v1},
  min_squared_distance_to_bounds, square, squared_l2_norm,
};

#[derive(Clone, Debug)]
enum Split {
  Dimension(usize, f32),
  Time(usize),
  Projection(Array1<f32>, f32),
}

#[derive(Debug)]
struct Node {
  start_ind: usize,

  end_ind: usize,

  children: Option<(usize, usize, Split)>,
}

/// Second version of spatial index with slightly different API + several
/// differences from first. Differences:
///   1. Stores keys + values + reorders them for memory locality
///   2. Uses kd tree to track time index as well
///   3. Uses query vectors to aide in optimal tree construction
///   4. Overall strategy change -- uses O(n) memory and many different strategy
///      types to determine what will restrict search down the most
#[pyclass]
pub struct SpatialIndexV2 {
  nodes: Vec<Vec<Node>>,

  /// Array of (number of heads, reordered time index, key dim)
  keys: Array3<f32>,

  /// Array of (number of heads, reordered time index, key dim)
  values: Array3<f32>,

  /// Array of (number of heads, reordered time index)
  /// represents index to time index (as time dimension has been reordered for
  /// cache locality purposes)
  time_idx: Array2<i64>,

  /// Array of (number of heads, time idx)
  /// represents index where a provided time idx is stored
  reverse_time_idx: Array2<usize>,

  /// Previous nearest neighbors (number of heads, T, neighbors)
  previous_inds: Option<Array3<i64>>,

  config: SpatialIndexConfigV2,

  /// Maximum ind of every key for each component
  /// Shape (key dim,)
  max_key_components: Array1<f32>,
  /// Minimum ind of every key for each component
  /// Shape (key dim,)
  min_key_components: Array1<f32>,
}

impl SpatialIndexV2 {
  /// Arguments:
  ///   query_sample (batch size, num heads, dim)
  ///   query_time_inds (batch_size,)
  ///   previous_nearest_neighbors (batch size, num heads, neighbors)
  ///   keys (num heads, time, key dim)
  ///   values (num heads, time, value dim)
  pub fn from_arrays(
    query_sample: ArrayView3<f32>,
    query_time_inds: ArrayView1<i64>,
    previous_nearest_neighbors: Option<ArrayView3<i64>>,
    mut keys: Array3<f32>,
    mut values: Array3<f32>,
    config: SpatialIndexConfigV2,
  ) -> Self {
    let num_heads = keys.shape()[0];
    let total_time_dim = keys.shape()[1];
    let key_dim = keys.shape()[2];

    let mut time_idx = Array2::<i64>::zeros((num_heads, total_time_dim));
    for head_ind in 0..num_heads {
      for time_ind in 0..total_time_dim {
        time_idx[[head_ind, time_ind]] = time_ind as i64;
      }
    }
    let all_nodes: Vec<_> = Zip::from(keys.axis_iter_mut(Axis(0)))
      .and(values.axis_iter_mut(Axis(0)))
      .and(time_idx.axis_iter_mut(Axis(0)))
      .into_par_iter()
      .map(|(key_slice, value_slice, time_slice)| {
        let nodes = SubIndexBuilder {
          keys: key_slice,
          values: value_slice,
          config,
          time_idx: time_slice,
          nodes: vec![],
          rng: if let Some(seed) = config.rng_seed {
            SmallRng::seed_from_u64(seed)
          } else {
            SmallRng::from_entropy()
          },
        }
        .build();
        nodes
      })
      .collect();

    let mut reverse_time_idx = Array2::zeros((num_heads, total_time_dim));
    for head_ind in 0..num_heads {
      for time_ind in 0..total_time_dim {
        reverse_time_idx[[head_ind, time_idx[[head_ind, time_ind]] as usize]] = time_ind;
      }
    }

    let mut max_key_components = Array1::<f32>::zeros((key_dim,));
    let mut min_key_components = Array1::<f32>::zeros((key_dim,));
    for head_ind in 0..num_heads {
      for time_ind in 0..total_time_dim {
        for key_ind in 0..key_dim {
          let idx = [head_ind, time_ind, key_ind];
          max_key_components[key_ind] = max_key_components[key_ind].max(keys[idx]);
          min_key_components[key_ind] = min_key_components[key_ind].min(keys[idx]);
        }
      }
    }
    Self {
      nodes: all_nodes,
      keys,
      values,
      time_idx,
      reverse_time_idx,
      config,
      max_key_components,
      min_key_components,
      previous_inds: previous_nearest_neighbors.map(|v| {
        let mut owned_v = v.to_owned();
        owned_v.swap_axes(0, 1);
        owned_v
      }),
    }
  }

  /// Arguments:
  ///   query (batch, heads, key dim)
  ///   time inds (batch,)
  ///   num_neighbors: Number of nearest neighbors to find
  ///   num_random: Number of random indices to generate. Random index is
  ///     between 0 and time ind inclusive
  ///   num_recent: Number of recent indicies to use. If higher than relevant
  ///     time ind, then repeats will happen (in general this case is not
  ///     supported)
  ///
  /// Output:
  ///   n nearest indicies for each input (batch, heads, num_neighbors + num_random)
  ///   keys (batch, heads, num neighbors + num random, key dim)
  ///   values (batch, heads, num neighbors + num random, value dim)
  pub fn find_n_nearest(
    &self,
    query: ArrayView3<f32>,
    time_inds: ArrayView1<i64>,
    num_neighbors: usize,
    num_random: usize,
    num_recent: usize,
  ) -> (Array3<i64>, Array4<f32>, Array4<f32>) {
    let batch_dim = query.shape()[0];
    let head_dim = query.shape()[1];
    let key_dim = query.shape()[2];
    let value_dim = self.values.shape()[2];
    assert_eq!(time_inds.shape()[0], batch_dim);
    assert_eq!(self.keys.shape()[0], head_dim);
    assert_eq!(self.keys.shape()[2], key_dim);

    let total_neighbors = num_neighbors + num_random + num_recent;
    let dummy_index_array = Array3::<i64>::zeros((batch_dim * head_dim, 1, 1));
    let mut nearest_neighbor_inds = Array2::<i64>::zeros((batch_dim * head_dim, total_neighbors));
    let mut nearest_neighbor_keys =
      Array3::<f32>::zeros((batch_dim * head_dim, total_neighbors, key_dim));
    let mut nearest_neighbor_values =
      Array3::<f32>::zeros((batch_dim * head_dim, total_neighbors, value_dim));

    Zip::indexed(dummy_index_array.axis_iter(Axis(0)))
      .and(nearest_neighbor_inds.axis_iter_mut(Axis(0)))
      .and(nearest_neighbor_keys.axis_iter_mut(Axis(0)))
      .and(nearest_neighbor_values.axis_iter_mut(Axis(0)))
      .into_par_iter()
      .for_each_init(
        || thread_rng(),
        |rng, (batch_and_head_ind, _, mut inds, mut keys, mut values)| {
          // inds: (neighbors, 1)
          // keys: (neighbors, key dim)
          // values: (neighbors, value dim)
          let batch_ind = batch_and_head_ind / head_dim;
          let head_ind = batch_and_head_ind % head_dim;
          if num_neighbors != 0 {
            let mut result_builder = NearestNeighborResult::new(
              inds.view_mut(),
              keys.view_mut(),
              values.view_mut(),
              num_neighbors,
            );
            if let Some(prev_nearest_inds) = &self.previous_inds {
              let time_ind = time_inds[batch_ind];
              for neighbor_ind in 0..prev_nearest_inds.shape()[2] {
                let prev_ind = prev_nearest_inds[[head_ind, time_ind as usize, neighbor_ind]];
                let raw_idx = self.reverse_time_idx[[head_ind, prev_ind as usize]];

                let key_slice = s![head_ind, raw_idx, ..];
                let key = self.keys.slice(key_slice);
                let value = self.values.slice(key_slice);
                let squared_dist = squared_l2_norm(query.slice(s![batch_ind, head_ind, ..]), key);
                result_builder.try_add(squared_dist, raw_idx as i64, key, value);
              }
            }
            let mut max_distance = 0.0;
            azip!((max_val in &self.max_key_components,
                   min_val in &self.min_key_components,
                   q_v in &query.slice(s![batch_ind, head_ind, ..]))
                  max_distance += square(q_v - max_val).max(square(q_v - min_val)));
            let searcher = SubIndexSearcher {
              config: self.config,
              keys: self.keys.slice(s![head_ind, .., ..]),
              nodes: &self.nodes[head_ind],
              values: self.values.slice(s![head_ind, .., ..]),
              query: query.slice(s![batch_ind, head_ind, ..]),
              time_ind: time_inds[batch_ind],
              result: result_builder,
              time_idx: self.time_idx.slice(s![head_ind, ..]),
              path: Vec::new(),
            };
            searcher.find_nearest_neighbors();
          }
          let mut i = num_neighbors;
          let current_time_ind = time_inds[batch_ind];

          for _ in 0..num_random {
            let random_time_ind = rng.gen_range(0..=current_time_ind);
            inds[i] = random_time_ind;
            keys.assign(&self.get_key_vector(head_ind, random_time_ind as usize));
            values.assign(&self.get_value_vector(head_ind, random_time_ind as usize));
            i += 1;
          }
          for j in 0..(num_recent as i64) {
            let recent_time_ind = if current_time_ind == 0 {
              0
            } else if j > current_time_ind {
              current_time_ind - ((j - 1) % current_time_ind)
            } else {
              current_time_ind - j
            };
            inds[i] = recent_time_ind;
            keys.assign(&self.get_key_vector(head_ind, recent_time_ind as usize));
            values.assign(&self.get_value_vector(head_ind, recent_time_ind as usize));
            i += 1;
          }
        },
      );

    (
      nearest_neighbor_inds
        .into_shape((batch_dim, head_dim, total_neighbors))
        .unwrap(),
      nearest_neighbor_keys
        .into_shape((batch_dim, head_dim, total_neighbors, key_dim))
        .unwrap(),
      nearest_neighbor_values
        .into_shape((batch_dim, head_dim, total_neighbors, value_dim))
        .unwrap(),
    )
  }

  fn get_key_vector(&self, head_ind: usize, time_ind: usize) -> ArrayView1<f32> {
    let actual_ind = self.reverse_time_idx[[head_ind, time_ind]];
    self.keys.slice(s![head_ind, actual_ind, ..])
  }

  fn get_value_vector(&self, head_ind: usize, time_ind: usize) -> ArrayView1<f32> {
    let actual_ind = self.reverse_time_idx[[head_ind, time_ind]];
    self.values.slice(s![head_ind, actual_ind, ..])
  }
}

#[pymethods]
impl SpatialIndexV2 {
  /// Arguments:
  ///   query_sample (batch size, num heads, dim)
  ///   query_time_inds (batch_size,)
  ///   keys (num heads, time, key dim)
  ///   values (num heads, time, value dim)
  #[staticmethod]
  #[pyo3(name = "from_arrays")]
  fn py_from_arrays<'py>(
    query_sample: PyReadonlyArray3<'py, f32>,
    query_time_inds: PyReadonlyArray1<'py, i64>,
    keys: PyReadonlyArray3<'py, f32>,
    values: PyReadonlyArray3<'py, f32>,
    config: Option<SpatialIndexConfigV2>,
  ) -> Self {
    let config_or_default = config.unwrap_or(Default::default());
    let num_heads = keys.shape()[0];
    let total_time_dim = keys.shape()[1];
    assert_eq!(values.shape()[0], num_heads);
    assert_eq!(values.shape()[1], total_time_dim);

    let keys_copy = keys.to_owned_array();
    let values_copy = values.to_owned_array();

    Self::from_arrays(
      query_sample.as_array(),
      query_time_inds.as_array(),
      None,
      keys_copy,
      values_copy,
      config_or_default,
    )
  }

  /// Arguments:
  ///   query (batch, heads, key dim)
  ///   time inds (batch,)
  ///   num_neighbors: Number of nearest neighbors to find
  ///   num_random: Number of random indices to generate. Random index is
  ///     between 0 and time ind inclusive
  ///   num_recent: Number of recent indicies to use. If higher than relevant
  ///     time ind, then repeats will happen (in general this case is not
  ///     supported)
  ///
  /// Output:
  ///   n nearest indicies for each input (batch, heads, num_neighbors + num_random)
  ///   keys (batch, heads, num neighbors + num random, key dim)
  ///   values (batch, heads, num neighbors + num random, value dim)
  #[pyo3(name="find_n_nearest",
         signature = (query, time_inds, num_neighbors, num_random=0, num_recent=0))]
  fn py_find_n_nearest<'py>(
    &self,
    py: Python<'py>,
    query: PyReadonlyArray3<'py, f32>,
    time_inds: PyReadonlyArray1<'py, i64>,
    num_neighbors: usize,
    num_random: usize,
    num_recent: usize,
  ) -> (&'py PyArray3<i64>, &'py PyArray4<f32>, &'py PyArray4<f32>) {
    let query_view = query.as_array();
    let time_ind_view = time_inds.as_array();

    let (inds, keys, values) = self.find_n_nearest(
      query_view,
      time_ind_view,
      num_neighbors,
      num_random,
      num_recent,
    );

    (
      inds.into_pyarray(py),
      keys.into_pyarray(py),
      values.into_pyarray(py),
    )
  }
}

struct NearestNeighborResult<'a, 'b, 'c> {
  /// stores (min squared distance, corresponding index to output array)
  heap: BinaryHeap<(NotNan<f32>, usize)>,
  num_neighbors: usize,

  /// shape: (num output, 1)
  output_inds: ArrayViewMut1<'a, i64>,

  /// shape: (num output: key dim)
  output_keys: ArrayViewMut2<'b, f32>,

  /// shape: (num output: value dim)
  output_values: ArrayViewMut2<'c, f32>,

  inds: HashSet<usize>,
}

impl<'a, 'b, 'c> NearestNeighborResult<'a, 'b, 'c> {
  fn new(
    output_inds: ArrayViewMut1<'a, i64>,
    output_keys: ArrayViewMut2<'b, f32>,
    output_values: ArrayViewMut2<'c, f32>,
    num_neighbors: usize,
  ) -> Self {
    Self {
      heap: BinaryHeap::new(),
      num_neighbors,
      output_inds,
      output_keys,
      output_values,
      inds: HashSet::new(),
    }
  }

  fn try_add(&mut self, distance: f32, raw_idx: i64, key: ArrayView1<f32>, value: ArrayView1<f32>) {
    if self.inds.contains(&(raw_idx as usize)) {
      return;
    }
    if distance < self.max_distance() {
      let idx_to_use = if self.heap.len() < self.num_neighbors {
        self.heap.len()
      } else if let Some((_, idx)) = self.heap.pop() {
        idx
      } else {
        unreachable!()
      };
      self.output_inds[[idx_to_use]] = raw_idx;
      key.assign_to(self.output_keys.slice_mut(s![idx_to_use, ..]));
      value.assign_to(self.output_values.slice_mut(s![idx_to_use, ..]));
      self.heap.push((NotNan::new(distance).unwrap(), idx_to_use));
    }
  }

  fn max_distance(&self) -> f32 {
    if self.heap.len() < self.num_neighbors || self.heap.is_empty() {
      f32::INFINITY
    } else {
      self.heap.peek().map(|v| *v.0).unwrap()
    }
  }
}

/// Intermediate state used to find nearest neighbors for a particular head
struct SubIndexSearcher<'a, 'b, 'c> {
  /// shape: (reordered time index, d) -- note that rows may be reordered, so time_ind is not
  /// equivalent to first dimension
  keys: ArrayView2<'a, f32>,
  /// shape: (reordered time idx, d) -- note that rows may be reordered, so time_ind is not
  /// equivalent to first dimension
  values: ArrayView2<'a, f32>,
  /// shape: (reordered time idx) -- keeps track of time index corresponding to row
  time_idx: ArrayView1<'a, i64>,

  config: SpatialIndexConfigV2,

  /// internal data for kd like tree
  /// Root node is zero
  nodes: &'a [Node],

  /// query to find nearest neighbor
  query: ArrayView1<'a, f32>,
  /// time ind of query
  time_ind: i64,
  result: NearestNeighborResult<'a, 'b, 'c>,
  path: Vec<usize>,
}

impl<'a, 'b, 'c> SubIndexSearcher<'a, 'b, 'c> {
  fn find_nearest_neighbors(mut self) {
    self.find_nearest_helper(
      0,
      0,
      &mut Array1::from_elem((self.query.shape()[0],), f32::NEG_INFINITY),
      &mut Array1::from_elem((self.query.shape()[0],), f32::INFINITY),
    );
  }

  fn find_nearest_helper(
    &mut self,
    node_ind: usize,
    min_time: usize,
    lower_bounds: &mut Array1<f32>,
    upper_bounds: &mut Array1<f32>,
  ) {
    if self.result.max_distance()
      < min_squared_distance_to_bounds(lower_bounds.view(), upper_bounds.view(), self.query)
    {
      return;
    }
    if self.time_ind < min_time as i64 {
      return;
    }

    // check if nearest distance to projection constraints are greater current
    // results
    if 'proj_constraint: {
      match self.path.last() {
        None => break 'proj_constraint false,
        Some(path_node) => {
          if !matches!(
            self.nodes[*path_node].children,
            Some((_, _, Split::Projection(..)))
          ) {
            break 'proj_constraint false;
          }
        }
      }
      let mut projections = Vec::new();
      let mut pivots = Vec::new();
      let mut less_than = Vec::new();
      let mut previous_children = (0, 0);
      let mut previous_was_projection = false;
      for path_node in self.path.iter() {
        if previous_was_projection {
          less_than.push(*path_node == previous_children.0);
        }
        if let Some((left_ind, right_ind, split)) = &self.nodes[*path_node].children {
          if let Split::Projection(projection, pivot_value) = split {
            projections.push(projection.view());
            pivots.push(*pivot_value);
            previous_was_projection = true;
            previous_children = (*left_ind, *right_ind);
          } else {
            previous_was_projection = false;
          }
        }
      }
      if previous_was_projection {
        less_than.push(node_ind == previous_children.0);
      }

      let projection_arr = ndarray::stack(Axis(0), &projections).unwrap();
      let pivot_arr = Array1::from_vec(pivots);

      let squared_distance = closest_point_custom_linalg(
        projection_arr.view(),
        pivot_arr.view(),
        &less_than,
        self.query,
        self
          .keys
          .slice(s![self.nodes[node_ind].start_ind, ..])
          .to_owned(),
      );
      self.result.max_distance() < squared_distance
    } {
      return;
    }

    self.path.push(node_ind);

    let node = &self.nodes[node_ind];
    match &node.children {
      Some((left_ind, right_ind, split)) => {
        match split {
          Split::Dimension(dim, pivot_value) => {
            let query_pivot = self.query[*dim];
            let prev_lower_bound = lower_bounds[*dim];
            let prev_upper_bound = upper_bounds[*dim];
            if query_pivot < *pivot_value {
              upper_bounds[*dim] = *pivot_value;
              self.find_nearest_helper(*left_ind, min_time, lower_bounds, upper_bounds);
              upper_bounds[*dim] = prev_upper_bound;

              lower_bounds[*dim] = *pivot_value;
              self.find_nearest_helper(*right_ind, min_time, lower_bounds, upper_bounds);
              lower_bounds[*dim] = prev_lower_bound;
            } else {
              lower_bounds[*dim] = *pivot_value;
              self.find_nearest_helper(*right_ind, min_time, lower_bounds, upper_bounds);
              lower_bounds[*dim] = prev_lower_bound;

              upper_bounds[*dim] = *pivot_value;
              self.find_nearest_helper(*left_ind, min_time, lower_bounds, upper_bounds);
              upper_bounds[*dim] = prev_upper_bound;
            }
          }
          Split::Time(pivot_value) => {
            self.find_nearest_helper(*left_ind, min_time, lower_bounds, upper_bounds);
            self.find_nearest_helper(*right_ind, *pivot_value, lower_bounds, upper_bounds);
          }
          Split::Projection(projection, pivot_value) => {
            let query_pivot = projection.dot(&self.query);
            if query_pivot < *pivot_value {
              self.find_nearest_helper(*left_ind, min_time, lower_bounds, upper_bounds);
              self.find_nearest_helper(*right_ind, min_time, lower_bounds, upper_bounds);
            } else {
              self.find_nearest_helper(*right_ind, min_time, lower_bounds, upper_bounds);
              self.find_nearest_helper(*left_ind, min_time, lower_bounds, upper_bounds);
            }
          }
        };
      }
      None => {
        for i in node.start_ind..node.end_ind {
          if self.time_idx[i] > self.time_ind {
            continue;
          }
          let current_distance_squared = squared_l2_norm(self.keys.slice(s![i, ..]), self.query);
          self.result.try_add(
            current_distance_squared,
            self.time_idx[i],
            self.keys.slice(s![i, ..]),
            self.values.slice(s![i, ..]),
          );
        }
      }
    }
    self.path.pop();
  }
}

struct SubIndexBuilder<'a, R: Rng> {
  /// shape: (time, key dim)
  keys: ArrayViewMut2<'a, f32>,

  /// shape: (time, value dim)
  values: ArrayViewMut2<'a, f32>,

  time_idx: ArrayViewMut1<'a, i64>,
  config: SpatialIndexConfigV2,
  nodes: Vec<Node>,
  rng: R,
}

impl<'a, R: Rng> SubIndexBuilder<'a, R> {
  fn build(mut self) -> Vec<Node> {
    self.initialize(0, self.time_idx.shape()[0], &mut Vec::new());
    self.nodes
  }

  fn initialize(&mut self, start_ind: usize, end_ind: usize, path: &mut Vec<usize>) -> usize {
    let key_dim = self.keys.shape()[1];
    let node_ind = self.nodes.len();

    self.nodes.push(Node {
      start_ind,
      end_ind,
      children: None,
    });

    if end_ind - start_ind <= self.config.min_leaf_size.max(1) {
      return node_ind;
    }

    let (mid_ind, split) = match self.config.split_strategy {
      SplitStrategyV2::Default => {
        let nodes_since_time_split = self.count_nodes_since_last_time_split(path);

        if nodes_since_time_split < key_dim {
          self.split_by_dimension(
            node_ind,
            nodes_since_time_split,
            self.config.calculate_variance_when_split_on_dim,
          )
        } else {
          self.split_by_time(node_ind)
        }
      }
      SplitStrategyV2::ProjectionOnly => {
        if end_ind - start_ind > 5000 {
          self.split_by_projection(node_ind)
        } else {
          self.split_by_time(node_ind)
        }
      }
      SplitStrategyV2::HeuristicV1 => {
        let sampled_times = self
          .time_idx
          .slice(s![start_ind..end_ind])
          .sample_axis_using(
            Axis(0),
            64.min(end_ind - start_ind),
            SamplingStrategy::WithoutReplacement,
            &mut self.rng,
          );
        let estimated_min_time = sampled_times.iter().min().unwrap();
        let estimated_max_time = sampled_times.iter().max().unwrap();
        if (estimated_max_time - estimated_min_time) as f32 / self.keys.shape()[0] as f32 > 0.75 {
          self.split_by_time(node_ind)
        } else {
          self.split_by_dimension(node_ind, 0, true)
        }
      }
    };

    path.push(node_ind);
    self.nodes[node_ind].children = Some((0, 0, split.clone()));
    let left_child = self.initialize(start_ind, mid_ind, path);
    let right_child = self.initialize(mid_ind, end_ind, path);
    path.pop();
    self.nodes[node_ind].children = Some((left_child, right_child, split));

    node_ind
  }

  fn split_by_projection(&mut self, node_ind: usize) -> (usize, Split) {
    let start_ind = self.nodes[node_ind].start_ind;
    let end_ind = self.nodes[node_ind].end_ind;
    let projection = compute_projection_v1(
      self.keys.slice(s![start_ind..end_ind, ..]),
      32,
      &mut self.rng,
    );
    let get_pivot = |s: &Self, ind: usize| s.keys.slice(s![ind, ..]).dot(&projection);
    let pivot_value = self.compute_pivot_value(start_ind, end_ind, get_pivot);
    let result = self.swap_based_on_pivot(get_pivot, start_ind, end_ind - 1, pivot_value);
    (result, Split::Projection(projection, pivot_value))
  }

  fn count_nodes_since_last_time_split(&self, path: &[usize]) -> usize {
    path
      .iter()
      .rev()
      .take_while(|n| {
        if let Some((_, _, split)) = &self.nodes[**n].children {
          !matches!(split, Split::Time(_))
        } else {
          false
        }
      })
      .count()
  }

  fn swap_based_on_pivot<T: PartialOrd>(
    &mut self,
    get_pivot: impl Fn(&Self, usize) -> T,
    mut start_check_ind: usize,
    mut end_check_ind: usize,
    pivot_value: T,
  ) -> usize {
    while start_check_ind < end_check_ind {
      while get_pivot(&self, start_check_ind) < pivot_value && start_check_ind < end_check_ind {
        start_check_ind += 1;
      }
      while get_pivot(&self, end_check_ind) >= pivot_value && start_check_ind < end_check_ind {
        end_check_ind -= 1;
      }
      // start_check_ind pivot > pivot_value
      // end_check_ind pivot < pivot_value
      if start_check_ind < end_check_ind {
        self.swap_rows(start_check_ind, end_check_ind);
      } else {
        break;
      }
    }
    assert_eq!(start_check_ind, end_check_ind);
    start_check_ind
  }

  fn swap_rows(&mut self, ind1: usize, ind2: usize) {
    if ind1 >= self.time_idx.shape()[0] || ind2 >= self.time_idx.shape()[0] {
      panic!("uh oh");
    }
    if ind1 >= self.values.shape()[0] || ind2 >= self.values.shape()[0] {
      panic!("uh oh2 ");
    }
    if ind1 >= self.keys.shape()[0] || ind2 >= self.keys.shape()[0] {
      panic!("uh oh3");
    }
    self.time_idx.swap(ind1, ind2);

    for i in 0..self.values.shape()[1] {
      self.values.swap([ind1, i], [ind2, i]);
    }

    for i in 0..self.keys.shape()[1] {
      self.keys.swap([ind1, i], [ind2, i]);
    }
  }

  fn split_by_time(&mut self, node_ind: usize) -> (usize, Split) {
    let start_ind = self.nodes[node_ind].start_ind;
    let end_ind = self.nodes[node_ind].end_ind;
    let pivot_value = self.compute_pivot_value(start_ind, end_ind, |s, i| s.time_idx[i]);

    let result = self.swap_based_on_pivot(
      |s, ind| s.time_idx[ind],
      start_ind,
      end_ind - 1,
      pivot_value,
    );

    (result, Split::Time(pivot_value as usize))
  }

  fn split_by_dimension(
    &mut self,
    node_ind: usize,
    dim_split_counter: usize,
    use_variance_splitting: bool,
  ) -> (usize, Split) {
    let start_ind = self.nodes[node_ind].start_ind;
    let end_ind = self.nodes[node_ind].end_ind;
    let dim = if use_variance_splitting {
      let stddevs = self
        .keys
        .slice(s![start_ind..end_ind, ..])
        .sample_axis_using(
          Axis(0),
          64.min(end_ind - start_ind),
          SamplingStrategy::WithoutReplacement,
          &mut self.rng,
        )
        .std_axis(Axis(0), 0.0);

      let (ind, _max_stddev) = stddevs
        .iter()
        .enumerate()
        .max_by(|(_, v1), (_, v2)| v1.partial_cmp(v2).unwrap())
        .unwrap();
      ind
    } else {
      dim_split_counter
    };
    let pivot_value = self.compute_pivot_value(start_ind, end_ind, |s, i| s.keys[[i, dim]]);
    let result = self.swap_based_on_pivot(
      |s, ind| s.keys[[ind, dim]],
      start_ind,
      end_ind - 1,
      pivot_value,
    );
    (result, Split::Dimension(dim, pivot_value))
  }

  fn compute_pivot_value<T: PartialOrd>(
    &mut self,
    start_ind: usize,
    end_ind: usize,
    get_pivot: impl Fn(&Self, usize) -> T,
  ) -> T {
    if self.config.number_of_samples_for_pivot <= 1 {
      let pivot_ind = self.rng.gen_range(start_ind..end_ind);
      return get_pivot(self, pivot_ind);
    }
    let mut pivots = (0..self.config.number_of_samples_for_pivot)
      .map(|_| self.rng.gen_range(start_ind..end_ind))
      .collect::<Vec<_>>();
    pivots.sort_by(|v1, v2| {
      get_pivot(self, *v1)
        .partial_cmp(&get_pivot(self, *v2))
        .unwrap()
    });
    get_pivot(self, pivots[pivots.len() / 2])
  }
}

#[pyclass]
#[derive(Clone, Copy, Debug)]
pub struct SpatialIndexConfigV2 {
  #[pyo3(get, set)]
  pub min_leaf_size: usize,

  #[pyo3(get, set)]
  pub rng_seed: Option<u64>,

  #[pyo3(get, set)]
  pub split_strategy: SplitStrategyV2,

  #[pyo3(get, set)]
  pub calculate_variance_when_split_on_dim: bool,

  #[pyo3(get, set)]
  pub number_of_samples_for_pivot: usize,
}

impl Default for SpatialIndexConfigV2 {
  fn default() -> Self {
    Self {
      min_leaf_size: 8,
      rng_seed: None,
      split_strategy: SplitStrategyV2::HeuristicV1,
      calculate_variance_when_split_on_dim: true,
      number_of_samples_for_pivot: 0,
    }
  }
}

#[pyclass]
#[derive(Clone, Copy, Debug)]
pub enum SplitStrategyV2 {
  /// Iterate through each dimension of kd tree + then use time dimension
  Default,

  /// Same as default strategy, except uses PCA projections instead of dimension
  ProjectionOnly,

  /// Use heursitics. In particular
  ///   1. Given a time range (a, b) and total time T, roughly
  ///      (b-a)**2/4 operations are saved with a time split. Therefore, decide
  ///      on splitting based on ratio of (b-a)/(T-a). Estimate min time using
  ///      sample
  ///   2. Otherwise, pick max variance dimension to use
  HeuristicV1,
}

#[pymethods]
impl SpatialIndexConfigV2 {
  #[new]
  fn py_new() -> Self {
    Self::default()
  }
}
