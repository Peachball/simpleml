use ndarray_linalg::{norm::Norm, TruncatedOrder, TruncatedSvd};
use ndarray_rand::{RandomExt, SamplingStrategy};
use numpy::array;
use rand::Rng;

fn main() {
  let test_arr = array![[-1.0, -1.0],
  [0.0, 0.0],
  [1.0, 1.0]];
  let result = TruncatedSvd::new(test_arr, TruncatedOrder::Largest)
    .decompose(1)
    .unwrap();
  let (s, u, v) = result.values_vectors();
  dbg!(s, u, v);
}
