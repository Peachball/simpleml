use anyhow::Result;
use ndarray_npy::{read_npy, write_npy};
use spatial_index_rs::{
  index_v2::{SpatialIndexConfigV2, SpatialIndexV2, SplitStrategyV2},
  squared_l2_norm, SpatialIndex, SpatialIndexConfig,
};
use std::{
  path::{Path, PathBuf},
  time::Instant,
};

use gumdrop::Options;
use ndarray::{s, Array1, Array3};
use rayon;

#[derive(Debug, Options)]
struct CliOptions {
  #[options(free, required)]
  data_dir: PathBuf,

  #[options(default = "1")]
  num_neighbors: usize,

  #[options(default = "1")]
  index_version: usize,

  #[options(default = "false")]
  save_inds: bool,

  #[options(default = "false")]
  use_existing_inds: bool,

  num_threads: Option<usize>,
}

struct QkvCombo {
  q: Array3<f32>,
  k: Array3<f32>,
  v: Array3<f32>,
  previous_inds: Option<Array3<i64>>,
}

fn format_previous_ind_path(path: &Path, layer_ind: usize) -> PathBuf {
  path.join(format!("layer_{}_inds.npy", layer_ind))
}

/// Returns
///   (Q, K, V) matrices. Shape: (n, T, d)
fn load_qkv(path: &Path, options: &CliOptions) -> Result<Vec<QkvCombo>> {
  let mut result = Vec::new();
  for i in 0.. {
    if !path.join(format!("layer_{}_q.npy", i)).exists() {
      break;
    }
    let previous_ind_path = format_previous_ind_path(path, i);
    let previous_inds = if options.use_existing_inds && previous_ind_path.exists() {
      Some(read_npy(previous_ind_path)?)
    } else {
      None
    };
    result.push(QkvCombo {
      q: read_npy(path.join(format!("layer_{}_q.npy", i)))?,
      k: read_npy(path.join(format!("layer_{}_k.npy", i)))?,
      v: read_npy(path.join(format!("layer_{}_v.npy", i)))?,
      previous_inds,
    });
  }
  Ok(result)
}

fn run_query_v1(qkv: QkvCombo, num_neighbors: usize) -> Array3<i64> {
  // Shape: (n, T, d)
  let QkvCombo { mut q, k, v, .. } = qkv;
  let mut config = SpatialIndexConfig::default();
  let index = SpatialIndex::from_array(k.clone(), config);
  q.swap_axes(0, 1);
  let time_inds: Array1<i64> = (0..q.shape()[0]).map(|i| i as i64).collect();

  // shape (T, n, neighbors)
  let result = index.find_n_nearest(q.view(), time_inds.view(), num_neighbors, 0, 0);
  println!("result shape: {:?}", result.shape());

  let start = Instant::now();
  let mut total_distance = 0.0;
  let mut value_sum = 0.0;
  for i in 0..result.shape()[0] {
    for head_ind in 0..result.shape()[1] {
      for neighbor_ind in 0..result.shape()[2] {
        value_sum += v
          .slice(s![
            head_ind,
            result[[i, head_ind, neighbor_ind]] as usize,
            ..
          ])
          .sum();
        total_distance += squared_l2_norm(
          k.slice(s![
            head_ind,
            result[[i, head_ind, neighbor_ind]] as usize,
            ..
          ]),
          q.slice(s![i, head_ind, ..]),
        );
      }
    }
  }
  println!(
    "index sum: {}\tvalue sum {:.1}\tdistance: {:.1}\tcomputation time: {:.1}",
    result.sum(),
    value_sum,
    total_distance,
    start.elapsed().as_secs_f32()
  );

  result
}

fn run_query_v2(qkv: QkvCombo, num_neighbors: usize) -> Array3<i64> {
  // Shape: (n, T, d)
  let QkvCombo {
    mut q,
    k,
    v,
    previous_inds,
  } = qkv;
  let mut config = SpatialIndexConfigV2::default();
  config.split_strategy = SplitStrategyV2::HeuristicV1;
  //config.split_strategy = SplitStrategyV2::ProjectionOnly;
  println!("Config: {:?}", config);
  q.swap_axes(0, 1);
  let time_inds: Array1<i64> = (0..q.shape()[0]).map(|i| i as i64).collect();
  let index = SpatialIndexV2::from_arrays(
    q.view(),
    time_inds.view(),
    previous_inds.as_ref().map(|v| v.view()),
    k.clone(),
    v,
    config,
  );
  println!("Done building");

  // shape (T, n, neighbors)
  let (result, nearest_keys, nearest_values) =
    index.find_n_nearest(q.view(), time_inds.view(), num_neighbors, 0, 0);
  println!("result shape: {:?}", result.shape());

  let start = Instant::now();
  let mut total_distance = 0.0;
  let mut value_sum = 0.0;
  for i in 0..result.shape()[0] {
    for head_ind in 0..result.shape()[1] {
      for neighbor_ind in 0..result.shape()[2] {
        value_sum += nearest_values
          .slice(s![i, head_ind, neighbor_ind, ..])
          .sum();
        total_distance += squared_l2_norm(
          k.slice(s![
            head_ind,
            result[[i, head_ind, neighbor_ind]] as usize,
            ..
          ]),
          q.slice(s![i, head_ind, ..]),
        );
      }
    }
  }
  println!(
    "index sum: {}\tvalue sum {:.1}\tdistance: {:.1}\tcomputation time: {:.1}",
    result.sum(),
    value_sum,
    total_distance,
    start.elapsed().as_secs_f32()
  );
  result
}

fn main() -> Result<()> {
  let opts = CliOptions::parse_args_default_or_exit();
  let qkv_groups = load_qkv(&opts.data_dir, &opts)?;
  println!("{:?}", qkv_groups[0].k.shape());

  if let Some(threads) = opts.num_threads {
    rayon::ThreadPoolBuilder::new()
      .num_threads(threads)
      .build_global()
      .unwrap();
  }

  let start_time = Instant::now();

  for (i, qkv) in qkv_groups.into_iter().enumerate() {
    let result = match opts.index_version {
      1 => run_query_v1(qkv, opts.num_neighbors),
      2 => run_query_v2(qkv, opts.num_neighbors),
      _ => panic!("Unknown index version"),
    };
    if opts.save_inds {
      write_npy(format_previous_ind_path(&opts.data_dir, i), &result)?;
    }
  }

  println!("Total time: {:.3}", start_time.elapsed().as_secs_f64());

  Ok(())
}
