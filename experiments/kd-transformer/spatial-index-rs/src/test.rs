use numpy::{
  array,
  ndarray::{Array2, ArrayView1},
};
use rand::{distributions::Uniform, thread_rng, Rng};

use crate::{KdTree, NearestNeighborResult, SpatialIndexConfig};

struct TreeWrapper {
  tree: KdTree,
  keys: Array2<f32>,
}

impl TreeWrapper {
  fn new(keys: Array2<f32>) -> Self {
    Self {
      tree: KdTree::new(keys.view(), 0, keys.shape()[0], &Default::default()),
      keys,
    }
  }

  fn new_with_config(keys: Array2<f32>, config: &SpatialIndexConfig) -> Self {
    Self {
      tree: KdTree::new(keys.view(), 0, keys.shape()[0], config),
      keys,
    }
  }
  fn get_nearest_neighbors(&self, query: ArrayView1<f32>, num_neighbors: usize) -> Vec<usize> {
    let mut output = NearestNeighborResult::new(num_neighbors);
    self
      .tree
      .find_n_nearest(self.keys.view(), query, num_neighbors, &mut output);
    let mut result: Vec<_> = output.heap.iter().map(|v| v.1).collect();
    result.sort();
    result
  }
}

#[test]
fn basic_kd_tree() {
  for _ in 0..50 {
    let keys = array![[0, 0], [0, 1], [1, 0], [1, 1]].mapv(|v| v as f32);
    let mut config = SpatialIndexConfig::default();
    config.min_kd_tree_nodes = 3;
    let wrapper = TreeWrapper::new_with_config(keys, &config);
    let result = wrapper.get_nearest_neighbors(array![0.0, 0.0].view(), 1);
    assert_eq!(result, vec![0]);

    dbg!(&wrapper.tree);
    let result = wrapper.get_nearest_neighbors(array![0.0, 0.0].view(), 3);
    assert_eq!(result, vec![0, 1, 2]);
  }
}

#[test]
fn large_tree() {
  let test_size = 128;
  let test_dim = 4;
  let mut keys = Array2::<f32>::zeros((test_size, test_dim));
  for i in 0..test_size {
    if i == 50 {
      continue;
    }
    for j in 0..test_dim {
      keys[[i, j]] = thread_rng().sample(Uniform::new(0.0, 1.0));
    }
  }
  for i in [50] {
    for j in 0..test_dim {
      keys[[i, j]] = 1.5;
    }
  }

  let wrapper = TreeWrapper::new(keys);
  let result = wrapper.get_nearest_neighbors(array![0.0, 0.0, 0.0, 0.0].view(), 1);
  assert!(!result.contains(&50));
}

#[test]
fn multiple_neighbors_clusters() {
  let test_size = 128;
  let test_dim = 4;
  let mut keys = Array2::<f32>::zeros((test_size, test_dim));
  for i in 0..test_size {
    if i == 50 || i == 100 {
      continue;
    }
    for j in 0..test_dim {
      keys[[i, j]] = thread_rng().sample(Uniform::new(0.0, 1.0));
    }
  }
  for i in [50, 100] {
    for j in 0..test_dim {
      keys[[i, j]] = 1.5;
    }
  }

  let wrapper = TreeWrapper::new(keys);

  let result = wrapper.get_nearest_neighbors(array![0.0, 0.0, 0.0, 0.0].view(), 1);
  assert!(!result.contains(&50));
  assert!(!result.contains(&100));

  let result = wrapper.get_nearest_neighbors(array![1.5, 1.5, 1.5, 1.5].view(), 1);
  assert!(result.contains(&50) || result.contains(&100));

  let result = wrapper.get_nearest_neighbors(array![1.5, 1.5, 1.5, 1.5].view(), 2);
  assert_eq!(result, vec![50, 100]);
}

#[test]
fn multiple_neighbors_clusters_with_config() {
  for _ in 0..50 {
    let test_size = 128;
    let test_dim = 4;
    let mut keys = Array2::<f32>::zeros((test_size, test_dim));
    for i in 0..test_size {
      if i == 50 || i == 100 {
        continue;
      }
      for j in 0..test_dim {
        keys[[i, j]] = thread_rng().sample(Uniform::new(0.0, 1.0));
      }
    }
    for i in [50, 100] {
      for j in 0..test_dim {
        keys[[i, j]] = 1.5;
      }
    }

    let mut config = SpatialIndexConfig::default();
    config.min_kd_tree_nodes = 3;
    let wrapper = TreeWrapper::new_with_config(keys, &config);

    let result = wrapper.get_nearest_neighbors(array![0.0, 0.0, 0.0, 0.0].view(), 1);
    assert!(!result.contains(&50));
    assert!(!result.contains(&100));

    let result = wrapper.get_nearest_neighbors(array![1.5, 1.5, 1.5, 1.5].view(), 1);
    assert!(result.contains(&50) || result.contains(&100));

    let result = wrapper.get_nearest_neighbors(array![1.5, 1.5, 1.5, 1.5].view(), 2);
    assert_eq!(result, vec![50, 100]);
  }
}
