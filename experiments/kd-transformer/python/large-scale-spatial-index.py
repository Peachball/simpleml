"""
Test effectiveness of spatial index module
"""
import argparse
from spatial_index_rs import SpatialIndex, SpatialIndexConfig
import numpy as np
import psutil
import time


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--heads", type=int, default=4)
    parser.add_argument("--seqlen", type=int, default=10**5)
    parser.add_argument("--querydim", type=int, default=32)
    args = parser.parse_args()

    rng = np.random.default_rng()
    sample_keys = rng.normal(size=(args.heads, args.seqlen, args.querydim)).astype(
        np.float32
    )
    print(sample_keys.shape)
    process = psutil.Process()
    print("memory usage:", process.memory_info().rss / 1024**2)
    config = SpatialIndexConfig()
    config.min_segment_tree_range = 100
    config.min_kd_tree_nodes = 100
    index = SpatialIndex.from_array(sample_keys, config)
    print("memory usage:", process.memory_info().rss / 1024**2)

    batch_size = 64
    query = rng.normal(size=(64, args.heads, args.querydim)).astype(np.float32)
    time_inds = rng.integers(args.seqlen, size=(64, args.heads))

    start = time.time()
    index.find_n_nearest(query, time_inds, 32)
    print("total time:", time.time() - start)


if __name__ == "__main__":
    main()
