"""
Plots training data
"""
import json
import argparse
from pathlib import Path
import matplotlib.pyplot as plt


def graph(data, x_key, y_key, **kwargs):
    plt.plot([x[x_key] for x in data], [y[y_key] for y in data], **kwargs)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("metrics", type=Path)
    args = parser.parse_args()

    with args.metrics.open() as f:
        stats = json.load(f)

    plt.subplot(221)
    plt.title("cross entropy")
    graph(stats, "step", "cross_entropy")
    if 'aux_loss' in stats[0]:
        plt.subplot(222)
        plt.title("aux loss")
        graph(stats, "step", "aux_loss")
        plt.subplot(223)
        plt.title("k divergence")
        graph(stats, "step", "k_divergence")
        plt.subplot(224)
        plt.title("v divergence")
        graph(stats, "step", "v_divergence")
    plt.show()


if __name__ == "__main__":
    main()
