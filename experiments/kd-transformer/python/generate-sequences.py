"""
Generates random sequence based off transformer
"""
import torch
import argparse
from simpleml.supervised.transformer import GptTransformer
from trainer import load_data_as_inds
from torch.nn import functional as F
from pathlib import Path
import matplotlib.pyplot as plt
from tqdm import tqdm
import numpy as np
from models import Lstm


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--model", default="/tmp/transformer.pkl", type=Path)
    parser.add_argument('--batch-size', default=128, type=int)
    parser.add_argument('--seqlen', default=128, type=int)
    args = parser.parse_args()

    state = torch.load(args.model)
    if 'model_type' not in state:
        state['model_type'] = 'transformer'
    if state['model_type'] == 'transformer':
        model = GptTransformer(**state["config"])
    elif state['model_type'] == 'lstm':
        model = Lstm(**state["config"])
    else:
        raise ValueError(f"Unrecognized model type: {state['model_type']}")
    model.load_state_dict(state['model'])
    chars = state["chars"]

    start_sequence = ['A']
    sequence = [chars.index(c) for c in start_sequence]
    rng = np.random.default_rng()


    state = None
    with torch.no_grad():
        for _ in tqdm(range(args.seqlen)):
            seq_tensor = F.one_hot(
                    torch.tensor(
                        sequence
                        )[None,:],
                num_classes=len(chars),
            )
            result = model(seq_tensor.type(torch.float32))
            prob = F.softmax(result, dim=2)[0, -1, :]
            random_char_ind = rng.choice(len(chars), p=prob.numpy())
            sequence.append(random_char_ind)
    print("Generated sequence:")
    print(''.join(chars[c] for c in sequence))


if __name__ == "__main__":
    main()
