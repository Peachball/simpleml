"""
Visualizes attention matrix
"""
import numpy as np
import torch
import argparse
from simpleml.supervised.transformer import GptTransformer
from trainer import load_data_as_inds
from torch.nn import functional as F
from pathlib import Path
import matplotlib.pyplot as plt


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--model", default="/tmp/transformer.pkl", type=Path)
    parser.add_argument("--seqlen", default=128, type=int)
    parser.add_argument("--start", default=0, type=int)
    args = parser.parse_args()

    state = torch.load(args.model)
    print(state["config"])
    transformer = GptTransformer(**state["config"])
    transformer.load_state_dict(state["model"])
    chars = state["chars"]
    sequence = load_data_as_inds()
    sequence_str = "".join(chars[s] for s in sequence)

    with torch.no_grad():
        subseq = F.one_hot(
            torch.tensor(
                sequence[None, args.start : args.start + args.seqlen], dtype=torch.int64
            ),
            num_classes=len(chars),
        )
        result = transformer.generate(subseq.type(torch.float32))
        print(sequence_str[args.start : args.start + args.seqlen])
        for layer in range(len(transformer.transformer_layers)):
            plt.suptitle(f"Layer {layer}")
            all_unnormalized_attention = transformer.transformer_layers[
                layer
            ].self_attention.last_unnormalized_attention[0, :, :, :]
            all_normalized_attention = transformer.transformer_layers[
                layer
            ].self_attention.last_normalized_attention[0, :, :, :]

            # print how many indicies are required to get 90% at each index
            plt.subplot(2, 5, 9)
            top_percent = 0.7
            for head_ind in range(all_normalized_attention.shape[0]):
                attention = all_normalized_attention[head_ind,:,:]
                sorted_attention = np.sort(attention, axis=1)[:, ::-1]
                attention_cumsum = np.cumsum(sorted_attention, axis=1)
                min_inds = (
                    np.argmax(attention_cumsum > top_percent, axis=1) + 1
                )
                plt.scatter(np.arange(args.seqlen), min_inds, s=5)

            for head_ind in range(4):
                plt.subplot(2, 5, head_ind + 1)
                # (T, T)
                attention = (
                    transformer.transformer_layers[layer]
                    .self_attention.last_normalized_attention[0, head_ind, :, :]
                    .detach()
                    .cpu()
                    .numpy()
                )
                plt.imshow(attention)

            for head_ind in range(4):
                plt.subplot(2, 5, head_ind + 5)
                attention = transformer.transformer_layers[
                    layer
                ].self_attention.last_unnormalized_attention[0, head_ind, :, :]
                plt.imshow(attention)
                plt.ylabel(f"min: {attention.min():.2f} max: {attention.max():.2f}")
            plt.tight_layout()
            plt.show()


if __name__ == "__main__":
    main()
