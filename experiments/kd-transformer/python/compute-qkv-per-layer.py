"""
Computes QKV matrices for each layer to aide optimizing kd tree
"""
import argparse
from pathlib import Path
import torch
from trainer import load_data_as_inds
import numpy as np
from torch.nn import functional as F
from models import KdTransformer, QKVCombo
import numpy as np


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("model", type=Path, help="path to model dir")
    parser.add_argument(
        "data", type=Path, help="path to text file with data to compute matrices"
    )
    parser.add_argument(
        "output", type=Path, help="output directory to place generated files"
    )
    parser.add_argument(
        "--batch-size", type=int, default=1024, help="batch size to use"
    )
    parser.add_argument(
        "--truncate", type=float, default=1, help="how much of data to use"
    )
    args = parser.parse_args()

    saved_model = torch.load(args.model)
    chars = saved_model["chars"]
    config = saved_model["config"]
    DEVICE = torch.device("cuda")
    # shape (T,)
    sequence = load_data_as_inds(args.data, chars)
    sequence = sequence[: int(sequence.shape[0] * args.truncate)]

    with DEVICE, torch.no_grad():
        model = KdTransformer(**config)
        model.load_state_dict(saved_model["model"])
        model.initialize_spatial_indexes(sequence, len(chars))
        matrix_sets = [
            QKVCombo(
                q=np.zeros(
                    (model.num_heads, sequence.shape[0], model.query_dim),
                    dtype=np.float32,
                ),
                k=np.zeros(
                    (model.num_heads, sequence.shape[0], model.query_dim),
                    dtype=np.float32,
                ),
                v=np.zeros(
                    (model.num_heads, sequence.shape[0], model.value_dim),
                    dtype=np.float32,
                ),
            )
            for _ in range(model.num_layers)
        ]
        for batch_ind in range(0, sequence.shape[0], args.batch_size):
            # shape (1, T, D)
            input_tensor = F.one_hot(
                torch.tensor(
                    sequence[batch_ind : batch_ind + args.batch_size], dtype=torch.int64
                ),
                num_classes=len(chars),
            ).type(torch.float32)

            _, qkv_combos = model.compute_qkv_each_layer(input_tensor, batch_ind)
            for layer in range(model.num_layers):
                matrix_sets[layer].q[:, batch_ind : batch_ind + args.batch_size, :] = (
                    qkv_combos[layer].q[0, :, :, :].detach().cpu().numpy()
                )
                matrix_sets[layer].k[:, batch_ind : batch_ind + args.batch_size, :] = (
                    qkv_combos[layer].k[0, :, :, :].detach().cpu().numpy()
                )
                matrix_sets[layer].v[:, batch_ind : batch_ind + args.batch_size, :] = (
                    qkv_combos[layer].v[0, :, :, :].detach().cpu().numpy()
                )

        args.output.mkdir(parents=True, exist_ok=True)
        for layer in range(model.num_layers):
            np.save((args.output / f"layer_{layer}_q.npy"), matrix_sets[layer].q)
            np.save((args.output / f"layer_{layer}_k.npy"), matrix_sets[layer].k)
            np.save((args.output / f"layer_{layer}_v.npy"), matrix_sets[layer].v)


if __name__ == "__main__":
    main()
