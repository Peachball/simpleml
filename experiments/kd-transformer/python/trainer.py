"""
Run transformer, lstm + custom transformer on tale of two cities db

Copied from transformer_shuffle_experiements
"""
import random
import collections
from dataclasses import dataclass
from typing import List, Optional
from simpleml.supervised.transformer import GptTransformer
from torch.nn import functional as F
import torch
import numpy as np
import matplotlib.pyplot as plt
import string
from pathlib import Path
from tqdm import tqdm
from simpleml import stat_logging as log
import time
import json
import argparse
from models import Lstm

DATA_PATH = Path("./data/tale-of-two-cities.txt")
CHARS = string.printable


def load_data_as_inds(path=DATA_PATH, chars=CHARS):
    with path.open() as f:
        contents = f.read()
    seq = []
    for c in contents:
        ind = chars.find(c)
        if ind < 0:
            continue
        seq.append(ind)
    return np.array(seq)


def load_data(seq, seq_len, batch_size):
    """
    Returns
        A (batch dim, time) dimension array representing data
    """
    num_examples = seq.shape[0] // seq_len
    elements_to_use = num_examples * seq_len
    return (
        seq[:elements_to_use].reshape((num_examples, seq_len)),
        np.arange(0, num_examples) * seq_len,
    )


def main():
    BATCH_SIZE = 32
    SEQ_LENGTH = 192
    LR = 1e-4
    PARAMS = {
        "query_dim": 32,
        "num_heads": 8,
        "value_dim": 32,
        "transformer_layer_args": {"attention_type": "neg_log_norm",
                                   "attention_scale": 1},
        "num_layers": 8
    }
    # None means no schedule
    LR_SCHEDULE = None
    DEVICE = torch.device("cuda")

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--model-type", default="transformer", choices=["transformer", "lstm"], type=str
    )
    parser.add_argument("--save-dir", default="/tmp/kd-transformer", type=Path)
    parser.add_argument("--epochs", default=100, type=int)
    parser.add_argument("--truncate", default=1, type=float)
    parser.add_argument("--positional-encoding-max", default=10000, type=int)
    args = parser.parse_args()

    args.save_dir.mkdir(exist_ok=True, parents=True)

    sequence = load_data_as_inds()
    data, sequence_start_inds = load_data(sequence, SEQ_LENGTH, BATCH_SIZE)
    truncated_data_length = int(args.truncate * data.shape[0])
    rng = np.random.default_rng()
    permuted_inds = rng.permutation(truncated_data_length)
    data = data[permuted_inds]
    sequence_start_inds = sequence_start_inds[permuted_inds]

    stats = []
    global_step = 0
    log_file = args.save_dir / "logs.json"
    with DEVICE:
        if args.model_type == "transformer":
            model = GptTransformer(
                len(CHARS),
                len(CHARS),
                positional_encoding_max=args.positional_encoding_max,
                **PARAMS,
            )
        elif args.model_type == "lstm":
            model = Lstm(len(CHARS), len(CHARS), **PARAMS)
        else:
            raise ValueError(f"Unexpected model type {args.model_type}")
        optim = torch.optim.Adam(model.parameters(), LR)

        def set_lr(lr):
            for g in optim.param_groups:
                g["lr"] = lr

        for i in range(args.epochs):
            for batch_ind in tqdm(range(0, data.shape[0], BATCH_SIZE)):
                log.advance_iter()
                if LR_SCHEDULE is not None:
                    set_lr(LR_SCHEDULE(i))

                data_tensor = F.one_hot(
                    torch.tensor(
                        data[batch_ind : batch_ind + BATCH_SIZE, :],
                        dtype=torch.int64,
                    ),
                    num_classes=len(CHARS),
                )
                shifted_input = data_tensor[:, :-1, :]
                expected_output = data_tensor[:, 1:, :]
                sequence_inds = torch.tensor(
                    sequence_start_inds[batch_ind : batch_ind + BATCH_SIZE]
                )

                optim.zero_grad()
                predictions = model(shifted_input.type(torch.float32), sequence_inds)

                cross_entropy = F.cross_entropy(
                    torch.transpose(predictions, 1, 2),
                    torch.transpose(expected_output.type(torch.float32), 1, 2),
                    reduction="none",
                )
                average_cross_entropy = cross_entropy.mean()
                global_step += 1
                stats.append(
                    {"cross_entropy": average_cross_entropy.item(), "step": global_step}
                )
                average_cross_entropy.backward()
                optim.step()
            with log_file.open("w") as f:
                json.dump(stats, f)

            if i % 5 == 0:
                save_file = args.save_dir / f"{args.model_type}-{i}.pkl"
                torch.save(
                    {
                        "optim": optim.state_dict(),
                        "model": model.state_dict(),
                        "model_type": args.model_type,
                        "config": {
                            "input_dim": len(CHARS),
                            "output_dim": len(CHARS),
                            **PARAMS,
                        },
                        "chars": CHARS,
                    },
                    save_file,
                )


if __name__ == "__main__":
    main()
