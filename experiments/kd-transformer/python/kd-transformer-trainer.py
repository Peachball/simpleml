"""
Run custom transformer on tale of two cities dataset

Copied from transformer_shuffle_experiements
"""
import random
import collections
from dataclasses import dataclass
from typing import List, Optional
from torch.nn import functional as F
import torch
import numpy as np
import matplotlib.pyplot as plt
import string
from pathlib import Path
from tqdm import tqdm
import time
import json
import argparse
from models import KdTransformer
from spatial_index_rs import SpatialIndexConfig, PivotSelectionStrategy

DATA_PATH = Path("./data/tale-of-two-cities.txt")
CHARS = string.printable


def load_data_as_inds():
    with DATA_PATH.open() as f:
        contents = f.read()
    seq = []
    for c in contents:
        ind = CHARS.find(c)
        if ind < 0:
            continue
        seq.append(ind)
    return np.array(seq)


def load_batched_data(seq, batch_size):
    """
    Returns
        A (total batches, batch size, time) dimention array representing the
        batched data
    """
    num_batches = seq.shape[0] // batch_size
    inds = np.arange(seq.shape[0] - 1)
    np.random.shuffle(inds)
    elements_to_use = num_batches * batch_size
    return inds[:elements_to_use].reshape((num_batches, batch_size))


def main():
    np.random.seed(1234)
    BATCH_SIZE = 64
    LR = 1e-3
    PARAMS = {
        "query_dim": 64,
        "num_heads": 8,
        "value_dim": 64,
        "transformer_layer_args": {
            "num_neighbors": 0,
            "num_random": 0,
            "num_recent": 32,
        },
    }
    INDEX_VERSION = 2
    USE_CACHED_INDS = True
    # None means no schedule
    LR_SCHEDULE = None
    DEVICE = torch.device("cuda")

    parser = argparse.ArgumentParser()
    parser.add_argument("--save-dir", default="/tmp/kd-transformer-3", type=Path)
    parser.add_argument("--epochs", default=100, type=int)
    parser.add_argument("--truncate", default=1, type=float)
    parser.add_argument("--positional-encoding-max", default=10000, type=int)
    args = parser.parse_args()

    args.save_dir.mkdir(exist_ok=True, parents=True)

    sequence = load_data_as_inds()
    truncated_sequence_length = int(args.truncate * sequence.shape[0])
    sequence = sequence[:truncated_sequence_length]

    # longs representing sequence positions
    # shape (number of batches, batch size)
    data = load_batched_data(sequence, BATCH_SIZE)
    rng = np.random.default_rng()

    stats = []
    global_step = 0
    log_file = args.save_dir / "logs.json"
    with DEVICE:
        model = KdTransformer(
            len(CHARS),
            len(CHARS),
            positional_encoding_max=args.positional_encoding_max,
            **PARAMS,
        )
        optim = torch.optim.Adam(model.parameters(), LR)

        def set_lr(lr):
            for g in optim.param_groups:
                g["lr"] = lr

        for i in range(args.epochs):
            with torch.no_grad():
                model.initialize_spatial_indexes(
                    sequence, len(CHARS), spatial_index_version=INDEX_VERSION,
                    save_dir=Path('experiments/kd-transformer/data/qkv_output_dim64_0/')
                )

            for batch_ind in tqdm(range(data.shape[0])):
                if LR_SCHEDULE is not None:
                    set_lr(LR_SCHEDULE(i))

                # (N,)
                time_inds = data[batch_ind]

                # (N, D)
                input_tensor = F.one_hot(
                    torch.tensor(sequence[time_inds], dtype=torch.int64),
                    num_classes=len(CHARS),
                )
                # (N,)
                expected_output = torch.tensor(
                    sequence[time_inds + 1], dtype=torch.int64
                )

                optim.zero_grad()

                # 1. Calculate normal gradient to update stored gradients
                # (N, D)
                predictions = model.compute_with_index(
                    input_tensor.type(torch.float32),
                    time_inds,
                    use_cached_nn_inds=USE_CACHED_INDS,
                )
                cross_entropy = F.cross_entropy(
                    predictions,
                    expected_output,
                    reduction="none",
                )
                average_cross_entropy = cross_entropy.mean()

                average_cross_entropy.backward()
                model.handle_grad()
                optim.zero_grad()

                # 2. Calculate actual gradient including auxiliary loss
                predictions = model.compute_with_index(
                    input_tensor.type(torch.float32),
                    time_inds,
                    use_stored_grad=True,
                    use_previous_nn_inds=True,
                )
                cross_entropy = F.cross_entropy(
                    predictions,
                    expected_output,
                    reduction="none",
                )
                average_cross_entropy = cross_entropy.mean()

                aux_loss, k_divergence, v_divergence = model.total_aux_loss()
                total_loss = (
                    1 * k_divergence
                    + 1 * v_divergence
                    + 1 * aux_loss
                    + average_cross_entropy
                )
                total_loss.backward()

                optim.step()

                global_step += 1
                stats.append(
                    {
                        "cross_entropy": average_cross_entropy.item(),
                        "step": global_step,
                        "aux_loss": aux_loss.item(),
                        "k_divergence": k_divergence.item(),
                        "v_divergence": v_divergence.item(),
                    }
                )
                if global_step % 100 == 0:
                    with log_file.open("w") as f:
                        json.dump(stats, f)

            save_file = args.save_dir / f"kd-transformer-{i}.pkl"
            torch.save(
                {
                    "optim": optim.state_dict(),
                    "model": model.state_dict(),
                    "model_type": "kd-transformer",
                    "config": {
                        "input_dim": len(CHARS),
                        "output_dim": len(CHARS),
                        **PARAMS,
                    },
                    "chars": CHARS,
                },
                save_file,
            )


if __name__ == "__main__":
    main()
