import numpy as np
from spatial_index_rs import (
    SpatialIndex,
    SpatialIndexConfig,
    SpatialIndexV2,
    SpatialIndexConfigV2,
    SplitStrategyV2
)


def test_basic_usage():
    num_heads = 2
    time_dim = 3
    key_dim = 4
    batch_size = 12
    rng = np.random.default_rng()
    keys = rng.normal(size=(num_heads, time_dim, key_dim)).astype(np.float32)
    index = SpatialIndex.from_array(keys)

    query = rng.normal(size=(batch_size, num_heads, key_dim)).astype(np.float32)
    time_inds = np.ones((batch_size,), dtype=np.int64)
    result = index.find_n_nearest(query, time_inds, 2, 2)

    assert result.shape == (batch_size, num_heads, 4)

    indicies = set()
    # make sure indicies aren't all the same, as 2 should be randomly generated
    # every time
    for i in range(batch_size):
        for j in range(num_heads):
            indicies.add(" ".join(str(v) for v in result[i, j, :]))
    assert len(indicies) > 1


def test_random_being_generated():
    num_heads = 15
    time_dim = 3
    key_dim = 4
    batch_size = 12
    rng = np.random.default_rng()
    keys = rng.normal(size=(num_heads, time_dim, key_dim)).astype(np.float32)
    index = SpatialIndex.from_array(keys)

    query = rng.normal(size=(batch_size, num_heads, key_dim)).astype(np.float32)
    time_inds = np.ones((batch_size,), dtype=np.int64)
    result = index.find_n_nearest(query, time_inds, 0, 2)

    assert result.shape == (batch_size, num_heads, 2)

    indicies = set()
    # make sure indicies aren't all the same, as 2 should be randomly generated
    # every time
    for i in range(batch_size):
        for j in range(num_heads):
            indicies.add(" ".join(str(v) for v in result[i, j, :]))
    assert len(indicies) > 1


def test_results_match():
    num_heads = 15
    time_dim = 1000
    key_dim = 16
    batch_size = 128
    num_neighbors = 10
    rng = np.random.default_rng(0)
    keys = rng.normal(size=(num_heads, time_dim, key_dim)).astype(np.float32)
    index = SpatialIndex.from_array(keys)

    query = rng.normal(size=(batch_size, num_heads, key_dim)).astype(np.float32)
    time_inds = rng.integers(
        num_neighbors, time_dim, size=(batch_size,), dtype=np.int64
    )
    result = index.find_n_nearest(query, time_inds, num_neighbors, 0)

    assert result.shape == (batch_size, num_heads, num_neighbors)

    for batch_ind in range(batch_size):
        for head_ind in range(num_heads):
            # shape: (key dim)
            current_query = query[batch_ind, head_ind, :]
            current_time = time_inds[batch_ind]

            # shape: (time subset, key dim)
            key_subset = keys[head_ind, : current_time + 1, :]
            norms = np.linalg.norm(key_subset - current_query[None, :], axis=1)
            indexes = np.argsort(norms)[:num_neighbors]
            assert all(
                np.sort(result[batch_ind, head_ind, :]) == np.sort(indexes)
            ), f"{batch_ind} {head_ind}"


def test_results_match_with_config():
    num_heads = 15
    time_dim = 100
    key_dim = 16
    batch_size = 16
    num_neighbors = 10
    rng = np.random.default_rng(0)
    keys = rng.normal(size=(num_heads, time_dim, key_dim)).astype(np.float32)
    config = SpatialIndexConfig()
    config.min_segment_tree_range = 3
    config.min_kd_tree_nodes = 5
    index = SpatialIndex.from_array(keys, config)

    query = rng.normal(size=(batch_size, num_heads, key_dim)).astype(np.float32)
    time_inds = rng.integers(
        num_neighbors, time_dim, size=(batch_size,), dtype=np.int64
    )
    result = index.find_n_nearest(query, time_inds, num_neighbors, 0)

    assert result.shape == (batch_size, num_heads, num_neighbors)

    for batch_ind in range(batch_size):
        for head_ind in range(num_heads):
            # shape: (key dim)
            current_query = query[batch_ind, head_ind, :]
            current_time = time_inds[batch_ind]

            # shape: (time subset, key dim)
            key_subset = keys[head_ind, : current_time + 1, :]
            norms = np.linalg.norm(key_subset - current_query[None, :], axis=1)
            indexes = np.argsort(norms)[:num_neighbors]
            assert all(
                np.sort(result[batch_ind, head_ind, :]) == np.sort(indexes)
            ), f"{batch_ind} {head_ind}"


def test_not_enough_values():
    keys = np.array([[[0, 0], [1, 1], [2, 2]]]).astype(np.float32)
    index = SpatialIndex.from_array(keys)
    time_ind = np.array([1])
    query = np.array([[[1.25, 1.25]]], dtype=np.float32)
    result = index.find_n_nearest(query, time_ind, 3, 0)


def test_index_v2_matches_brute_force():
    num_heads = 15
    time_dim = 1000
    key_dim = 16
    value_dim = 32
    batch_size = 32
    num_neighbors = 10
    rng = np.random.default_rng(0)

    queries = rng.normal(size=(num_heads, time_dim, key_dim)).astype(np.float32)
    time_inds = np.arange(time_dim)
    keys = rng.normal(size=(num_heads, time_dim, key_dim)).astype(np.float32)
    values = rng.normal(size=(num_heads, time_dim, value_dim)).astype(np.float32)

    config = SpatialIndexConfigV2()
    config.rng_seed = 2
    index = SpatialIndexV2.from_arrays(queries, time_inds, keys, values, config=config)

    random_time_inds = rng.integers(
        num_neighbors, time_dim, size=(batch_size,), dtype=np.int64
    )
    # shape: (N, n, d)
    random_queries = np.transpose(queries[:, random_time_inds, :], (1, 0, 2))
    neighbors, result_keys, result_values = index.find_n_nearest(
        random_queries, random_time_inds, num_neighbors
    )

    assert neighbors.shape == (batch_size, num_heads, num_neighbors)

    for batch_ind in range(batch_size):
        for head_ind in range(num_heads):
            # shape: (key dim)
            current_query = random_queries[batch_ind, head_ind, :]
            current_time = random_time_inds[batch_ind]

            # shape: (time subset, key dim)
            key_subset = keys[head_ind, : current_time + 1, :]
            norms = np.linalg.norm(key_subset - current_query[None, :], axis=1)
            indexes = np.argsort(norms)[:num_neighbors]

            assert all(
                np.sort(neighbors[batch_ind, head_ind, :]) == np.sort(indexes)
            ), f"{batch_ind} {head_ind}"

            assert np.all(
                np.abs(
                    result_keys[batch_ind, head_ind, :, :]
                    - keys[head_ind, neighbors[batch_ind, head_ind, :], :]
                )
                < 1e-8
            )

            assert np.all(
                np.abs(
                    result_values[batch_ind, head_ind, :, :]
                    - values[head_ind, neighbors[batch_ind, head_ind, :], :]
                )
                < 1e-8
            )


def test_index_v2_projections_matches_brute_force():
    num_heads = 15
    time_dim = 1000
    key_dim = 2
    value_dim = 32
    batch_size = 32
    num_neighbors = 10
    rng = np.random.default_rng(0)

    queries = rng.normal(size=(num_heads, time_dim, key_dim)).astype(np.float32)
    time_inds = np.arange(time_dim)
    keys = rng.normal(size=(num_heads, time_dim, key_dim)).astype(np.float32)
    values = rng.normal(size=(num_heads, time_dim, value_dim)).astype(np.float32)

    config = SpatialIndexConfigV2()
    config.rng_seed = 2
    config.split_strategy = SplitStrategyV2.ProjectionOnly
    index = SpatialIndexV2.from_arrays(queries, time_inds, keys, values, config=config)

    random_time_inds = rng.integers(
        num_neighbors, time_dim, size=(batch_size,), dtype=np.int64
    )
    # shape: (N, n, d)
    random_queries = np.transpose(queries[:, random_time_inds, :], (1, 0, 2))
    neighbors, result_keys, result_values = index.find_n_nearest(
        random_queries, random_time_inds, num_neighbors
    )

    assert neighbors.shape == (batch_size, num_heads, num_neighbors)

    for batch_ind in range(batch_size):
        for head_ind in range(num_heads):
            # shape: (key dim)
            current_query = random_queries[batch_ind, head_ind, :]
            current_time = random_time_inds[batch_ind]

            # shape: (time subset, key dim)
            key_subset = keys[head_ind, : current_time + 1, :]
            norms = np.linalg.norm(key_subset - current_query[None, :], axis=1)
            indexes = np.argsort(norms)[:num_neighbors]

            print(np.sort(neighbors[batch_ind, head_ind, :]))
            print(np.sort(indexes))
            assert all(
                np.sort(neighbors[batch_ind, head_ind, :]) == np.sort(indexes)
            ), f"{batch_ind} {head_ind}"

            assert np.all(
                np.abs(
                    result_keys[batch_ind, head_ind, :, :]
                    - keys[head_ind, neighbors[batch_ind, head_ind, :], :]
                )
                < 1e-8
            )

            assert np.all(
                np.abs(
                    result_values[batch_ind, head_ind, :, :]
                    - values[head_ind, neighbors[batch_ind, head_ind, :], :]
                )
                < 1e-8
            )


def test_index_v2_heuristic_matches_brute_force():
    num_heads = 15
    time_dim = 1000
    key_dim = 2
    value_dim = 32
    batch_size = 32
    num_neighbors = 10
    rng = np.random.default_rng(0)

    queries = rng.normal(size=(num_heads, time_dim, key_dim)).astype(np.float32)
    time_inds = np.arange(time_dim)
    keys = rng.normal(size=(num_heads, time_dim, key_dim)).astype(np.float32)
    values = rng.normal(size=(num_heads, time_dim, value_dim)).astype(np.float32)

    config = SpatialIndexConfigV2()
    config.rng_seed = 2
    config.split_strategy = SplitStrategyV2.HeuristicV1
    index = SpatialIndexV2.from_arrays(queries, time_inds, keys, values, config=config)

    random_time_inds = rng.integers(
        num_neighbors, time_dim, size=(batch_size,), dtype=np.int64
    )
    # shape: (N, n, d)
    random_queries = np.transpose(queries[:, random_time_inds, :], (1, 0, 2))
    neighbors, result_keys, result_values = index.find_n_nearest(
        random_queries, random_time_inds, num_neighbors
    )

    assert neighbors.shape == (batch_size, num_heads, num_neighbors)

    for batch_ind in range(batch_size):
        for head_ind in range(num_heads):
            # shape: (key dim)
            current_query = random_queries[batch_ind, head_ind, :]
            current_time = random_time_inds[batch_ind]

            # shape: (time subset, key dim)
            key_subset = keys[head_ind, : current_time + 1, :]
            norms = np.linalg.norm(key_subset - current_query[None, :], axis=1)
            indexes = np.argsort(norms)[:num_neighbors]

            print(np.sort(neighbors[batch_ind, head_ind, :]))
            print(np.sort(indexes))
            assert all(
                np.sort(neighbors[batch_ind, head_ind, :]) == np.sort(indexes)
            ), f"{batch_ind} {head_ind}"

            assert np.all(
                np.abs(
                    result_keys[batch_ind, head_ind, :, :]
                    - keys[head_ind, neighbors[batch_ind, head_ind, :], :]
                )
                < 1e-8
            )

            assert np.all(
                np.abs(
                    result_values[batch_ind, head_ind, :, :]
                    - values[head_ind, neighbors[batch_ind, head_ind, :], :]
                )
                < 1e-8
            )
