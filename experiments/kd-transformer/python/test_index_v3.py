import numpy as np
from index_v3 import SpatialIndexV3

def test_basic_create():
    num_heads = 15
    time_dim = 1000
    key_dim = 16
    value_dim = 32
    batch_size = 32
    num_neighbors = 10
    rng = np.random.default_rng(0)

    queries = rng.normal(size=(num_heads, time_dim, key_dim)).astype(np.float32)
    time_inds = np.arange(time_dim)
    keys = rng.normal(size=(num_heads, time_dim, key_dim)).astype(np.float32)
    values = rng.normal(size=(num_heads, time_dim, value_dim)).astype(np.float32)

    index = SpatialIndexV3(keys, values)
