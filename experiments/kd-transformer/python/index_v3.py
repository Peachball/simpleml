"""
Index based purely on k-means
"""
import numpy as np
import torch


class SpatialIndexV3:
    """
    Uses kmeans repeatedly until each centroid is within certain distance of all
    points

    Uses RVQ for layers after
    """
    def __init__(self, keys, values, squared_error_tolerance=1e-3):
        """
        Arguments:
            keys (n, T, d)
            values (n, T, d)
            squared_error_tolerance
        """
        assert keys.shape[:2] == values.shape[:2]
        self.squared_error_tolerance = squared_error_tolerance
        self.keys = keys
        self.values = values
        self.time_idx = np.arange(keys.shape[1])

    def torch_kmeans(self):
        """
        Updates arrays in place for each head based on kmeans
        """
        centroid_inds = np.random.choice(self.keys.shape[1], replace=False)
        with torch.no_grad():
            keys = torch.tensor(self.keys)
