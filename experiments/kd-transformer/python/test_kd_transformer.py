import numpy as np
from models import KdTransformer
import torch
from torch.nn import functional as F


def test_small_transformer_training():
    model = KdTransformer(
        2,
        2,
        hidden_dim=4,
        query_dim=3,
        value_dim=5,
        num_heads=1,
        feed_forward_dim=6,
        num_layers=2,
        transformer_layer_args={"num_neighbors": 1, "num_random": 0, "num_recent": 1},
    )
    optim = torch.optim.Adam(model.parameters(), 1e-3)
    sequence = np.array([0, 0, 1, 1, 0, 1, 1])
    with torch.no_grad():
        model.initialize_spatial_indexes(
            sequence,
            2,
        )
    assert np.all(model.transformer_layers[0].self_attention.stored_k_grad == 0)
    assert np.all(model.transformer_layers[0].self_attention.stored_v_grad == 0)

    time_inds = np.array([1, 5])
    input_tensor = F.one_hot(
        torch.tensor(sequence[time_inds], dtype=torch.int64), num_classes=2
    ).type(torch.float32)
    output_tensor = torch.tensor(sequence[time_inds + 1], dtype=torch.int64)
    predictions = model.compute_with_index(input_tensor, time_inds)
    assert model.transformer_layers[0].self_attention.aux_loss == 0

    cross_entropy = F.cross_entropy(predictions, output_tensor, reduction="mean")
    cross_entropy.backward()
    model.handle_grad()

    optim.zero_grad()

    model.compute_with_index(
        input_tensor, time_inds, use_stored_grad=True, use_previous_nn_inds=True
    )
    assert abs(model.transformer_layers[0].self_attention.k_divergence_loss) < 1e-5
    assert abs(model.transformer_layers[0].self_attention.v_divergence_loss) < 1e-5
    assert abs(model.transformer_layers[1].self_attention.k_divergence_loss) < 1e-5
    assert abs(model.transformer_layers[1].self_attention.v_divergence_loss) < 1e-5
    aux_loss, k_divergence, v_divergence = model.total_aux_loss()
    assert k_divergence < 1e-8
    assert v_divergence < 1e-8
