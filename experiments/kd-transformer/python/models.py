"""
Experiment specific models
"""
import time
import torch
from torch.nn import functional as F
from torch import nn
from simpleml.supervised.transformer import positional_embedding
from simpleml.utils import Stopwatch
from spatial_index_rs import SpatialIndex, SpatialIndexV2
import numpy as np
from tqdm import tqdm
import psutil
from spatial_index_rs import SpatialIndexConfig, copy_gradients, index_nearest_neighbors
from collections import namedtuple

QKVCombo = namedtuple("QKVCombo", "q k v")


class Lstm(nn.Module):
    def __init__(self, input_dim, output_dim, hidden_size=256, num_layers=3):
        super().__init__()
        self.input_layer = nn.Linear(input_dim, hidden_size)
        self.lstm = nn.LSTM(hidden_size, hidden_size, num_layers)
        self.output_layer = nn.Linear(hidden_size, output_dim)

    def forward(self, x, hidden=None, starting_position=0, return_hidden=False):
        """
        Arguments:
            x (N, T, D)
        """
        x = self.input_layer(x)
        x += positional_embedding(x, starting_position)
        x, hidden = self.lstm(x, hidden)
        x = self.output_layer(x)
        if return_hidden:
            return x, hidden
        return x


class KdNormAttention(nn.Module):
    def __init__(
        self,
        input_dim,
        query_dim,
        value_dim,
        num_heads,
        output_dim,
        num_neighbors=16,
        num_random=16,
        num_recent=16,
    ):
        super().__init__()
        self.query_dim = query_dim
        self.value_dim = value_dim
        self.num_heads = num_heads
        self.mapping = nn.Linear(input_dim, num_heads * (2 * query_dim + value_dim))
        self.output = nn.Linear(num_heads * value_dim, output_dim)
        self.last_normalized_attention = None
        self.spatial_index = None
        self.spatial_index_version = None

        # shape (n, T, d_k)
        self.stored_k = None
        self.stored_k_grad = None
        # shape (n, T, d_v)
        self.stored_v = None
        self.stored_v_grad = None

        # shape (n, T, neighbors)
        self.nearest_neighbors_cache = None
        # shape (T,)
        self.nearest_neighbors_cache_initialized = None

        # shape (N, n, neighbors, d_k)
        self.last_k_tensor = None
        self.last_v_tensor = None
        self.aux_loss = 0

        # shape (N, n, neighbors)
        self.last_nn_inds = None

        self.num_random = num_random
        self.num_neighbors = num_neighbors
        self.num_recent = num_recent

    def forward(self, x):
        """
        Let n denote number of heads in shape calculations

        Arguments
            x (N, T, D)
        """
        N = x.shape[0]
        T = x.shape[1]
        D = x.shape[2]

        Q, K, V = self.compute_qkv(x)

        attention = self.compute_attention(Q, K)

        # (N, n, T, T)
        mask = torch.triu(torch.full_like(attention, -torch.inf), diagonal=1)

        # (N, n, T, T)
        normalized_attention = F.softmax(
            (attention + mask) / np.sqrt(self.query_dim), dim=3
        )

        # (N, n, T, d_v)
        scaled_values = torch.matmul(normalized_attention, V)

        # (N, T, d_v * n)
        result = torch.transpose(scaled_values, 1, 2).reshape(
            N, T, self.num_heads * self.value_dim
        )
        return self.output(result)

    def generate(self, x, state):
        """
        Let n denote number of heads in shape calculations
        Let T_prev denote timesteps that state contains

        Arguments
            x (N, T, D)
            state two tensors with shapes:
                (N, n, T_prev, d_k)
                (N, n, T_prev, d_v)

        Returns
            Tuple of (final output, new state)
            final output: (N, D) tensor
            new states: (N, T+1, D) tensor
        """
        N = x.shape[0]
        T = x.shape[1]
        D = x.shape[2]

        Q, K, V = self.compute_qkv(x)

        T_prev = 0
        if state is not None:
            # expected shapes:
            # K_prev (N, n, T_prev, d_k)
            # V_prev (N, n, T_prev, d_v)
            (K_prev, V_prev) = state

            T_prev = K_prev.shape[2]

            # (N, n, T_prev + T, d_k)
            K = torch.cat([K_prev, K], dim=2)

            # (N, n, T_prev + T, d_v)
            V = torch.cat([V_prev, V], dim=2)

        attention = self.compute_attention(Q, K)

        # (N, n, T, T_prev + T)
        mask = torch.triu(torch.full_like(attention, -torch.inf), diagonal=1 + T_prev)

        # (N, n, T, T_prev + T)
        normalized_attention = F.softmax(
            (attention + mask) / np.sqrt(self.query_dim), dim=3
        )
        self.last_normalized_attention = normalized_attention

        # (N, n, T, d_v)
        scaled_values = torch.matmul(normalized_attention, V)

        # (N, T, d_v * n)
        result = torch.transpose(scaled_values, 1, 2).reshape(
            N, T, self.num_heads * self.value_dim
        )
        return self.output(result), (K, V)

    def compute_qkv(self, x):
        """
        Arguments
            x (N, T, D)
        Returns
            Q (N, n, T, d_k): queries
            K (N, n, T, d_k): keys
            V (N, n, T, d_v): values
        """
        N = x.shape[0]
        T = x.shape[1]
        D = x.shape[2]

        # (N, T, n(2*d_k + d_v))
        results = self.mapping(x)

        # (N, T, n*d_k)
        Q_all = results[:, :, : self.num_heads * self.query_dim]

        # (N, n, T, d_k)
        Q = torch.transpose(Q_all.view(N, T, self.num_heads, self.query_dim), 1, 2)

        # (N, T, n*d_k)
        K_all = results[
            :, :, self.num_heads * self.query_dim : 2 * self.num_heads * self.query_dim
        ]
        # (N, n, T, d_k)
        K = torch.transpose(K_all.view(N, T, self.num_heads, self.query_dim), 1, 2)

        # (N, T, n*d_v)
        V_all = results[:, :, 2 * self.num_heads * self.query_dim :]
        # (N, n, T, d_v)
        V = torch.transpose(V_all.view(N, T, self.num_heads, self.value_dim), 1, 2)

        return Q, K, V

    def compute_attention(self, Q, K):
        # L2 norm attention
        # shape of Q - K: (N, n, T, T, d_k)
        # resulting shape: (N, n, T, T)
        return -torch.linalg.norm(Q[:, :, :, None, :] - K[:, :, None, :, :], dim=4)

    def compute_with_index(
        self,
        x,
        time_inds,
        use_stored_grad=False,
        use_previous_nn_inds=False,
        use_cached_nn_inds=False,
    ):
        """
        Arguments
            x (N, D)
            time_inds (N,)
        """
        stopwatch = Stopwatch()
        stopwatch.lap("start")
        N = x.shape[0]
        # shape (N, n, 1, d)
        Q_expanded, K_expanded, V_expanded = self.compute_qkv(x[:, None, :])

        # shape (N, n, d)
        Q = Q_expanded[:, :, 0, :]
        K = K_expanded[:, :, 0, :]
        V = V_expanded[:, :, 0, :]

        # shape (N, n, neighbors)
        neighbor_inds = None
        if use_previous_nn_inds:
            neighbor_inds = self.last_nn_inds
        if (
            neighbor_inds is None
            and use_cached_nn_inds
            and np.all(self.nearest_neighbors_cache_initialized[time_inds])
        ):
            # cache shape (n, T, neighbors)
            neighbor_inds = np.transpose(
                self.nearest_neighbors_cache[:, time_inds, :], (1, 0, 2)
            )
        relevant_K_arr = None
        relevant_V_arr = None
        if neighbor_inds is None:
            if self.spatial_index_version == 1:
                neighbor_inds = self.spatial_index.find_n_nearest(
                    Q.detach().cpu().numpy(),
                    time_inds,
                    num_neighbors=self.num_neighbors,
                    num_random=self.num_random,
                    num_recent=self.num_recent,
                )
            elif self.spatial_index_version == 2:
                (
                    neighbor_inds,
                    relevant_K_arr,
                    relevant_V_arr,
                ) = self.spatial_index.find_n_nearest(
                    Q.detach().cpu().numpy(),
                    time_inds,
                    num_neighbors=self.num_neighbors,
                    num_random=self.num_random,
                    num_recent=self.num_recent,
                )
            else:
                raise ValueError(
                    f"invalid stored spatial index version: {self.spatial_index_version}"
                )

        self.last_nn_inds = neighbor_inds
        self.update_nearest_neighbor_cache(neighbor_inds, time_inds)

        stopwatch.lap("spatial index queried")

        stopwatch.lap("starting index selection")
        should_use_numpy_indexing = (relevant_K_arr is None)
        if should_use_numpy_indexing:
            # index array that selects the right head index for each neighbor index
            # The 1 axis's are required to broadcast with neighbor_inds
            # shape (1, n, 1)
            first_index_helper = np.arange(neighbor_inds.shape[1])[None, :, None]

            # shape (N, n, neighbors, d_k)
            relevant_K = torch.tensor(
                self.stored_k[first_index_helper, neighbor_inds, :],
                dtype=torch.float32,
                requires_grad=True,
            )
            stopwatch.lap("key selected based on ind")
            relevant_V = torch.tensor(
                self.stored_v[first_index_helper, neighbor_inds, :],
                dtype=torch.float32,
                requires_grad=True,
            )
            stopwatch.lap("value selected based on ind")
        else:
            relevant_K = torch.tensor(
                relevant_K_arr, dtype=torch.float32, requires_grad=True
            )
            relevant_V = torch.tensor(
                relevant_V_arr, dtype=torch.float32, requires_grad=True
            )

        self.last_k_tensor = relevant_K
        self.last_v_tensor = relevant_V

        # shape (N, n, d_k, neighbors)
        relevant_K_transposed = torch.transpose(relevant_K, 2, 3)

        # shape (N, n, 1, neighbors)
        attention = self.compute_attention(Q_expanded, relevant_K)

        # shape (N, n, 1, neighbors)
        normalized_attention = F.softmax(attention / np.sqrt(self.query_dim), dim=3)

        # shape (N, n, 1, d_v)
        scaled_values = torch.matmul(normalized_attention, relevant_V)

        # shape (N, n*d_v)
        result = scaled_values[:, :, 0, :].reshape(N, self.num_heads * self.value_dim)

        result = self.output(result)

        if use_stored_grad:
            aux_loss = 0
            # (n, N, d_k)
            current_k_grads = torch.tensor(self.stored_k_grad[:, time_inds, :])
            aux_loss += torch.einsum("Nnd,nNd", K, current_k_grads)

            current_v_grads = torch.tensor(self.stored_v_grad[:, time_inds, :])
            aux_loss += torch.einsum("Nnd,nNd", V, current_v_grads)

            self.stored_k_grad[:, time_inds, :] = 0
            self.stored_v_grad[:, time_inds, :] = 0

            self.aux_loss = aux_loss / N

            # shape (n, N, d_k)
            expanded_stored_k = torch.tensor(self.stored_k[:, time_inds, :])
            k_norm_squared = (
                torch.square(K - torch.transpose(expanded_stored_k, 0, 1))
                .mean(dim=[0, 1])
                .sum()
            )
            expanded_stored_v = torch.tensor(self.stored_v[:, time_inds, :])
            v_norm_squared = (
                torch.square(V - torch.transpose(expanded_stored_v, 0, 1))
                .mean(dim=[0, 1])
                .sum()
            )

            self.k_divergence_loss = k_norm_squared
            self.v_divergence_loss = v_norm_squared

        stopwatch.lap("done")
        # stopwatch.print_debug()
        return result

    def update_nearest_neighbor_cache(self, nearest_neighbors, time_inds):
        """
        Arguments:
            nearest_neighbors (N, n, neighbors)
        """
        # cache shape (n, T, neighbors)
        self.nearest_neighbors_cache[:, time_inds, :] = np.transpose(
            nearest_neighbors, (1, 0, 2)
        )
        self.nearest_neighbors_cache_initialized[time_inds] = True

    def handle_grad(self):
        """
        Propagates the gradient into the stored parameters
        """
        # shape (N, n, neighbors, d_k)
        last_k_grad = self.last_k_tensor.grad.detach().cpu().numpy()
        last_v_grad = self.last_v_tensor.grad.detach().cpu().numpy()
        copy_gradients(self.stored_k_grad, last_k_grad, self.last_nn_inds)
        copy_gradients(self.stored_v_grad, last_v_grad, self.last_nn_inds)


class KdTransformerLayer(nn.Module):
    def __init__(
        self,
        hidden_dim,
        query_dim,
        value_dim,
        num_heads,
        feed_forward_dim,
        num_neighbors=16,
        num_random=16,
        num_recent=2,
    ):
        super().__init__()
        self.self_attention = KdNormAttention(
            hidden_dim,
            query_dim,
            value_dim,
            num_heads,
            hidden_dim,
            num_neighbors=num_neighbors,
            num_random=num_random,
            num_recent=num_recent,
        )
        self.attention_norm = nn.LayerNorm(hidden_dim)
        self.ff_linear = nn.Sequential(
            nn.Linear(hidden_dim, feed_forward_dim),
            nn.ReLU(),
            nn.Linear(feed_forward_dim, hidden_dim),
        )
        self.ff_norm = nn.LayerNorm(hidden_dim)

    def forward(self, x):
        """
        Arguments
            x (N, T, D)
        """
        x = self.attention_norm(x + self.self_attention(x))
        x = self.ff_norm(x + self.ff_linear(x))
        return x

    def generate(self, x, state):
        """
        Arguments:
            x (N, T, D)
            state: attention state
        """
        x_delta, new_state = self.self_attention.generate(x, state)
        x = self.attention_norm(x + x_delta)
        x = self.ff_norm(x + self.ff_linear(x))
        return x, new_state

    def compute_with_index(
        self,
        x,
        time_inds,
        use_stored_grad=False,
        use_previous_nn_inds=False,
        use_cached_nn_inds=False,
    ):
        """
        Arguments
            x (N, D)
            time_inds (N,)
        """
        # (N, D)
        x = self.attention_norm(
            x
            + self.self_attention.compute_with_index(
                x,
                time_inds,
                use_stored_grad=use_stored_grad,
                use_previous_nn_inds=use_previous_nn_inds,
                use_cached_nn_inds=use_cached_nn_inds,
            )
        )

        # (N, D)
        x = self.ff_norm(x + self.ff_linear(x))
        return x


class KdTransformer(nn.Module):
    def __init__(
        self,
        input_dim,
        output_dim,
        hidden_dim=128,
        query_dim=4,
        value_dim=8,
        num_heads=4,
        feed_forward_dim=512,
        num_layers=3,
        transformer_layer_args={},
        positional_encoding_max=10000,
    ):
        super().__init__()
        self.num_layers = num_layers
        self.input_layer = nn.Linear(input_dim, hidden_dim)
        self.transformer_layers = nn.ModuleList(
            [
                KdTransformerLayer(
                    hidden_dim=hidden_dim,
                    query_dim=query_dim,
                    value_dim=value_dim,
                    num_heads=num_heads,
                    feed_forward_dim=feed_forward_dim,
                    **transformer_layer_args,
                )
                for _ in range(self.num_layers)
            ]
        )
        self.output_transformation = nn.Linear(hidden_dim, output_dim)
        self.num_heads = num_heads
        self.query_dim = query_dim
        self.value_dim = value_dim

    def forward(self, x, starting_position=0):
        """
        N: batch size
        T: time dimension
        D: feature dimension
        Arguments:
            input (N, T, D): Tensor representing input sequence

        Output:
            (N, T, D): Tensor representing prediction for each part of
            input sequence
        """
        x = self._preprocess(x, starting_position)
        for layer in self.transformer_layers:
            x = layer(x)
        return self._postprocess(x)

    def generate(self, x, state=None, starting_position=0):
        """
        A faster generation method, that keeps track of previously stored hidden
        layer kv pairs. Reduces overall complexity to O(n), for O(n^2)
        generation, as opposed to O(n^3) generation that would've happened with
        the forward() method. Assumes gradient is not tracked.

        Arguments:
            input (batch dim, time dim, feature dim)

        Returns
            Tuple of:
                Matrix with dimension (N, time dim, output_dim)
                hidden state object
        """
        x = self._preprocess(x, starting_position)
        if state is None:
            state = [None for _ in self.transformer_layers]
        new_state = []
        for layer, layer_state in zip(self.transformer_layers, state):
            x, new_layer_state = layer.generate(x, layer_state)
            new_state.append(new_layer_state)
        return self._postprocess(x), new_state

    def _preprocess(self, x, starting_position):
        """
        Arguments:
            x (N, T, D) or (N, D)
            starting_position int or (N,)
        """
        x = self.input_layer(x)
        x = positional_embedding(x, starting_position)
        return x

    def _postprocess(self, x):
        x = self.output_transformation(x)
        return x

    def initialize_spatial_indexes(
        self,
        sequence,
        num_chars,
        batch_size=1024 * 16,
        spatial_index_config=None,
        spatial_index_version=1,
        save_dir=None
    ):
        """
        Arguments:
            sequence (time,): np ndarray representing input sequence
        """
        for layer_init_ind in range(len(self.transformer_layers)):
            print(f"Initializing layer {layer_init_ind}")
            memory_used = psutil.Process().memory_info().rss / 1024**2
            print(f"memory used: {memory_used}")
            layer = self.transformer_layers[layer_init_ind]
            attention = layer.self_attention
            q_output = np.zeros(
                (attention.num_heads, sequence.shape[0], attention.query_dim),
                dtype=np.float32,
            )
            k_output = np.zeros(
                (attention.num_heads, sequence.shape[0], attention.query_dim),
                dtype=np.float32,
            )
            v_output = np.zeros(
                (attention.num_heads, sequence.shape[0], attention.value_dim),
                dtype=np.float32,
            )
            for batch_ind in tqdm(range(0, sequence.shape[0], batch_size)):
                # shape (1, N, D). have 1 because using N as time dimension
                # allows positional embedding logic to assign right embeddings
                sequence_batch = F.one_hot(
                    torch.tensor(
                        sequence[batch_ind : batch_ind + batch_size], dtype=torch.int64
                    ),
                    num_classes=num_chars,
                )[None, :, :].type(torch.float32)
                time_inds = np.arange(batch_ind, batch_ind + sequence_batch.shape[1])

                # shape (1, N (T), D) -> (N, D)
                x_batch = self._preprocess(sequence_batch, batch_ind)[0, :, :]
                for i, initialized_layer in enumerate(
                    self.transformer_layers[:layer_init_ind]
                ):
                    x_batch = initialized_layer.compute_with_index(
                        x_batch, time_inds, use_cached_nn_inds=True
                    )

                # (N, n, T, d)
                q, k, v = attention.compute_qkv(x_batch[None, :, :])
                k_output[:, batch_ind : batch_ind + batch_size, :] = (
                    k[0, :, :, :].cpu().numpy()
                )
                v_output[:, batch_ind : batch_ind + batch_size, :] = (
                    v[0, :, :, :].cpu().numpy()
                )
                q_output[:, batch_ind : batch_ind + batch_size, :] = (
                    q[0, :, :, :].cpu().numpy()
                )

            if save_dir is not None:
                save_dir.mkdir(parents=True, exist_ok=True)
                np.save(save_dir / f'layer_{layer_init_ind}_k.npy', k_output)
                np.save(save_dir / f'layer_{layer_init_ind}_q.npy', q_output)
                np.save(save_dir / f'layer_{layer_init_ind}_v.npy', v_output)

            attention.stored_k = k_output
            attention.stored_k_grad = np.zeros_like(k_output)
            attention.stored_v = v_output
            attention.stored_v_grad = np.zeros_like(v_output)
            if spatial_index_version == 1:
                attention.spatial_index = SpatialIndex.from_array(
                    k_output, spatial_index_config
                )
            elif spatial_index_version == 2:
                attention.spatial_index = SpatialIndexV2.from_arrays(
                    q_output,
                    np.arange(q_output.shape[0]),
                    k_output,
                    v_output,
                    spatial_index_config,
                )
            else:
                raise ValueError(
                    f"Unsupported spatial index version: {spatial_index_version}"
                )

            attention.spatial_index_version = spatial_index_version

            attention.nearest_neighbors_cache = np.zeros(
                (
                    attention.num_heads,
                    sequence.shape[0],
                    attention.num_random
                    + attention.num_neighbors
                    + attention.num_recent,
                ),
                dtype=np.int64,
            )
            attention.nearest_neighbors_cache_initialized = np.full(
                (sequence.shape[0],), False
            )

    def compute_with_index(
        self,
        x,
        starting_position,
        use_stored_grad=False,
        use_previous_nn_inds=False,
        use_cached_nn_inds=False,
    ):
        """
        Arguments
            x (N, D): pytorch tensor
            starting_position (N,): ndarray

        Returns
            (N, D) tensor representing predicted logits
        """
        # (N, 1, D), putting new axis in middle because want distinct starting
        # positions

        x = self._preprocess(
            x[:, None, :], torch.tensor(starting_position, dtype=torch.int64)
        )
        assert x.shape[1] == 1

        # (N, D)
        x = x[:, 0, :]
        for layer in self.transformer_layers:
            x = layer.compute_with_index(
                x,
                starting_position,
                use_stored_grad=use_stored_grad,
                use_previous_nn_inds=use_previous_nn_inds,
                use_cached_nn_inds=use_cached_nn_inds,
            )

        return self._postprocess(x)

    def total_aux_loss(self):
        aux_loss = 0
        k_divergence_loss = 0
        v_divergence_loss = 0
        for layer in self.transformer_layers:
            attention = layer.self_attention
            aux_loss += attention.aux_loss
            k_divergence_loss += attention.k_divergence_loss
            v_divergence_loss += attention.v_divergence_loss
        return aux_loss, k_divergence_loss, v_divergence_loss

    def handle_grad(self):
        """
        Special method to propagate the gradient to the relevant attended to
        positions
        """
        for layer in self.transformer_layers:
            layer.self_attention.handle_grad()

    def compute_qkv_each_layer(self, x, starting_position):
        """
        Arguments:
            x (T, D)
            time_ind (N,) or int (assumes x has shape (T, D))
        """
        if isinstance(starting_position, int):
            starting_position = np.arange(x.shape[0]) + starting_position

        qkv_combos = []
        x = self._preprocess(x[None, :, :], starting_position)[0, :, :]
        for layer in self.transformer_layers:
            # shape (1, n, T, d): queries
            q, k, v = layer.self_attention.compute_qkv(x[None, :, :])
            qkv_combos.append(QKVCombo(q=q, k=k, v=v))

            x = layer.compute_with_index(x, starting_position, use_cached_nn_inds=True)
        x = self._postprocess(x)
        return x, qkv_combos
