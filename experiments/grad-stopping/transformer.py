from pathlib import Path
from simpleml.datasets import load_enwiki9
from torch.nn import functional as F
import matplotlib.pyplot as plt
from simpleml.supervised.transformer import (
    calculate_sinusoidal_embedding,
    GptTransformer,
)
from torch import nn
import torch
import numpy as np
from tqdm import tqdm

DATA_DIR = Path("data/enwik9.zip")
HIDDEN_SIZE = 256
DEVICE = torch.device("cuda")


class ResNetLinearBlock(nn.Module):
    def __init__(self, output_size):
        super().__init__()
        self.output_size = output_size
        self.normalizer = nn.LayerNorm(output_size)
        self.layer = nn.Linear(output_size, output_size)
        self.activation = nn.LeakyReLU()

    def forward(self, x):
        return self.normalizer(x + self.activation(self.layer(x)))


class FFResNet(nn.Module):
    def __init__(self, layers, num_chars):
        super().__init__()
        self.input = nn.Linear(num_chars, HIDDEN_SIZE)
        self.layers = nn.ModuleList(
            [ResNetLinearBlock(HIDDEN_SIZE) for _ in range(layers)]
        )
        self.output = nn.Linear(HIDDEN_SIZE, num_chars)

    def forward(self, x):
        """
        Arguments:
            x (N, D): Expects x to be positionally encoded
        """
        x = self.input(x)
        for layer in self.layers:
            x = layer(x)
        return x


def load_data(limit, remove_unused_chars=False):
    """
    Arguments:
        limit (int): Total length of sequence to pull
        remove_unused_chars (bool): Whether to remove unused characters or not

    Returns:
        tuple of:
            data: 1d uint8 numpy array of raw data
            num_chars: max value in data
            mapping: map of value in data to original value
    """
    data = load_enwiki9(DATA_DIR, limit=limit)
    if remove_unused_chars:
        unique_values, inverse_data = np.unique(data, return_inverse=True)
        return (
            inverse_data,
            unique_values.shape[0],
            {v: unique_values[v] for v in range(unique_values.shape[0])},
        )
    else:
        max_data_value = data.max() + 1
        return data, max_data_value, {v: v for v in range(max_data_value)}


def run_normal_transformer():
    """
    Uses a traditional transformer to predict characters

    For inference, we just extend the sequence, and hope for the best
    """
    seqlen = 128
    batch_size = 128 * 8
    epochs = 1000
    warmup_period = 10
    lr = 1e-3
    warmup_lr = lr * 0.1
    grad_clipping = 1
    data, num_chars, _ = load_data(limit=10**6, remove_unused_chars=False)
    rng = np.random.default_rng(1234)
    seq_start_inds = np.arange((data.shape[0] - 1) // seqlen) * seqlen
    rng.shuffle(seq_start_inds)

    transformer = GptTransformer(
        num_chars, num_chars, hidden_dim=HIDDEN_SIZE, num_layers=1
    )
    num_bits = 0
    for param in transformer.parameters():
        num_bits += torch.finfo(param.dtype).bits * param.numel()
    print(f"Model size in bytes: {num_bits/8}")

    def evaluate():
        evaluate_batch_size = max(128 * 8, batch_size)
        with torch.no_grad():
            all_correct = []
            all_cross_entropy = []

            def accumulate_stats(logits, labels):
                nonlocal all_cross_entropy, all_correct
                if len(logits.shape) > 2:
                    logits = logits.reshape(-1, logits.shape[-1])
                    labels = labels.reshape(-1)
                entropy = F.cross_entropy(logits, labels, reduction="sum")
                num_correct = torch.sum(logits.argmax(dim=-1) == labels)
                all_cross_entropy.append(entropy.item())
                all_correct.append(num_correct.item())

            for i in range(0, data.shape[0] - 1, evaluate_batch_size * seqlen):
                upper_bound = min(i + evaluate_batch_size * seqlen, data.shape[0] - 1)
                amount_to_pad = 0
                actual_batch_size = (upper_bound - i) // seqlen
                overflow = (upper_bound - i) % seqlen
                if overflow != 0:
                    amount_to_pad = seqlen - (upper_bound % seqlen)
                    actual_batch_size += 1
                input_data = np.pad(data[i:upper_bound], (0, amount_to_pad))
                input_data = np.reshape(input_data, (actual_batch_size, seqlen))
                input_tensor = torch.tensor(input_data, dtype=torch.int64)
                input_tensor = F.one_hot(input_tensor, num_classes=num_chars).type(
                    torch.float32
                )

                label_data = np.pad(data[i + 1 : upper_bound + 1], (0, amount_to_pad))
                label_data = np.reshape(label_data, (actual_batch_size, seqlen))
                label_tensor = torch.tensor(label_data, dtype=torch.int64)

                starting_positions = torch.arange(actual_batch_size) * seqlen + i

                logits = transformer(input_tensor, starting_position=starting_positions)

                unpadded_batch = (upper_bound - i) // seqlen
                accumulate_stats(logits[:unpadded_batch], label_tensor[:unpadded_batch])

                if amount_to_pad != 0:
                    accumulate_stats(
                        logits[unpadded_batch, :overflow],
                        label_tensor[unpadded_batch, :overflow],
                    )

            total_training_samples = data.shape[0] - 1
            return (
                sum(all_correct) / total_training_samples,
                sum(all_cross_entropy) / total_training_samples,
            )

    def select_data(start_inds):
        nonlocal data, seqlen
        input_data = np.zeros((start_inds.shape[0], seqlen), dtype=np.uint8)
        row_indicies = np.arange(start_inds.shape[0])
        col_indicies = np.tile(np.arange(seqlen), (start_inds.shape[0], 1))
        indicies = start_inds[:, None] + col_indicies
        input_data[row_indicies[:, None], col_indicies] = data[indicies]

        return torch.tensor(input_data, dtype=torch.int64)

    optim = torch.optim.Adam(transformer.parameters(), lr=lr)
    reduce_scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optim)

    accuracy_history = []
    entropy_history = []
    grad_norm = []
    for epoch in tqdm(range(epochs)):
        accuracy, entropy = evaluate()
        if epoch < warmup_period:
            for g in optim.param_groups:
                g["lr"] = warmup_lr
        elif epoch == warmup_period:
            for g in optim.param_groups:
                g["lr"] = lr
        else:
            reduce_scheduler.step(entropy)
        current_lr = optim.param_groups[0]["lr"]
        print(f"Accuracy: {accuracy}")
        print(f"Entropy: {entropy}")
        print(f"LR: {current_lr}")
        accuracy_history.append(accuracy)
        entropy_history.append(entropy)
        for i in range(0, seq_start_inds.shape[0], batch_size):
            start_inds = seq_start_inds[i : i + batch_size]

            input_tensor = select_data(start_inds)
            label_tensor = select_data(start_inds + 1)
            input_tensor = F.one_hot(input_tensor, num_classes=num_chars).type(
                torch.float32
            )

            optim.zero_grad()
            logits = transformer(input_tensor, start_inds)
            error = F.cross_entropy(
                logits.reshape(-1, num_chars), label_tensor.reshape(-1)
            )

            error.backward()

            current_norm = torch.nn.utils.clip_grad_norm_(
                transformer.parameters(), grad_clipping
            )
            grad_norm.append(current_norm.item())

            optim.step()
    plt.subplot(221)
    plt.title("accuracy")
    plt.plot(accuracy_history)
    plt.subplot(222)
    plt.title("entropy")
    plt.plot(entropy_history)
    plt.subplot(223)
    plt.title("norm")
    plt.plot(grad_norm)

    plt.show()


def run_sanity_test():
    """
    Uses a FF network to predict characters based off character index
    """
    batch_size = 1024 * 16 * 8 * 4
    data, num_chars, _ = load_data(limit=10**6, remove_unused_chars=False)
    print(num_chars)
    inds = np.arange(data.shape[0])
    rng = np.random.default_rng(1234)

    # ignore last index because index is treated as input here
    shuffled_inds = rng.permutation(inds[:-1])
    shuffled_data = data[shuffled_inds + 1]

    model = FFResNet(12, num_chars)
    optim = torch.optim.Adam(model.parameters(), lr=1e-3)

    def evaluate():
        evaluate_batch_size = max(4096, batch_size)
        with torch.no_grad():
            all_correct = []
            all_cross_entropy = []
            for i in range(0, inds.shape[0] - 1, evaluate_batch_size):
                upper_bound = min(i + evaluate_batch_size, inds.shape[0] - 1)
                indexes = inds[i:upper_bound]
                labels = torch.tensor(data[i + 1 : upper_bound + 1], dtype=torch.int64)

                embeddings = calculate_sinusoidal_embedding(
                    torch.tensor(indexes), num_chars
                )
                logits = model(embeddings)

                entropy = F.cross_entropy(logits, labels, reduction="sum")
                num_correct = torch.sum(logits.argmax(dim=1) == labels)
                all_correct.append(num_correct.item())
                all_cross_entropy.append(entropy.item())
            total_training_samples = data.shape[0] - 1
            return (
                sum(all_correct) / total_training_samples,
                sum(all_cross_entropy) / total_training_samples,
            )

    errors = []
    for _ in tqdm(range(30)):
        for i in range(0, shuffled_inds.shape[0], batch_size):
            index_batch = shuffled_inds[i : i + batch_size]
            index_tensor = torch.tensor(index_batch, dtype=torch.int64)
            data_batch = torch.tensor(shuffled_data[i : i + batch_size]).type(
                torch.int64
            )

            embedded_batch = calculate_sinusoidal_embedding(index_tensor, num_chars)

            optim.zero_grad()
            logits = model(embedded_batch)
            error = F.cross_entropy(logits, data_batch)
            error.backward()
            errors.append(error.item())

            optim.step()
    accuracy, entropy = evaluate()
    print(f"Accuracy: {accuracy}")
    print(f"Entropy: {entropy}")
    plt.plot(errors)
    plt.show()


def main():
    with torch.device(DEVICE):
        # run_sanity_test()
        run_normal_transformer()


if __name__ == "__main__":
    main()
