# Gradient stopping

## Motivation

I've been brainstorming, and feel like my main 2 ideas for scaling transformers
both rely on propagating gradients independently. For example:

- KD-transformer: The original idea of using the KD lookup also required
  propagating the gradient to the looked up key vector for better training.
- Memorizing transformer [1]: Although they don't test gradient propagation,
  it's not clear why they don't. Does it just not work?
- Hierarchal transformers: My current idea is that I should be able to train
  "global context" tokens that represent the large scale via multiple phases of
  training. Some example of phases are (1) train small scale transformer using
  randomly initialized global context tokens and input data (2) train separate
  transformer on only sequence of global context tokens (3) train transformer on
  new global context

## Goals

Figure out if gradient propagation is feasible, and what the best ways to do so
are.

## Things to test

Settings
- propagate gradient for mlp on MNIST, CIFAR, etc
- propagate gradient for global context in my form of hierarchal transformer

Gradient propagation schemes
- converge on "correct" context and then train each part separately going up the
  chain
- store gradients, and use gradients later

## Learnings

Transformers are extremely sensitive. Some notes
- Initial learning rate, and learning rate schedule affects convergence spot
- Gradient clipping is very important, and stabilizes network. Generally,
  network can have error spike near end of training
- position of layer normalization has a large impact on stability
- Transformer training in general is extremely finicky, and probably more
  important than the context length

## Experiments

### Adjustments to decoder only transformer

Removed unused characters (256 values -> 195 values). Additionally, truncates
256 -> 237 by default because it uses max value from dataset to determine number
of possible values.

FF net has no difference in training/error (13% acc, 3.6 entropy) enwiki6

Transformer on enwiki6 doesn't have that much of a starting difference --
beginning entropy is around 5.6. Not sure what is happening at the ~3-4 entropy
range.

Will continue experiements without this data normalization -- don't think it
helps

Trying enwiki5 2 layer transformer to see if anything interesting happens. Tried with
lr=1e-3 and found annoying plateau into sudden improvement pattern at roughly
epoch 200. Increasing epochs to 2000 shows training instability (sudden error
spike just before hitting 0) + "convergence" at roughly 90% acc and 0.37
entropy.

Trying gradient clipping. Gradient follows peculiar pattern pattern that seems
related to the data. Norm goes from high -> low -> increasingly
spiky/oscillating. Error follows this pattern, where beginning error decrease is
fast -> plateau -> gradual.

Norm clipping of 10 gets 96% acc with 1000 epochs. Norm clipping of gets 99%
acc.

Retrying above with 1 layer yields similar result. Trying with lr=1e-2 to see if
I can force network into bad region. It just slows down training, but doesn't
seem to consistently get stuck. Training speed seems to vary a *lot*, but the
ReduceLROnPlateau seems to impact this as well (sometimes it gets stuck earlier
harder, which is compounded by LR dropping).

Trying with enwiki6 lr=1e-3 converges around 60% acc and 1.31 entropy.

In general, it's probably not worth it to move past enwiki5 because training
transformers has a ton of variance

Switching everything to pre-LN now because of better stability, and using
enwiki5 + lr=1e-2. After 2 runs, it *seems* more stable(?). Even after
increasing lr=1e-1 and a *massive* error rate spike it comes back down.

Trying enwiki6 with lr=1e-2 and 1 layer gets acc=0.69 entropy=1.02

Adding output normalization and retrying with lr=1e-2. Gets around 0.68 acc at
epoch 250 -- think it'll get to acc=0.69 soon enough. Not clear if output norm
helps that much, but it logically makes sense based on pre-LN improvement.

Lowering lr to 1e-3 actually lowers convergence crazily enough (acc 0.64 entropy
1.22)

### Decoder only transformer

Used 12 layer transformer with skip connections, and 256 hidden layer size. Note
that this is very very deep, as each "layer" has multiple sub layers. The
learning rate is set lower  (1e-4 for 6 layer and 1e-5 for 12 layer), and leaky
ReLU is used. Seems to be converging around 1.6 cross entropy and 0.53 accuracy
(at time of writing, errors are 1.65 and 0.526). Graph suggests error + accuracy
could be improved with further training -- the error curve has a slope at the
end still. Overall training time took around 2-3 hours.

Trained on enwiki7.

Reducing layers to 4 (because it wasn't converging with 12 quickly) and running
gets >0.55 accuracy and <1.543 entropy.

Reducing layers to 1 to sanity check -- also gets to ~0.5 accuracy and 1.69
entropy without converging.

Increasing dataset size to enwiki8

Not much seems to change. Not clear how many epochs it takes for transformer to
fully converge (even on enwiki6).

With enwiki6 for 2000 epochs and 1 layer, lr=1e-3, training roughly stabilized at around
0.67 accuracy, 1.09 entropy.

Tried enwiki6 with 2 layers, and already getting stuck. Reddit [3] suggests that
issue is due to high learning rate, and low batch size, with warm up periods.
Not experienced enough to know how long this should be, so testing with lr=1e-4,
4096 batch size, AdamW. Seems to converge at roughly 0.57 accuracy and 1.44
entropy at epoch ~3k.

Trying enwiki6 with 2 layers, lr=1e-5, 4096 batch size, Adam. This gets 2.24
entropy and 0.35 accuracy after >5k epochs.

Trying same setup but with lr=3e-5 -- got to same spot (1.44/1.43 ish entropy)
after 7k epochs. Still stuck.

Trying same setup starting at lr=1e-3, but using ReduceLROnPlateau. Am at
entropy=0.87, accuracy=0.75 at epoch ~2.1k, at ~1.4 it/s.

Switching to flash attention/memory efficient attention brought speed to 1.75
it/s (25% speed improvement). Restarting run using lr=1e-2 (and ignoring warmup)
causes it to get stuck in 3.506 entropy (too extreme).

Trying warmup of 10 epochs using warmup lr=1e-5, and using lr=1e-2 after warmup. Still
permanently stuck at around 3.506.

Trying warmup of 30 epochs using warmup of 30 and lr=1e-2 after warmup. Still
stuck at 3.506. Looks like high LR will cause model to be permanently stuck in
bad solution space.

Switching to use AdamW does not get out of the local minima.

Using lr=3e-3 is still too high. lr=1e-3 seems like it's the move -- accuracy
and entropy converged to same amount as last time (acc: 0.74, entropy: 0.88).
This phase of training is hard to say because the error is not consistently
decreasing, and it's not clear if it will suddenly drop.

Retrying 1e-3 experiment on enwiki7, but it seems to get stuck longer than 1e-4.
Seems like the entropy=3.5 is a barrier, as before that is the model learning to
ignore printable characters. Additionally, using higher LR (1e-3) tends to cause
the model to "hit" the barrier too quickly, and causes it to get stuck. That
said, 1e-5 is really too slow though.

Warmup does not seem to be helping with any of the experiments so far.

Seeing if we can lower sequence length to 32 to dramatically increase batch size
and therefore convergence. Doesn't seem to be that much faster (same it/s and
increasing lr doesn't help that much as it still gets stuck at 3.5).

Another theory about the 3.5 area is that higher LRs are getting through it
faster, but it just doesn't look like it because of the oscillations + different
scale.

Trying enwiki7. Epoch ~1k has 2.13 entropy 0.39 accuracy with lr 1e-5.

Restarting with lr 1e-4. Epoch ~3k has 1.51 entropy .56 accuracy and lr dropped
at some point. enwiki6 is being overfit but not by that much (because enwiki7
accuracy/metrics are similar)

Trying enwiki7 with 12 layer network and lr=1e-6 for night. Batch size is 1024
instead of 4096. Entropy is ~2.30 after 980 epochs. 2.05 entropy after 1.8k
epochs.

Training just the LLM is raising a lot of questions:
1. Is there a difference in output between low LR vs high LR? Are high LR models
   stuck in different minima?
  * answer: YES. High LR can cause model to permanently stay in bad region, and
    it's qualitatively different than low LR.
2. What is the overall training curve? Should I expect it to be bumpy? Or a
   smooth curve?
  * answer: very gradual
3. What is the minimum batch size required?
  * answer: larger is always better. Do as large as GPU memory allows

### Sequence modelling control

Used a deep ff net. It's hard to train at enwiki8 scale quickly, so will use
enwiki7 unless I'm confident/curious. Generally, top1 accuracy seems to stabilize
around 13%, and entropy is around 3.5

### Stop gradient training scheme (`mlp.py:run_context`)

Emulate my plan for training hierarchal transformers. I'll have the following
trainable components:
- model A: (784 -> hidden)
- model B: (hidden -> logits)
- hidden state

The hidden dimension will represent the context that will happen for the stop
gradient approach.

The training will go as follows:
1. Train model B + hidden, initialize hidden
2. Train model A
3. Train from bottom up on repeat:
  3a. Train model B
  3b. Train hidden
  3c. Train model A

The final model will be the (A -> B)

Result:
Needs roughly double the network size to get the same performance. Model
capacity is significantly smaller, and takes roughly an order of magnitude
longer to train.

Larger capacity means 784 -> 128 -> 128 -> 10

Doing only step 3 matches performance of control. Think fashion-MNIST is too
easy, so one epoch is reaching close to convergence

Q&amp;A:
- Where will will model capacity be needed?
  - The part before the context seems slightly better (0.9 vs 0.8) but not by a
    lot -- doesn't matter too much

### Control (`mlp.py:run_control`)

A simple 2 layer NN using LeakyReLU activations, Adam optimizer, 4096 batch size, and
200 epochs, seems to converge at around 95% accuracy, or about 0.18 average
cross entropy

## Brainstorming

### Problems with experiment

Issues:
- transformer takes too long to train
- hard to tell if transformer converged
  - transformer never converges to a clearly low error value
  - results are always way higher than SOTA and not clear if convergence is true
  - results seem to depend on pattern of initial learning rate
- hard to tell if performance is because of bad training hyperparameters, or
  because of actual results
  - [4] may help with this

Attempts to remediate
- normalize data by removing characters that never exist
  * doesn't work. The plateau is at a lower error entropy than this
- compare with LSTM + other models implemented myself instead of SOTA

### Next steps based on first experiment

Transformers are quite slow. 2-3 hours is too long to iterate. Need to look
around about what is taking so long. Try some things like:
- smaller model (not sure if this is issue because model is deep)
- smaller dataset (this is risky though, because might fit 100%)

### Transformer model next steps

Will make some simplifying assumptions for faster implementation. In particular:
- Will only train on context aligned sections
- Will use decoder model for predicting context

Every model will be at least 3 layers, with hidden size 128. Will assume model
gets to know which index is being generated.

Will use enwiki8 instead of enwiki9 for faster testing

Overall model architecture will be as follows.

Models:
- model F (fine grained)
- model C (coarse)
- model P (next step prediction)
- context H (context vectors every block)

Training cycle:
1. Train F given H and data
2. Train H given F and data
3. Train P given H
4. Train C given residual of P and H

Control architecture: Single 12 layer model

Sanity test: Single FF network using only positional encoding

Basic hierarchal model:
- model A: 6 layer, fine grained
- model B: 6 layer, coarse

issue: not sure how to deal with efficient prediction for hierarchal model


## References

Memorizing transformer:
[1]: https://arxiv.org/abs/2203.08913

Hierarchal transformer
[2]: https://arxiv.org/pdf/2110.13711

Informal reddit llm training tips
[3]: https://www.reddit.com/r/MachineLearning/comments/z088fo/r_tips_on_training_transformers/

Understanding the Difficulty of Training Transformers
[4]: https://arxiv.org/abs/2004.08249

Pre-Layer norm vs Post-layer norm
[5]: https://arxiv.org/pdf/2002.04745

Explanation of transformer scaling issues
Allows for deeper transformers
[6]: https://arxiv.org/pdf/2403.09635
