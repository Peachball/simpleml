import torch
from torch import nn
from torch.nn import functional as F
from pathlib import Path
import urllib.request
from tqdm import tqdm
import matplotlib.pyplot as plt
import itertools
from simpleml.datasets import load_fashion_mnist

MNIST_DATA_DIR = Path("data/fashion-mnist")
HIDDEN_SIZE = 128
DEVICE = torch.device("cuda")



def run_context():
    BATCH_SIZE = 64
    EPOCHS = 0
    STEP3_REPEATS = 10
    STEP3_INTERNAL_EPOCHS = 1

    images, labels = load_fashion_mnist(MNIST_DATA_DIR)

    model_A = nn.Sequential(
        nn.Linear(784, HIDDEN_SIZE),
    ).to(DEVICE)
    model_B = nn.Sequential(
        nn.LeakyReLU(),
        nn.Linear(HIDDEN_SIZE, 10),
    ).to(DEVICE)

    hidden = nn.Parameter(
        torch.randn(images.shape[0], HIDDEN_SIZE, dtype=torch.float32, device=DEVICE)
    )

    def evaluate():
        with torch.no_grad():
            image_tensor = torch.tensor(images, dtype=torch.float32, device=DEVICE)
            label_tensor = torch.tensor(labels, dtype=torch.int64, device=DEVICE)
            predicted_logits = model_B(model_A(image_tensor))
            average_entropy_loss = F.cross_entropy(predicted_logits, label_tensor)
            top1_accuracy = (
                torch.sum(label_tensor == torch.argmax(predicted_logits, dim=1))
                / label_tensor.shape[0]
            )
            return average_entropy_loss.item(), top1_accuracy.item()

    # step 1
    print("Running step 1")
    optim = torch.optim.Adam(itertools.chain(model_B.parameters(), [hidden]), lr=1e-3)
    errors = []
    for _ in tqdm(range(EPOCHS)):
        for i in range(0, images.shape[0], BATCH_SIZE):
            optim.zero_grad()

            hidden_batch = hidden[i : i + BATCH_SIZE]
            label_batch = torch.tensor(
                labels[i : i + BATCH_SIZE], dtype=torch.int64
            ).to(DEVICE)

            predicted_logits = model_B(hidden_batch)
            error = F.cross_entropy(predicted_logits, label_batch)
            error.backward()

            errors.append(error.item())
            optim.step()

    # plt.plot(errors)
    # plt.show()
    entropy, accuracy = evaluate()
    print(f"Entropy: {entropy}")
    print(f"Accuracy: {accuracy}")

    # step 2
    print("Running step 2")
    optim = torch.optim.Adam(model_A.parameters(), lr=1e-3)
    errors = []
    for _ in tqdm(range(EPOCHS)):
        for i in range(0, images.shape[0], BATCH_SIZE):
            optim.zero_grad()

            image_batch = torch.tensor(
                images[i : i + BATCH_SIZE], dtype=torch.float32
            ).to(DEVICE)
            hidden_batch = hidden[i : i + BATCH_SIZE]

            predicted_hidden = model_A(image_batch)
            error = F.mse_loss(predicted_hidden, hidden_batch)
            error.backward()

            errors.append(error.item())
            optim.step()
    # plt.plot(errors)
    # plt.show()

    entropy, accuracy = evaluate()
    print(f"Entropy: {entropy}")
    print(f"Accuracy: {accuracy}")

    print("Running step 3")
    step3_entropy = []
    step3_accuracy = []
    optim = torch.optim.Adam(
        itertools.chain(model_A.parameters(), model_B.parameters(), [hidden]), lr=1e-3
    )
    for _ in tqdm(range(STEP3_REPEATS)):
        entropy, accuracy = evaluate()
        step3_entropy.append(entropy)
        step3_accuracy.append(accuracy)

        hidden.data = model_A(torch.tensor(images, dtype=torch.float32, device=DEVICE))
        # model B
        for _ in range(STEP3_INTERNAL_EPOCHS):
            for i in range(0, images.shape[0], BATCH_SIZE):
                hidden_batch = hidden.detach()[i : i + BATCH_SIZE]

                label_batch = torch.tensor(
                    labels[i : i + BATCH_SIZE], dtype=torch.int64
                ).to(DEVICE)

                predicted_logits = model_B(hidden_batch)
                error = F.cross_entropy(predicted_logits, label_batch)
                error.backward()

                optim.step()
                optim.zero_grad()

        # hidden
        # freeze model parameters
        model_B.requires_grad_(False)

        for _ in range(STEP3_INTERNAL_EPOCHS):
            for i in range(0, images.shape[0], BATCH_SIZE):
                hidden_batch = hidden[i : i + BATCH_SIZE]

                label_batch = torch.tensor(
                    labels[i : i + BATCH_SIZE], dtype=torch.int64
                ).to(DEVICE)

                predicted_logits = model_B(hidden_batch)
                error = F.cross_entropy(predicted_logits, label_batch)
                error.backward()

                optim.step()
                optim.zero_grad()

        # unfreeze model parameters
        model_B.requires_grad_(True)

        # model A
        for _ in range(STEP3_INTERNAL_EPOCHS):
            for i in range(0, images.shape[0], BATCH_SIZE):
                image_batch = torch.tensor(
                    images[i : i + BATCH_SIZE], dtype=torch.float32
                ).to(DEVICE)
                hidden_batch = hidden.detach()[i : i + BATCH_SIZE]

                predicted_hidden = model_A(image_batch)
                error = F.mse_loss(predicted_hidden, hidden_batch)
                error.backward()

                optim.step()
                optim.zero_grad()
    plt.subplot(121)
    plt.plot(step3_entropy)
    plt.title("Entropy")

    plt.subplot(122)
    plt.plot(step3_accuracy)
    plt.title("Accuracy")
    plt.show()


def run_control():
    BATCH_SIZE = 4096
    EPOCHS = 200

    images, labels = load_fashion_mnist(MNIST_DATA_DIR)

    model = nn.Sequential(
        nn.Linear(784, HIDDEN_SIZE), nn.LeakyReLU(), nn.Linear(HIDDEN_SIZE, 10)
    ).to(DEVICE)
    optim = torch.optim.Adam(model.parameters(), lr=1e-3)

    def evaluate():
        # evaluation
        with torch.no_grad():
            image_tensor = torch.tensor(images, dtype=torch.float32, device=DEVICE)
            label_tensor = torch.tensor(labels, dtype=torch.int64, device=DEVICE)
            predicted_logits = model(image_tensor)
            average_entropy_loss = F.cross_entropy(predicted_logits, label_tensor)
            top1_accuracy = (
                torch.sum(label_tensor == torch.argmax(predicted_logits, dim=1))
                / label_tensor.shape[0]
            )
            return average_entropy_loss.item(), top1_accuracy.item()

    # training
    errors = []
    overall_entropy = []
    overall_accuracy = []
    for _ in tqdm(range(EPOCHS)):
        for i in range(0, images.shape[0], BATCH_SIZE):
            image_batch = torch.tensor(
                images[i : i + BATCH_SIZE], dtype=torch.float32
            ).to(DEVICE)
            label_batch = torch.tensor(
                labels[i : i + BATCH_SIZE], dtype=torch.int64
            ).to(DEVICE)

            optim.zero_grad()
            predicted_logits = model(image_batch)
            error = F.cross_entropy(predicted_logits, label_batch)

            error.backward()
            errors.append(error.item())
            optim.step()
        entropy, accuracy = evaluate()
        overall_entropy.append(entropy)
        overall_accuracy.append(accuracy)

    plt.subplot(221)
    plt.plot(errors)
    plt.title("training batch cross entropy")

    plt.subplot(222)
    plt.plot(overall_entropy)
    plt.title("Overall cross entropy")

    plt.subplot(223)
    plt.plot(overall_accuracy)
    plt.title("Overall top1 accuracy")
    plt.show()


def main():
    # run_control()
    run_context()


if __name__ == "__main__":
    main()
