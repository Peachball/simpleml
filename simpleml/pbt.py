import shutil
import pickle
import logging
import json
import random


class Agent:
    def __init__(self, hyperparams, save_dir):
        self.hyperparams = hyperparams
        self.save_dir = save_dir
        self.steps = 0
        self.metric_value = float("-inf")


class PBTScheduler:
    def __init__(
        self, population_size, sample_function, save_directory, perturb_fn=None
    ):
        """
        Arguments:
            population_size (int): Number of agents to maintain
            sample_function (fn (dict, list of dicts) -> dict or None): Function
                to randomly sample hyperparameters
            save_directory (Path): Directory to store models/training
        """
        self.agents = []
        assert population_size > 1
        self.population_size = population_size
        self.save_directory = save_directory
        self.sample_function = sample_function
        self.perturb_fn = perturb_fn
        self.history = []
        self.history_path = self.save_directory / "history.json"
        if self.history_path.exists() and self.history_path.is_file():
            with self.history_path.open() as f:
                previous_history = json.load(f)
                self.history += previous_history
        self.agent_config = self.save_directory / "config.pkl"
        if self.agent_config.exists() and self.agent_config.is_file():
            logging.info("Loading existing agents")
            with self.agent_config.open("rb") as f:
                self.agents = pickle.load(f)
        if population_size > len(self.agents):
            num_orig_agents = len(self.agents)
            logging.info("Adding new agents")
            for i in range(len(self.agents), population_size):
                agent = Agent(sample_function(), save_directory / "models" / str(i))
                # Initialize weights to existing agents
                if num_orig_agents > 0:
                    random_existing_agent = random.choice(self.agents[:num_orig_agents])
                    if agent.save_dir.exists():
                        shutil.rmtree(agent.save_dir)
                    shutil.copytree(random_existing_agent.save_dir, agent.save_dir)
                    agent.steps = random_existing_agent.steps
                else:
                    agent.save_dir.mkdir(parents=True, exist_ok=True)
                self.agents.append(agent)
        self.agents = self.agents[:population_size]
        self.last_exploit_step = min([ag.steps for ag in self.agents])
        self.total_steps = sum([ag.steps for ag in self.agents])
        self.write_agent_config()

    def explore_hyperparam(self, hyperparam):
        if self.perturb_fn is None:
            return self.sample_function()
        return self.perturb_fn(hyperparam, self.agents)

    def write_agent_config(self):
        with self.agent_config.open("wb") as f:
            pickle.dump(self.agents, f)

    def get_next_run(self):
        next_agent = min(self.agents, key=lambda ag: ag.steps)
        return next_agent

    def record_updated(self, agent, metric_value):
        # Update history
        self.history.append(
            {
                "hyperparams": agent.hyperparams,
                "step": agent.steps,
                "metric": metric_value,
            }
        )
        agent.metric_value = metric_value
        agent.steps += 1
        self.total_steps += 1

        # Check if agents are ready for exploit step
        min_agent_step = min([ag.steps for ag in self.agents])
        if min_agent_step > self.last_exploit_step:
            # Exploit using top k
            logging.info("Exploiting")
            self.last_exploit_step = min_agent_step
            number_to_adjust = int(len(self.agents) * 0.2)
            self.agents.sort(key=lambda a: a.metric_value)
            for i in range(number_to_adjust):
                bad_agent = self.agents[i]
                good_agent = self.agents[len(self.agents) - i - 1]
                shutil.rmtree(bad_agent.save_dir)
                shutil.copytree(good_agent.save_dir, bad_agent.save_dir)
                bad_agent.hyperparams = self.explore_hyperparam(good_agent.hyperparams)
                bad_agent.steps = good_agent.steps
                bad_agent.metric_value = good_agent.metric_value

        # Save current agent progress
        self.write_agent_config()

        # Save history
        with self.history_path.open("w") as f:
            json.dump(self.history, f)
