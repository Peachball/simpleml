import tensorflow as tf
import numpy as np
from keras.datasets import mnist

init = lambda std: tf.random_normal_initializer(stddev=std)


def build_generator(inp, reuse=None):
    with tf.variable_scope('generator', reuse=reuse):
        net = inp
        net = tf.layers.dense(
            net, 1024, kernel_initializer=init(tf.sqrt(1. / 784)))
        net = tf.nn.selu(net)
        net = tf.layers.dense(
            net, 7 * 7 * 128, kernel_initializer=init(tf.sqrt(1. / 1024)))
        net = tf.nn.selu(net)
        net = tf.reshape(net, tf.stack([tf.shape(inp)[0], 7, 7, 128]))
        net = tf.layers.conv2d_transpose(
            net,
            64, [4, 4], [2, 2],
            padding='same',
            kernel_initializer=init(tf.sqrt(1. / (4 * 4 * 128))))
        net = tf.nn.selu(net)
        net = tf.layers.conv2d_transpose(
            net,
            1, [4, 4], [2, 2],
            padding='same',
            kernel_initializer=init(tf.sqrt(1. / (4 * 4 * 64))))
        net = tf.layers.flatten(net)
        net = tf.sigmoid(net)
        return net


def build_discriminator(inp, reuse=None, output_dim=1):
    with tf.variable_scope('discriminator', reuse=reuse):
        net = inp
        net = tf.reshape(net, [-1, 28, 28, 1])
        net = tf.layers.conv2d(
            net,
            64, [4, 4], [2, 2],
            padding='same',
            kernel_initializer=init(tf.sqrt(1. / (4 * 4 * 1))))
        net = tf.nn.selu(net)
        net = tf.layers.conv2d(
            net,
            128, [4, 4], [2, 2],
            padding='same',
            kernel_initializer=init(tf.sqrt(1. / (4 * 4 * 64))))
        net = tf.nn.selu(net)
        net = tf.layers.flatten(net)
        net = tf.layers.dense(
            net,
            1024,
            kernel_initializer=init(
                tf.sqrt(1. / tf.cast(tf.shape(net)[1], tf.float32))))
        net = tf.nn.selu(net)
        net = tf.layers.dense(
            net, output_dim, kernel_initializer=init(tf.sqrt(1. / 1024 / 1024)))
        return net


def gen_seed(num, dim=32):
    seed = tf.random_normal([num, dim])
    return seed


def mnist_generator(batch_size=32, reshape=True):
    (X_d, _), _ = mnist.load_data()
    X_d = X_d[:,:,:,np.newaxis]
    if reshape:
        X_d = X_d.reshape(-1, 784)
    X_d = X_d.astype('float32') / 255
    while True:
        for i in range(0, len(X_d), batch_size):
            if i + batch_size > len(X_d): break
            data = X_d[i:i + batch_size]
            yield data
