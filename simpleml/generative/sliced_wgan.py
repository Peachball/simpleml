import numpy as np
import tensorflow as tf

from .utils import build_generator, gen_seed, mnist_generator


def build_discriminator(inp, reuse=None, hidden_dim=128, output_dim=1):
    with tf.variable_scope('discriminator', reuse=reuse):
        net = inp
        net = tf.layers.dense(
            net,
            256,
            kernel_initializer=tf.contrib.layers.xavier_initializer())
        net = tf.layers.dense(
            net,
            hidden_dim,
            kernel_initializer=tf.contrib.layers.xavier_initializer())
        hidden = net
        net = tf.nn.selu(net)
        net = tf.layers.dense(
            net, output_dim, kernel_initializer=tf.contrib.layers.xavier_initializer())
        return net, hidden


def get_random_projections(num, dim):
    projections = tf.random_normal([num, dim])
    projections = projections / tf.expand_dims(
        tf.norm(projections, axis=1), axis=1)
    tf.summary.scalar('debug/proj_norm', tf.global_norm([projections]))
    return projections


def sort_slices(proj, tensor):
    proj = tf.transpose(proj)
    sliced = tf.transpose(tf.matmul(tensor, proj))
    sorted_sliced, ind = tf.nn.top_k(sliced, k=tf.shape(sliced)[1])
    return sorted_sliced


def main():
    seed_dim = 8
    batch_size = 1024
    num_proj = 16
    out_dim = 16
    disc_updates = 5

    real_images = tf.placeholder(tf.float32, [None, 784])
    tf.summary.image('real_images', tf.reshape(real_images, [-1, 28, 28, 1]))
    seed_ = tf.placeholder(tf.float32, [None, seed_dim])

    pred_rl, real_lbl = build_discriminator(real_images, hidden_dim=out_dim)
    fake_img = build_generator(seed_)
    tf.summary.image('generated_images', tf.reshape(fake_img, [-1, 28, 28, 1]))
    pred_fk, fake_lbl = build_discriminator(
        fake_img, hidden_dim=out_dim, reuse=True)

    proj = get_random_projections(num_proj, out_dim)

    # shape: [num projections, batch size]
    sliced_fake = sort_slices(proj, fake_lbl)
    sliced_real = sort_slices(proj, real_lbl)

    sliced_error = tf.reduce_mean(
        tf.squared_difference(sliced_real, sliced_fake))
    fake_loss = tf.nn.sigmoid_cross_entropy_with_logits(
        labels=tf.zeros_like(pred_fk), logits=pred_fk)
    true_loss = tf.nn.sigmoid_cross_entropy_with_logits(
        labels=tf.ones_like(pred_rl), logits=pred_rl)
    disc_error = tf.reduce_mean(true_loss + fake_loss, name='mean_err')
    tf.summary.scalar('sliced_error', sliced_error)
    tf.summary.scalar('disc_error', disc_error)

    global_step = tf.Variable(0, trainable=False)
    disc_vars = tf.get_collection(
        tf.GraphKeys.TRAINABLE_VARIABLES, scope='discriminator')
    gen_vars = tf.get_collection(
        tf.GraphKeys.TRAINABLE_VARIABLES, scope='generator')
    train_disc = tf.train.AdamOptimizer(1e-4).minimize(
        disc_error, var_list=disc_vars, global_step=global_step)
    train_gen = tf.train.AdamOptimizer(1e-4).minimize(
        sliced_error, var_list=gen_vars, global_step=global_step)

    summaries = tf.summary.merge_all()
    sw = tf.summary.FileWriter('tflogs/sliced/')

    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        for i, data in enumerate(mnist_generator(batch_size)):
            results = {
                'summaries': summaries,
                'global_step': global_step,
                'err': sliced_error
            }
            if i % disc_updates != 0:
                results['train'] = train_disc
            else:
                results['train'] = train_gen
            ret = sess.run(
                results,
                feed_dict={
                    seed_: gen_seed(batch_size, seed_dim),
                    real_images: data
                })
            sw.add_summary(ret['summaries'], global_step=ret['global_step'])
            print('Error: %f' % (ret['err']))


if __name__ == '__main__':
    main()
