import tensorflow as tf

from .utils import gen_seed, mnist_generator


def encode(img, latent_dim=8):
    with tf.variable_scope('encoder'):
        net = img
        net = tf.layers.dense(net, 128)
        net = tf.nn.selu(net)
        net = tf.layers.dense(net, latent_dim)
        return net


def decode(z):
    with tf.variable_scope('decoder'):
        net = z
        net = tf.layers.dense(net, 128)
        net = tf.nn.selu(net)
        net = tf.layers.dense(net, 784)
        net = tf.nn.sigmoid(net)
        return net


def _get_sorted(proj, p):
    projected = tf.matmul(proj, tf.transpose(p))
    sorted_p, inds = tf.nn.top_k(projected, k=tf.shape(projected)[-1])
    return tf.transpose(sorted_p)


def sliced_wassertian(p, q, directions=8):
    proj = tf.random_normal([directions, tf.shape(p)[1]])
    p_proj = _get_sorted(proj, p)
    q_proj = _get_sorted(proj, q)
    return tf.squared_difference(p_proj, q_proj)


def main():
    latent_dim = 16
    batch_size = 32
    img = tf.placeholder(tf.float32, [None, 784])
    tf.summary.image('input_image', tf.reshape(img, [-1, 28, 28, 1]))

    encoded = encode(img, latent_dim=latent_dim)
    recons = decode(encoded)
    tf.summary.image('reconstructed_image', tf.reshape(recons,
                                                       [-1, 28, 28, 1]))

    recons_err = tf.reduce_mean(tf.squared_difference(img, recons))
    gen_err = tf.reduce_mean(
        sliced_wassertian(gen_seed(tf.shape(img)[0], latent_dim), encoded))
    tf.summary.scalar('recons_error', recons_err)
    tf.summary.scalar('latent_error', gen_err)

    error = gen_err + 100 * recons_err
    # error = recons_err
    tf.summary.scalar('total_error', error)

    global_step = tf.Variable(0, trainable=False)
    train = tf.train.AdamOptimizer(1e-4).minimize(
        error, global_step=global_step)
    merged_sums = tf.summary.merge_all()

    sw = tf.summary.FileWriter('tflogs/sliced_was_vae')

    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        for data in mnist_generator(batch_size):
            fetches = {
                'summaries': merged_sums,
                'train': train,
                'error': error,
                'global_step': global_step
            }
            results = sess.run(fetches, feed_dict={img: data})
            sw.add_summary(
                results['summaries'], global_step=results['global_step'])
            print('Error: %f' % results['error'])


if __name__ == '__main__':
    main()
