import os
from pathlib import Path
from scipy import signal
from scipy.io import wavfile
import math
import numpy as np
from typing import List
import resampy
from multiprocessing import Pool
import itertools

DATA_DIR = '/home/peachball/D/git/EE-DeepLearning/ncs'
DEST_DATA_DIR = './songs'


def get_songs_index() -> List[Path]:
    return [x for x in Path(DATA_DIR).iterdir() if x.suffix == '.wav']


def get_num_songs() -> int:
    return len(get_songs_index())


def get_song(index: int, dest_rate: int = 16 * 1000) -> np.ndarray:
    all_songs = get_songs_index()
    songfile = all_songs[index]
    rate, data = wavfile.read(songfile.resolve())
    if dest_rate is None:
        return rate, data
    else:
        return dest_rate, resampy.resample(data, rate, dest_rate, axis=0)


def create_downsampled_data(dest_dir: str = DEST_DATA_DIR,
                            dest_rate: int = 16 * 1000):
    '''
        DEPRECATED: Takes long time when sox could be used
    '''
    dest_path = Path(dest_dir)
    dest_path.mkdir(exist_ok=True)
    pool = Pool()
    list(
        pool.imap_unordered(_write_resample,
                            zip(
                                range(get_num_songs()),
                                itertools.repeat(dest_path),
                                itertools.repeat(dest_rate))))


def _write_resample(context):
    i, dest_path, dest_rate = context
    _, song = get_song(i, dest_rate=dest_rate)
    new_path = dest_path / '{}.wav'.format(i)
    wavfile.write(new_path, dest_rate, song)
    print("Downsampled {}".format(i))


if __name__ == '__main__':
    create_downsampled_data()
