import tensorflow as tf

from .utils import *

# arxiv is a scam: https://openreview.net/pdf?id=S1m6h21Cb

SEED_DIM = 128
GEN_DIM = 256
BATCH_SIZE = 32
DISC_STEPS = 5


def critic(hx, hsample, hreal):
    return tf.norm(hx - hsample, axis=1) - tf.norm(hx - hreal, axis=1)


def surrogate_gen(hu, hv, hxr):
    return (tf.norm(hxr - hv, axis=1) - tf.norm(hxr, axis=1) -
            tf.norm(hu - hv, axis=1) + tf.norm(hu, axis=1))


def main():
    img = tf.placeholder(tf.float32, [None, 784])
    tf.summary.image('input_image', tf.reshape(img, [-1, 28, 28, 1]))
    sample1 = build_generator(gen_seed(tf.shape(img)[0], dim=SEED_DIM))
    tf.summary.image('generated_image1', tf.reshape(sample1, [-1, 28, 28, 1]))
    sample2 = build_generator(
        gen_seed(tf.shape(img)[0], dim=SEED_DIM), reuse=True)
    tf.summary.image('generated_image2', tf.reshape(sample2, [-1, 28, 28, 1]))
    hsample1 = build_discriminator(sample1, output_dim=GEN_DIM)
    hsample2 = build_discriminator(sample2, output_dim=GEN_DIM, reuse=True)
    hreal = build_discriminator(img, output_dim=GEN_DIM, reuse=True)
    tf.summary.histogram('hreal', tf.norm(hreal, axis=1))
    tf.summary.histogram('hsample', tf.norm(hsample1, axis=1))
    tf.summary.histogram('distance', tf.norm(hreal - hsample1, axis=1))

    eps = tf.random_uniform([tf.shape(img)[0], 1])
    interpolate = eps * sample1 + (1 - eps) * img
    hinterp = build_discriminator(interpolate, output_dim=GEN_DIM, reuse=True)
    tf.summary.image('interpolated_image',
                     tf.reshape(interpolate, [-1, 28, 28, 1]))
    grads = tf.gradients(critic(hinterp, hsample2, hreal), interpolate)
    grad_penalty = tf.square(tf.norm(grads[0], axis=1) - 1.0)
    tf.summary.scalar('gradient_penalty', tf.reduce_mean(grad_penalty))

    generator_loss = (tf.norm(hreal - hsample1, axis=1) + tf.norm(
        hreal - hsample2, axis=1) - tf.norm(hsample1 - hsample2, axis=1))
    surrogate_loss = 0.5 * (surrogate_gen(hsample1, hsample2, hreal) +
                            surrogate_gen(hsample2, hsample1, hreal))
    critic_loss = 10 * grad_penalty  # - surrogate_loss
    tf.summary.scalar('generator_loss', tf.reduce_mean(generator_loss))
    tf.summary.scalar('surrogate_loss', tf.reduce_mean(surrogate_loss))
    tf.summary.scalar('critic_loss', tf.reduce_mean(critic_loss))

    total_generator_loss = generator_loss + surrogate_loss

    global_step = tf.Variable(0, trainable=False)
    disc_vars = tf.get_collection(
        tf.GraphKeys.TRAINABLE_VARIABLES, scope='discriminator')
    gen_vars = tf.get_collection(
        tf.GraphKeys.TRAINABLE_VARIABLES, scope='generator')
    train_gen = tf.train.AdamOptimizer(0).minimize(
        tf.reduce_mean(total_generator_loss),
        var_list=gen_vars,
        global_step=global_step)
    train_disc = tf.train.AdamOptimizer(1e-4).minimize(
        tf.reduce_mean(critic_loss),
        var_list=disc_vars,
        global_step=global_step)

    sw = tf.summary.FileWriter('tflogs/cramer_gan')
    summaries = tf.summary.merge_all()
    with tf.Session() as sess:
        sess.run(
            tf.global_variables_initializer(),
            feed_dict={img: next(mnist_generator(BATCH_SIZE))})
        for i, data in enumerate(mnist_generator(BATCH_SIZE)):
            fetches = {'summary': summaries, 'global': global_step}
            if i % (DISC_STEPS + 1) == 0:
                fetches['train'] = train_gen
                print('gen')
            else:
                fetches['train'] = train_disc
                print('disc')
            results = sess.run(fetches, feed_dict={img: data})
            sw.add_summary(results['summary'], global_step=results['global'])
            if i > 100:
                exit()


if __name__ == '__main__':
    main()
