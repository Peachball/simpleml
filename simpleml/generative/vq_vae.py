import tensorflow as tf
import numpy as np
from .utils import mnist_generator
import matplotlib.pyplot as plt
import shutil


def encode(x, z_dim=10):
    with tf.variable_scope("encoder", reuse=tf.AUTO_REUSE):
        x = tf.layers.flatten(x)
        x = tf.layers.dense(x, 128)
        x = tf.nn.elu(x)
        x = tf.layers.dense(x, z_dim)
        return x


def decode(x, out_dim=[28, 28, 1]):
    with tf.variable_scope("decoder", reuse=tf.AUTO_REUSE):
        x = tf.layers.dense(x, 128)
        x = tf.nn.elu(x)
        x = tf.layers.dense(x, np.prod(out_dim))
        x = tf.sigmoid(x)
        x = tf.reshape(x, [-1] + out_dim)
        return x


@tf.custom_gradient
def quantise(x, dictionary):
    def grad(dy):
        return [dy, tf.zeros_like(dictionary)]

    distances = tf.reduce_sum(
        tf.square(tf.expand_dims(x, 1) - tf.expand_dims(dictionary, 0)),
        axis=-1)
    max_inds = tf.argmin(distances, axis=1)

    return tf.nn.embedding_lookup(dictionary, max_inds), grad


def main():
    LOGDIR = "tflogs/vq_vae"
    inp = tf.placeholder(tf.float32, [None, 28, 28, 1])
    dictionary = tf.Variable(tf.random_normal(shape=[15, 10]))

    z_cont = encode(inp)
    z_quantised = quantise(z_cont, dictionary)
    recons = decode(z_quantised)
    recons_err = tf.reduce_mean(
        tf.reduce_sum(tf.square(inp - recons), axis=[1, 2, 3]))
    codebook_err = tf.reduce_sum(
        tf.square(tf.stop_gradient(z_cont) - z_quantised))
    commit_err = tf.reduce_sum(
        tf.square(z_cont - tf.stop_gradient(z_quantised)))
    total_error = recons_err + codebook_err + .25 * commit_err

    lr = tf.Variable(1e-5, trainable=False)
    opt = tf.train.GradientDescentOptimizer(lr)

    global_step = tf.Variable(0, trainable=False)
    train_op = opt.minimize(total_error, global_step=global_step)

    summaries = tf.summary.merge_all()
    sw = tf.summary.FileWriter(LOGDIR)
    shutil.rmtree(LOGDIR)

    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())

        for dat in mnist_generator(reshape=False):
            err, step, s, generated, _ = sess.run(
                [total_error, global_step, summaries, recons, [train_op]], feed_dict={inp: dat})
            sw.add_summary(s, global_step=step)
            print("Error:", err)


if __name__ == '__main__':
    main()
