import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
from keras.datasets import mnist

from .utils import build_discriminator, build_generator, gen_seed


def main():
    batch_size = 32
    epochs = 100
    gradient_penalty_magnitude = 10
    images = tf.placeholder(tf.float32, [None, 784], name='actual_images')
    seed = gen_seed(tf.shape(images)[0])
    disc_perm = tf.placeholder(tf.float32, [None, None], name='d_perm')
    gen_perm = tf.placeholder(tf.float32, [None, None], name='g_perm')

    gen_image = build_generator(seed)
    tf.summary.image('generated_images', tf.reshape(gen_image,
                                                    [-1, 28, 28, 1]))
    tf.summary.image('input_images', tf.reshape(images, [-1, 28, 28, 1]))
    cor_labels = build_discriminator(images)
    incorrect_labels = build_discriminator(gen_image, reuse=True)

    eps = tf.expand_dims(tf.random_uniform(tf.shape(seed)[0:1]), 1)
    interp_images = eps * gen_image + (1 - eps) * images
    tf.summary.image('interpolated_image',
                     tf.reshape(interp_images, [-1, 28, 28, 1]))
    interp_lbls = build_discriminator(interp_images, reuse=True)
    disc_gradients = tf.gradients(interp_lbls, interp_images)
    # grad_norm = tf.sqrt(tf.reduce_sum(disc_gradients[0] ** 2, axis=1))
    grad_norm = tf.norm(disc_gradients[0], axis=1)
    gradient_penalty = gradient_penalty_magnitude * tf.reduce_mean(
        (grad_norm - 1)**2)
    tf.summary.scalar('gradient_penalty', gradient_penalty)

    disc_error = tf.reduce_mean(cor_labels) - tf.reduce_mean(
        incorrect_labels) + gradient_penalty
    wassertian_generator_error = tf.reduce_mean(incorrect_labels)
    tf.summary.scalar('discriminator_error', disc_error)
    tf.summary.scalar('wassertian_error', wassertian_generator_error)

    gen_vars = tf.get_collection(
        tf.GraphKeys.TRAINABLE_VARIABLES, scope='generator')
    disc_vars = tf.get_collection(
        tf.GraphKeys.TRAINABLE_VARIABLES, scope='discriminator')

    global_step = tf.Variable(0, name='global_step', trainable=False)
    train_generator = tf.train.AdamOptimizer(
        1e-4, beta1=.5).minimize(
            wassertian_generator_error,
            var_list=gen_vars,
            global_step=global_step)
    train_disc = tf.train.AdamOptimizer(
        1e-4, beta1=.5).minimize(
            disc_error, var_list=disc_vars, global_step=global_step)

    summaries = tf.summary.merge_all()
    sw = tf.summary.FileWriter('tflogs/improved_wgan/')

    with tf.Session() as sess:
        (image_data, _), _ = mnist.load_data()
        image_data = image_data.reshape(-1, 784).astype('float32') / 255

        sess.run(tf.global_variables_initializer(), feed_dict={images:
                                                               image_data[:batch_size]})

        disc_steps = 5
        for epoch in range(epochs):
            i = 0
            while i + batch_size <= len(image_data):
                sums = []
                for j in range(disc_steps - 1):
                    image_batch = image_data[i:i + batch_size]
                    i += batch_size
                    _, s = sess.run(
                        [train_disc, summaries],
                        feed_dict={
                            images: image_batch
                        })
                    sums += [s]
                    if i + batch_size > len(image_data):
                        break
                if i + batch_size > len(image_data):
                    break
                image_batch = image_data[i:i + batch_size]
                i += batch_size
                results = sess.run(
                    [
                        disc_error, wassertian_generator_error,
                        train_generator, summaries, global_step
                    ],
                    feed_dict={
                        images: image_batch
                    })
                print(
                    "\r%.2f/%d done %2.2f" % (1. * i / len(image_data) + epoch,
                                              epochs, results[0] + results[1]),
                    end="")
                sw.add_summary(results[-2], global_step=results[-1])
                [sw.add_summary(s, global_step=results[-1]) for s in sums]
                i += batch_size
            sw.flush()
        print()


if __name__ == '__main__':
    main()
