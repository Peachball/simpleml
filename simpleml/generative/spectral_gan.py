import code
import hashlib
import os
import shutil

import numpy as np
import tensorflow as tf

from .utils import *


def constraint(weight):
    with tf.variable_scope(weight.name.split(':')[0], reuse=tf.AUTO_REUSE):
        kshape = weight.get_shape().as_list()
        flattened = tf.reshape(weight, [-1, kshape[-1]])
        wshape = flattened.get_shape().as_list()
        u = tf.get_variable('power', [
            wshape[0],
        ])
        v = tf.einsum('ij,i->j', flattened, u)
        v = v / tf.norm(v)

        new_u = tf.einsum('ij,j->i', flattened, v)
        new_u = new_u / tf.norm(new_u)
        ev = tf.einsum('i,ij,j->', new_u, flattened, v)
    with tf.control_dependencies([tf.assign(u, new_u)]):
        return weight / ev


def discriminator(net, use_constraint=True, mlp=False):
    ksizes = [32, 64, 96, 256]
    kwargs = {'kernel_initializer': tf.variance_scaling_initializer()}
    if use_constraint:
        kwargs.update({'kernel_constraint': constraint})
    with tf.variable_scope('discriminator', reuse=tf.AUTO_REUSE):
        if mlp:
            net = tf.layers.flatten(net)
            net = tf.layers.dense(net, 256, **kwargs)
            net = tf.nn.selu(net)
            net = tf.layers.dense(net, 1, **kwargs)
            return net
        else:
            for k in ksizes:
                net = tf.layers.conv2d(net, k, 3, 1, 'same', **kwargs)
                net = tf.nn.selu(net)
            net = tf.layers.flatten(net)
            net = tf.layers.dense(net, 1, **kwargs)
            return net


def prod(iterable):
    r = 1
    for v in iterable:
        r *= v
    return r


def generator(out_dim,
              use_constraint=True,
              seed=tf.random_normal((1, 2, 2, 256)),
              mlp=False):
    ksizes = [96, 64, 32, out_dim[-1]]
    net = seed
    kwargs = {'kernel_initializer': tf.variance_scaling_initializer()}
    if use_constraint:
        kwargs.update({'kernel_constraint': constraint})
    with tf.variable_scope('generator', reuse=tf.AUTO_REUSE):
        if mlp:
            net = tf.layers.flatten(net)
            net = tf.layers.dense(net, 256, **kwargs)
            net = tf.nn.selu(net)
            net = tf.layers.dense(net, prod(out_dim), **kwargs)
            net = tf.sigmoid(net)
            net = tf.reshape(net, [-1] + out_dim)
            return net
        else:
            for k in ksizes[:-1]:
                net = tf.layers.conv2d_transpose(net, k, 5, 2, **kwargs)
                net = tf.nn.selu(net)
            net = tf.layers.conv2d_transpose(net, ksizes[-1], 5, 2, **kwargs)
            net = tf.image.crop_to_bounding_box(net, 0, 0, out_dim[0],
                                                out_dim[1])
            net = tf.sigmoid(net)
            return net


def main():
    LOGDIR = './tflogs/spectral_gan'
    img_dim = [28, 28, 1]
    bs = 32
    MLP = True
    GC = False
    DC = True
    inp = tf.placeholder(tf.float32, [None] + img_dim)
    seed = tf.random_normal([bs, 2, 2, 256])
    seed2 = tf.random_normal([bs, 2, 2, 256])

    gen_image = generator(img_dim, seed=seed, use_constraint=GC, mlp=MLP)
    snd_gen_img = generator(img_dim, seed=seed2, use_constraint=GC, mlp=MLP)
    eps = tf.random_uniform([bs, 1, 1, 1])
    interp_img = eps * snd_gen_img + (1 - eps) * inp
    tf.summary.image('interpolate', interp_img)
    grads = tf.gradients(discriminator(
        interp_img, mlp=MLP, use_constraint=DC), interp_img)
    grad_penalty = tf.squared_difference(tf.global_norm(grads), 1)
    tf.summary.scalar('gradient_penalty', grad_penalty)
    tf.summary.image('generated_image', gen_image)
    disc_fake = discriminator(gen_image, use_constraint=DC, mlp=MLP)
    disc_real = discriminator(inp, use_constraint=DC, mlp=MLP)

    disc_vars = tf.get_collection(
        tf.GraphKeys.TRAINABLE_VARIABLES, scope='discriminator')
    gen_vars = tf.get_collection(
        tf.GraphKeys.TRAINABLE_VARIABLES, scope='generator')

    wassertian_distance = tf.reduce_mean(
        disc_real - disc_fake) + 10 * grad_penalty
    generator_error = tf.reduce_mean(disc_fake)

    tf.summary.scalar('disc_error', wassertian_distance)
    tf.summary.scalar('gen_error', generator_error)
    tf.summary.scalar('avg_real', tf.reduce_mean(disc_real))

    global_step = tf.Variable(0, trainable=False)
    gen_train_op = tf.train.AdamOptimizer(1e-4).minimize(
        generator_error, global_step=global_step, var_list=gen_vars)
    disc_train_op = tf.train.AdamOptimizer(1e-4).minimize(
        wassertian_distance, global_step=global_step, var_list=disc_vars)

    merged_summaries = tf.summary.merge_all()

    if os.path.isdir(LOGDIR):
        shutil.rmtree(LOGDIR)
    sw = tf.summary.FileWriter(LOGDIR)
    saver = tf.train.Saver()
    (x_train, y_train), _ = tf.keras.datasets.mnist.load_data()
    x_train = x_train.astype('float32') / 255
    x_train = x_train[:, :, :, None]

    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        while True:
            for i, s in enumerate(range(0, x_train.shape[0], bs)):
                img_batch = x_train[i:i + bs]
                fetches = {
                    'summaries': merged_summaries,
                    'global_step': global_step,
                    'train': disc_train_op,
                }
                if s % 5 == 0:
                    fetches['train'] = gen_train_op
                res = sess.run(fetches, feed_dict={inp: img_batch})
                sw.add_summary(
                    res['summaries'], global_step=res['global_step'])
                # saver.save(
                # sess,
                # os.path.join(LOGDIR, 'model'),
                # global_step=res['global_step'])


if __name__ == '__main__':
    main()
