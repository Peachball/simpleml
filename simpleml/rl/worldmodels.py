"""
Recreation of world models paper (for hopefully faster training)
https://arxiv.org/pdf/1803.10122.pdf
"""
import os

import cma
import cv2
import gym
import numpy as np
import retro
import scipy.special
import tensorflow as tf
from baselines.common.vec_env.subproc_vec_env import SubprocVecEnv

from .utils import space_size, get_action


def preprocess_batch(images, dst):
    return np.array(
        [cv2.resize(images[i, :, :, :], dst)
         for i in range(images.shape[0])]) / 255


class Trainer():
    """
    Trains using world models
    """

    LOGDIR = 'tflogs/worldmodels'

    def __init__(self,
                 env_name='Boxing-v4',
                 img_size=[64, 64],
                 N_env=8,
                 z_dim=32):
        self.envs = SubprocVecEnv([lambda: gym.make(env_name)] * N_env)
        self.img_dim = tuple(img_size)
        self.z_dim = z_dim

        self._rollout_inp = tf.placeholder(tf.float32,
                                           [None, None] + img_size + [3])

        # VAE ops
        self.inp = tf.reshape(self._rollout_inp, [-1] + img_size + [3])
        self.training = tf.Variable(False, trainable=False)
        tf.summary.image('original_image', self.inp, collections=['vae'])
        self.z, mu, sigma = self.vae_encode(self.inp, N=z_dim)
        self.recons = self.vae_decode(self.z)
        tf.summary.image('recons_image', self.recons, collections=['vae'])

        # Controller ops
        act_size = space_size(self.envs.action_space)
        self.c_weight = tf.placeholder(tf.float32,
                                       [None, (z_dim + 1) * act_size])
        self.zero_c_weights = tf.Variable(
            np.zeros(((z_dim + 1) * act_size, )), trainable=False)
        self.moves = self.controller(self.z)

        # Variables
        self.vae_encode_vars = tf.get_collection(
            tf.GraphKeys.TRAINABLE_VARIABLES, scope='VAE_encode')
        self.vae_decode_vars = tf.get_collection(
            tf.GraphKeys.TRAINABLE_VARIABLES, scope='VAE_decode')

        # Error ops
        recons_error = tf.reduce_mean(
            tf.reduce_sum(tf.square(self.inp - self.recons), axis=[1, 2, 3]))
        tf.summary.scalar('recons_error', recons_error, collections=['vae'])
        kl_error = .5 * tf.reduce_mean(
            tf.reduce_sum(tf.exp(sigma) + tf.square(mu) - 1 - sigma, axis=1))
        tf.summary.scalar('vae_kl_error', kl_error, collections=['vae'])
        self.vae_error = recons_error + kl_error
        tf.summary.scalar('vae_error', self.vae_error, collections=['vae'])

        # Optimization ops
        self.global_step = tf.Variable(0, trainable=False, name='global_step')
        self.lr = tf.Variable(1e-3, trainable=False)
        self.opt = tf.train.GradientDescentOptimizer(self.lr)

        # Need to run batch normalization using update ops:
        # link: https://www.tensorflow.org/api_docs/python/tf/layers/batch_normalization
        update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
        with tf.control_dependencies(update_ops):
            self.vae_train_op = self.opt.minimize(
                self.vae_error,
                var_list=self.vae_encode_vars + self.vae_decode_vars,
                global_step=self.global_step)
        self.vae_summaries = tf.summary.merge_all('vae')

    def bn(self, net):
        return tf.layers.batch_normalization(net, training=self.training)

    def vae_encode(self, img, N=32):
        """
        Architecture taken from world models paper:
        """
        with tf.variable_scope('VAE_encode', reuse=tf.AUTO_REUSE):
            net = img
            net = tf.layers.conv2d(net, 32, 4, 2)
            net = self.bn(net)
            net = tf.nn.leaky_relu(net, alpha=0.01)

            net = tf.layers.conv2d(net, 64, 4, 2)
            net = self.bn(net)
            net = tf.nn.leaky_relu(net, alpha=0.01)

            net = tf.layers.conv2d(net, 128, 4, 2)
            net = self.bn(net)
            net = tf.nn.leaky_relu(net, alpha=0.01)

            net = tf.layers.conv2d(net, 256, 4, 2)
            net = self.bn(net)
            net = tf.nn.leaky_relu(net, alpha=0.01)

            features = tf.layers.flatten(net)
            mu = tf.layers.dense(features, N)
            mu = self.bn(mu)
            sigma = tf.layers.dense(features, N)
            sigma = self.bn(sigma)

            # Sigma divided by 2 because sigma here represents log variance, so
            # it needs to be converted to stddev first
            # link: https://wiseodd.github.io/techblog/2016/12/10/variational-autoencoder/
            z = mu + tf.random_normal(tf.shape(sigma)) * tf.exp(sigma / 2)
            return z, mu, sigma

    def vae_decode(self, z):
        with tf.variable_scope('VAE_decode', reuse=tf.AUTO_REUSE):
            net = z
            net = tf.layers.dense(net, 1024)
            net = self.bn(net)
            net = tf.reshape(net, [-1, 1, 1, 1024])

            net = tf.layers.conv2d_transpose(net, 128, 5)
            net = self.bn(net)
            net = tf.nn.leaky_relu(net, alpha=0.01)

            net = tf.layers.conv2d_transpose(net, 64, 5, strides=2)
            net = self.bn(net)
            net = tf.nn.leaky_relu(net, alpha=0.01)

            net = tf.layers.conv2d_transpose(net, 32, 6, strides=2)
            net = self.bn(net)
            net = tf.nn.leaky_relu(net, alpha=0.01)

            net = tf.layers.conv2d_transpose(net, 3, 6, strides=2)
            net = self.bn(net)
            net = tf.sigmoid(net)

            return net

    def memory_rnn(self, z_sequences):
        with tf.variable_scope('memory_rnn', reuse=tf.AUTO_REUSE):
            pass

    def controller(self, z):
        # No variable_scope needed here, because no variables are created
        one_col = tf.ones([tf.shape(z)[0], 1])
        new_z = tf.concat([z, one_col], axis=1)
        cshape = tf.shape(self.c_weight)
        weights = tf.reshape(self.c_weight, [
            cshape[0],
            tf.shape(new_z)[1],
            space_size(self.envs.action_space)
        ])
        moves = tf.einsum('bjk,bj->bk', weights, new_z)
        return moves

    def train(self):
        self.saver = tf.train.Saver()
        self.sw = tf.summary.FileWriter(self.LOGDIR)
        with tf.Session() as sess:
            latest = tf.train.latest_checkpoint(self.LOGDIR)
            sess.run(tf.global_variables_initializer())
            if latest is not None:
                print('Restored previous model')
                self.saver.restore(sess, latest)
            self.sw.add_graph(sess.graph)
            print('Training variational autoencoder')
            self.train_vae(sess)
            print('Using cma to train controller')
            self.train_controller(sess)

    def generate_images(self, num=10000):
        imgs = []
        self.envs.reset()
        while True:
            obs, _, done, _ = self.envs.step([
                self.envs.action_space.sample()
                for _ in range(self.envs.num_envs)
            ])
            imgs += list(preprocess_batch(obs, self.img_dim))
            if len(imgs) > num:
                imgs = np.array(imgs)
                return imgs

    def train_vae(self, sess):
        """
        Trains VAE until error no longer decreases
        """
        bs = 32
        lowest_lr = 1e-5
        lr = 1e-3
        data = self.generate_images()
        np.random.shuffle(data)
        prev_error = 1e8
        while True:
            total_error = 0
            for i in range(0, data.shape[0], bs):
                fetches = {
                    'summary': self.vae_summaries,
                    'step': self.global_step,
                    'train': self.vae_train_op,
                    'error': self.vae_error
                }
                res = sess.run(
                    fetches,
                    feed_dict={
                        self.inp: data[i:i + bs],
                        self.training: True,
                        self.lr: lr
                    })
                self.sw.add_summary(res['summary'], global_step=res['step'])
                total_error += res['error']
            print('New error: %f' % (total_error / (data.shape[0] / bs)))
            self.saver.save(
                sess,
                os.path.join(self.LOGDIR, 'model'),
                global_step=sess.run(self.global_step))
            if total_error < prev_error:
                prev_error = total_error
            else:
                if lr < lowest_lr:
                    break
                lr /= 2

    def train_controller(self, sess):
        optimizer = cma.CMAEvolutionStrategy(sess.run(self.zero_c_weights), 1)
        while not optimizer.stop():
            solutions = optimizer.ask()

            # break solutions up into batches and get rewards
            rewards = []
            for i in range(0, len(solutions), self.envs.num_envs):
                rewards += list(
                    self.evaluate_solutions(
                        sess, solutions[i:i + self.envs.num_envs]))
            print('Best solution: %f' % (-min(rewards)))
            optimizer.tell(solutions, rewards)

    def evaluate_solutions(self, sess, solutions, trials=1):
        total_rewards = np.zeros((len(solutions), ))
        games = np.zeros((len(solutions), ))
        obs = preprocess_batch(self.envs.reset()[:len(solutions)],
                               self.img_dim)
        while np.sum(games) < games.shape[0] * trials:
            act = sess.run(
                self.moves,
                feed_dict={
                    self.c_weight: solutions,
                    self.inp: obs
                })
            act = get_action(act, self.envs.action_space)
            obs, rew, done, info = self.envs.step(
                list(act) + [
                    self.envs.action_space.sample()
                    for _ in range(self.envs.num_envs - len(solutions))
                ])
            obs = preprocess_batch(obs[:len(solutions)], self.img_dim)

            # Note total_rewards is negative because we want to "minimize" the
            # result of this function
            total_rewards -= (games < trials) * rew[:len(solutions)]
            games += done[:len(solutions)]
        return total_rewards


def main():
    trainer = Trainer()
    trainer.train()


if __name__ == '__main__':
    main()
