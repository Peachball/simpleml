"""
Test if using inverse dynamic model technique from https://arxiv.org/pdf/1705.05363.pdf
can work to learn a powerful world model for environments
"""
import argparse
import unittest

import gym
import numpy as np
import torch

from simpleml.utils import flatten


class VisualEncoder(torch.nn.Module):
    def __init__(self, inp_shape, out_dim, config={
        'hidden_config': [32, 64, 128, 256]
    }):
        super(VisualEncoder, self).__init__()
        assert len(inp_shape) == 3

        channel_sizes = [inp_shape[0]] + config['hidden_config']
        layers = []
        for i in range(len(channel_sizes) - 1):
            layers.append(torch.nn.Conv2d(channel_sizes[i], channel_sizes[i+1],
                                          4, stride=2))
            layers.append(torch.nn.SELU())
        self.conv = torch.nn.Sequential(*layers)

        fc_inp_size = self._calculate_fc_size(self.conv, inp_shape)
        self.ff = torch.nn.Linear(fc_inp_size, out_dim)

    def _calculate_fc_size(self, conv, inp_shape):
        sample_input = torch.zeros(*([1] + inp_shape))
        result = self.conv(sample_input)
        return np.prod(result.size()[1:])

    def forward(self, x):
        original_dims = x.size()
        conv_features = self.conv(x.view(-1, *original_dims[-3:]))
        fc_features = self.ff(flatten(conv_features))
        return fc_features.view(*original_dims[:-3], -1)


class TestVisualEncoder(unittest.TestCase):
    def testShapes(self):
        sample_shapes = [
                ((29, 3, 64, 64), (29, 256)),
                ((5, 7, 8, 39, 3, 64, 64), (5, 7, 8, 39, 256))]
        for inp_shape, out_shape in sample_shapes:
            encoder = VisualEncoder(list(inp_shape[-3:]), 256)
            sample_input = torch.randn(*inp_shape)
            sample_output = encoder(sample_input)
            self.assertTupleEqual(out_shape, tuple(sample_output.size()))


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--env', type=str, default='CarRacing-v0')

    return parser.parse_args()


def test_model(env):
    pass


def main():
    args = parse_args()
    test_model(gym.make(args.env))


if __name__ == '__main__':
    main()
