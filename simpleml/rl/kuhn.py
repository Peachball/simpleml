import random
import torch

game_descr = {
    "c": {"c": (1, 1), "b": {"f": (1, 2), "c": (2, 2)}},
    "b": {"f": (2, 1), "c": (2, 2)},
}


class State:
    def __init__(self, p1_card, p2_card, str_rep):
        self.str_rep = str_rep
        self.p1_card = p1_card
        self.p2_card = p2_card

    def get_node(self):
        n = game_descr
        for c in self.str_rep:
            n = n[c]
        return n

    def is_end(self):
        node = self.get_node()
        return isinstance(node, tuple)

    def reward(self):
        p1_invest, p2_invest = self.get_node()
        if self.p1_card > self.p2_card:
            return (p2_invest, -p2_invest)
        else:
            return (-p1_invest, p1_invest)

    def actions(self):
        node = self.get_node()
        return sorted(node.keys())

    def make_action(self, action):
        return State(self.p1_card, self.p2_card, self.str_rep + action)

    def cur_player_rep(self):
        if len(self.str_rep) % 2 == 0:
            card = self.p1_card
        else:
            card = self.p2_card
        return str(card) + self.str_rep

    @staticmethod
    def sample_state():
        p1_card, p2_card = random.sample([0, 1, 2], 2)
        return State(p1_card, p2_card, "")


def return_zeros():
    return torch.tensor([0, 0], dtype=torch.float32)


class TabularModel:
    def __init__(self, initialize=return_zeros):
        self.weights = {}
        self.initialize = initialize

    def get_move_probs(self, state):
        if state.str_rep not in self.weights:
            self.weights[state.str_rep] = self.initialize()
        return self.weights[state.str_rep]
