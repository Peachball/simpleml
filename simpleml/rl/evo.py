import cma
import math
import gym
import numpy as np
import operator
from functools import reduce
from multiprocessing import Pool, Process, Queue
from sklearn.manifold import TSNE
import matplotlib.pyplot as plt
import time


def prod(vals):
    return reduce(operator.mul, vals, 1)


def plot_history(points):
    all_points = [(v, p) for i, l in enumerate(points) for v, p in l]
    # calculate percentile
    all_points.sort(key=lambda v: v[0])
    colors = list(zip(*all_points))[0]
    smallest_c = min(colors)
    largest_c = max(colors)
    embedded_points = TSNE().fit_transform(list(zip(*all_points))[1])
    plot_points = []
    colors = []
    for (i, _), p in zip(enumerate(all_points), embedded_points):
        plot_points += [p]
        # colors += [(i - smallest_c) / (largest_c - smallest_c)]
        colors += [i / len(all_points)]
    print("Seen plot points:", len(plot_points))
    plt.scatter(*zip(*plot_points), c=colors, s=4)


def main():
    ENV_NAME = 'LunarLander-v2'
    hsize = 8
    use_hidden = False
    popsize = 50
    plt.ion()
    envs = [gym.make(ENV_NAME) for _ in range(8)]
    env = envs[0]
    odim = prod(env.observation_space.shape)
    if type(env.action_space) is gym.spaces.discrete.Discrete:
        adim = env.action_space.n
    else:
        adim = env.action_space.sample().size

    def get_action(params, observation):
        w1, b1 = (hsize, odim), (hsize, )
        if not use_hidden:
            w1, b1 = (adim, odim), (adim, )
        w2, b2 = (adim, hsize), (adim, )
        all_weight_shapes = [w1, b1, w2, b2]
        if not use_hidden:
            all_weight_shapes = all_weight_shapes[:2]
        i = 0
        wvals = []
        for ws in all_weight_shapes:
            wsize = prod(ws)
            wvals.append(params[i:i + wsize].reshape(ws))
            i += wsize
        if use_hidden:
            w1, b1, w2, b2 = wvals
        else:
            w1, b1 = wvals
        hidden = np.maximum(np.dot(w1, observation) + b1, 0)
        res = hidden
        if use_hidden:
            res = np.dot(w2, hidden) + b2
        if type(env.action_space) is gym.spaces.box.Box:
            return np.clip(res, env.action_space.low, env.action_space.high)
        else:
            return np.argmax(res)

    def obj_fn(params, trials=5, env=env):
        rewards = []
        for _ in range(trials):
            total_reward = 0
            o = env.reset()
            done = False
            while not done:
                o, r, done, info = env.step(get_action(params, o))
                total_reward += r
            rewards.append(total_reward)
        return -sum(rewards) / len(rewards)

    def display_solution(params):
        o = env.reset()
        done = False
        while not done:
            env.render()
            o, r, done, info = env.step(get_action(params, o))

    def worker(param_list, env, dest):
        for i, p in param_list:
            dest.put((i, obj_fn(p, env=env)))

    zero_param = np.zeros((odim * hsize + adim + hsize * adim + hsize, ))
    if not use_hidden:
        zero_param = zero_param[:odim * adim + adim]
    o = env.reset()
    if popsize is not None:
        optimizer = cma.CMAEvolutionStrategy(zero_param, 1, {'popsize': popsize})
    else:
        optimizer = cma.CMAEvolutionStrategy(zero_param, 1)
    iteration = -1
    solution_history = []
    while not optimizer.stop():
        solutions = optimizer.ask()
        esol = list(enumerate(solutions))
        rup = lambda i: math.ceil(len(esol) / 8 * i)
        dest = Queue()
        processes = [
            Process(
                target=worker, args=(esol[rup(i):rup(i + 1)], envs[i], dest))
            for i in range(8)
        ]
        [p.start() for p in processes]
        obj_eval = []
        while len(obj_eval) < len(esol):
            obj_eval.append(dest.get())
        obj_eval.sort()
        obj_eval = list(zip(*obj_eval))[1]
        # obj_eval = list(map(obj_fn, solutions))

        solution_history += [list(zip(obj_eval, solutions))]
        optimizer.tell(solutions, obj_eval)
        iteration += 1
        optimizer.disp()
        optimizer.logger.add()
        optimizer.manage_plateaus()
        if iteration % 10 == 0:
            plt.cla()
            plot_history(solution_history)
        if iteration % 2 == 0:
            plt.pause(.01)
        if iteration % 2 == 0:
            best_ind = np.argmin(obj_eval)
            display_solution(solutions[best_ind])
            print("Best solution:", -obj_eval[best_ind])
            print('Popsize:', optimizer.popsize)
            print('Best norm:', np.linalg.norm(solutions[best_ind]))
    optimizer.logger.plot()


if __name__ == '__main__':
    main()
