import gym
from bayes_opt import BayesianOptimization
import numpy as np
import multiprocessing as mp
import warnings
import sys
import os
import logging
import pandas as pd
import os
from sklearn.gaussian_process import GaussianProcessRegressor
import scipy.optimize
import time
gym.logger.set_level(gym.logger.DISABLED)


def get_action(w, b, obs):
    return np.argmax(np.dot(obs, w) + b)


def test_weights(w, b, render=False):
    env = gym.make('LunarLander-v2')
    obs = env.reset()
    total_reward = 0
    done = False
    while not done:
        act = get_action(w, b, obs)
        if render:
            env.render()
        obs, reward, done, _ = env.step(act)
        total_reward += reward
    env.close()
    return total_reward


def test_params(**kwargs):
    w = np.array([kwargs['w_%d' % i] for i in range(32)]).reshape(8, 4)
    b = np.array([kwargs['b_%d' % i] for i in range(4)])

    return test_weights(w, b)


def main():
    weight_ranges = {'w_%d' % d: (-1e+3, 1e+3) for d in range(32)}
    weight_ranges.update({'b_%d' % d: (-1e+3, 1e+3) for d in range(4)})

    bo = BayesianOptimization(test_params, weight_ranges, verbose=0)
    eval_file = 'bayes_evaluations.csv'
    if os.path.isfile(eval_file):
        bo.initialize_df(pd.read_csv(eval_file, skipinitialspace=True))
    while True:
        bo.maximize(n_iter=1, alpha=40)
        bo.points_to_csv(eval_file)
        print('Latest Reward', bo.res['all']['values'][-1])
        print('Best Reward', bo.res['max']['max_val'])


def thompson_sampling():
    random_w = np.random.uniform(-1, 1, (8, 4))
    random_b = np.random.uniform(-1, 1, 4)
    rewards = []
    for i in range(100):
        rewards.append(test_weights(random_w, random_b))
    print("Standard deviation:", np.std(rewards))
    gp = GaussianProcessRegressor(
        alpha=10, normalize_y=True, n_restarts_optimizer=3)
    history = []
    cur_best = None
    best_x = None
    for i in range(1000):
        tic = time.time()
        # Find maximum from sampled process
        large_sample = np.random.uniform(-1, 1, (1000, 36))
        y = gp.sample_y(large_sample, random_state=i)
        best_reward = (large_sample[y[:,0].argmax()], y[:,0].max())
        total_fevals = 0
        for j in range(5):
            if best_x is not None:
                x0 = best_x + np.random.normal(0, .1, 36)
            else:
                x0 = np.random.uniform(-1, 1, 36)
            result = scipy.optimize.minimize(
                fun=lambda v: -gp.sample_y(v[None, :], random_state=i)[0],
                x0=x0,
                method='L-BFGS-B',
                bounds=[(-1, 1) for _ in range(36)])
            total_fevals += result.nfev
            if best_reward is None or -result.fun > best_reward[1]:
                best_reward = (result.x, -result.fun)
        print("Evaluations:", total_fevals)

        optimize_tic = time.time()
        measured_reward = test_weights(best_reward[0][:32].reshape(8, 4),
                                       best_reward[0][-4:])
        toc = time.time()
        history.append((best_reward[0], measured_reward))
        print("Reward: {}\tOptimize Time: {}\tEnv time: {}".format(
            measured_reward, optimize_tic - tic, toc - optimize_tic))
        if cur_best is None or measured_reward > cur_best:
            print("Current best!")
            print(best_x)
            cur_best = measured_reward
            best_x = best_reward[0]
            # test_weights(
            # best_reward[0][:32].reshape(8, 4),
            # best_reward[0][-4:],
            # render=True)

        gp.fit(*zip(*history))

    best = None
    for i in range(100):
        x0 = np.random.uniform(-1, 1, 36)
        reward_func = lambda v: -test_weights(v[:32].reshape(8, 4), v[-4:])
        print(reward_func(x0))
        optimized = scipy.optimize.minimize(
            fun=reward_func,
            x0=x0,
            method='L-BFGS-B',
            bounds=[(-1, 1) for _ in range(36)],
            options={'disp': True})
        print(optimized)
        if best is None or -optimized.fun > best[1]:
            best = (optimized.x, optimized.fun)
    w = best[0][:32].reshape(8, 4)
    b = best[0][-4:]
    test_weights(w, b, render=True)


if __name__ == '__main__':
    thompson_sampling()
