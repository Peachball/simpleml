import gym
from typing import List
import numpy as np
import scipy.special

EPSILON = 1e-5


def get_action(weights: np.ndarray, action_space):
    if isinstance(action_space, gym.spaces.Discrete):
        if len(weights.shape) > 1:
            return np.argmax(weights, axis=1)
        return np.argmax(weights)
    if isinstance(action_space, gym.spaces.MultiBinary):
        return (weights > 0)
    if isinstance(action_space, gym.spaces.Box):
        return scipy.special.expit(weights) * (
            action_space.high - action_space.low) - action_space.low
    raise NotImplementedError("{} space not implemented yet".format(
        type(action_space).__name__))


def random_action(weights: np.ndarray, action_space):
    if isinstance(action_space, gym.spaces.Discrete):
        # Check if weights have been normalized
        if np.abs(np.sum(weights) - 1) > EPSILON:
            weights = np.exp(weights)
            weights = weights / weights.sum()
        return np.random.choice(weights.shape[-1], p=weights)
    raise NotImplementedError("{} space not implemented yet".format(
        type(action_space).__name__))


def space_shape(space) -> List[int]:
    """
    Calculates the shape of any space
    """
    if isinstance(space, gym.spaces.Discrete):
        return [space.n]
    if isinstance(space, gym.spaces.Box):
        return list(space.shape)
    raise NotImplementedError("{} space not implemented yet".format(
        str(type(space))))


def space_size(space) -> int:
    """
    Calculates total number of dimensions of an gym.Space

    Arguments:
        space (gym.Space): Space to analyze size of

    Returns:
        int: total size/number of dimensions of space
    """
    return np.prod(space_shape(space)).item()
