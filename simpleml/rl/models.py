import torch
import gym

class StateEmbed(torch.nn.Module):
    """
    Takes in arbitrary space, and transforms it into a vector

    Should eventually work for all spaces
    """
    def __init__(self, space: gym.spaces.Space, out_dim: int):
        if isinstance(space, gym.spaces.Box):
            pass
