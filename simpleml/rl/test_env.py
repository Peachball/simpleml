import argparse
import time

import gym

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="simulate openai environmnt")
    parser.add_argument('env', type=str, help='ID of environment to simulate')
    parser.add_argument('--max-steps', type=int, default=10**6,
                        help='maximum number of environment steps to simulate')
    args = parser.parse_args()

    env = gym.make(args.env)
    env.reset()
    for _ in range(args.max_steps):
        env.render()
        time.sleep(0.05)
        _, _, done, _ = env.step(env.action_space.sample())
        if done:
            break
