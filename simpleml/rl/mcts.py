import abc

class Environment(abc.ABC):
    @abc.abstractmethod
    def undo(self):
        pass

    @abc.abstractmethod
    def apply_action(self, act):
        pass

    @abc.abstractmethod
    def valid_actions(self) -> List[str]:
        pass

    @abc.abstractmethod
    def get_state(self):
        pass

    @abc.abstractmethod
    def is_terminal(self):
        pass

    @abc.abstractmethod
    def get_reward(self):
        pass


class Model(abc.ABC):
    def value(self, state) -> float:
        """
        Given a state, calculate the expected value

        Arguments:
            state: current state of proof environment

        Returns:
            float representing expected value
        """
        pass

    def policy(self, state):
        """
        Given state, calculate policy probabilities to guide search

        Arguments:
            state: current state of proof environment

        Returns:
            function from string -> float, representing the normalized
            probability that a given action should be taken

            If function is not normalized, the algorithm will probably still
            work, just deviates from assumptions in literature
        """
        pass


class MCTSEdgeInfo:
    def __init__(self, p):
        self.n = 0
        self.q = 0
        self.p = p
        self.node = MCTSNode()


class MCTSNode:
    def __init__(self):
        # Empty edges means that current node has not been expanded yet
        self.edges = None

    def visit_count(self):
        return sum(k.n for k in self.edges.values())

    def value(self):
        return sum(k.q for k in self.edges.values()) / self.visit_count()

    def is_leaf(self):
        return self.edges is None

    def initialize_actions(self, action_prob_mapping: Dict[str, float]):
        """
        Initializes given node with action probabilities if current node is
        uninitialized

        Arguments:
            action_prob_mapping (dict str float): Dictionary with keys as
                actions and values being probabilities of choosing that action
        """
        if self.edges is None:
            self.edges = {
                a: MCTSEdgeInfo(p)
                for (a, p) in action_prob_mapping.items()
            }
        else:
            warnings.warn("Not initializing edges")


class MCTS:
    def __init__(self,
                 model: Model,
                 env: Environment,
                 c1: float = 1.25,
                 c2: float = 1,
                 initial_node: MCTSNode = None):
        """
        Arguments:
            model: Model to guide search
            env: Environment to perform search on
            c1, c2: constants to adjust weight of model probabilities, as
                described in muzero paper
        """
        self.model = model
        if initial_node is None:
            self.root = MCTSNode()
        else:
            self.root = initial_node
        self.env = env
        self.max_q = 0
        self.min_q = -1
        self.c1 = c1
        self.c2 = c2

    def run_simulation(self):
        node = self.root
        path = []
        total_reward = 0
        while not node.is_leaf() and not self.env.is_terminal():
            edge_values = {}
            total_visit_count = node.visit_count()
            for a, e in node.edges.items():
                log_numer = total_visit_count + self.c2 + 1
                log_counts = self.c1 + math.log(log_numer / self.c2)
                visit_weight = total_visit_count**.5 / (1 + e.n) * log_counts
                norm_q = (e.q - self.min_q) / (self.max_q - self.min_q)
                edge_values[a] = (norm_q + e.p * visit_weight)
            best_act, best_v = max(edge_values.items(), key=lambda i: i[1])
            if best_v == float('-inf'):
                break
            self.env.apply_action(best_act)
            best_edge = node.edges[best_act]
            path.append(best_edge)
            node = best_edge.node
            total_reward += self.env.get_reward()

        # env should be same as state associated with node
        if self.env.is_terminal():
            unnormalized_q = total_reward
        else:
            all_actions = self.env.valid_actions()

            state = self.env.get_state()
            prob_func = self.model.policy(state)

            action_prob_mapping = {a: prob_func(a) for a in all_actions}
            node.initialize_actions(action_prob_mapping)
            value = self.model.value(state)
            unnormalized_q = value + total_reward

        for e in path:
            self.env.undo()
            e.q = (e.n * e.q + unnormalized_q) / (e.n + 1)
            self.max_q = max(self.max_q, e.q)
            self.min_q = min(self.min_q, e.q)
            e.n += 1

    def apply_action(self, act):
        self.env.apply_action(act)
        self.root = self.root.edges[act].node

    def get_best_action(self):
        return max(self.root.edges.items(), key=lambda v: v[1].n)[0]

    def get_action_dist(self):
        total = self.root.visit_count()
        return {a: v.n / total for a, v in self.root.edges.items()}

    def get_value(self):
        return self.root.value()
