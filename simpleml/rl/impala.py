"""
IMPALA
Link: https://arxiv.org/pdf/1802.01561.pdf
"""
import torch
import numpy as np
import abc
import collections
import gym
import multiprocessing
import copy
import time

from . import utils

ENV = 'LunarLander-v2'
HIDDEN_DIM = 32
WORKERS = 1
NSTEP = 16
BATCH_SIZE = 32


class Policy(torch.nn.Module):
    def __init__(self, env):
        super(Policy, self).__init__()
        self.lstm = torch.nn.LSTMCell(
            input_size=utils.space_size(env.observation_space),
            hidden_size=HIDDEN_DIM)
        self.policy_head = torch.nn.Linear(HIDDEN_DIM,
                                           utils.space_size(env.action_space))
        self.value_head = torch.nn.Linear(HIDDEN_DIM, 1)

    def forward(self, x, hidden=None):
        if hidden is None:
            hidden = self.zero_state(x.shape[0])
        h, c = self.lstm(x, hidden)
        p = self.policy_head(h)
        v = self.value_head(h)
        return p, v, (h, c)

    def zero_state(self, batch_size):
        return (torch.zeros((batch_size, HIDDEN_DIM)),
                torch.zeros((batch_size, HIDDEN_DIM)))


def train_runner(policy, stop, run_queue, env_fn, sync):
    env = env_fn()
    while True:
        if stop.poll():
            return
        states, actions, rewards, act_probs = [], [], [], []
        obs = env.reset()
        states.append(obs)
        done = False
        hidden = None
        sync(policy)
        while not done:
            act_weights, _, hidden = policy(
                torch.from_numpy(obs[None, :].astype('float32')), hidden)
            act_probs.append(act_weights)
            act_weights = act_weights.detach().numpy().reshape(
                utils.space_shape(env.action_space))
            act = utils.random_action(act_weights, env.action_space)
            actions.append(act)

            obs, r, done, _ = env.step(act)
            states.append(obs)
            rewards.append(r)
            if stop.poll():
                return
        run_queue.put((np.array(states), np.array(actions), np.array(rewards),
                       np.array(act_probs)))


class ImpalaTrainer:
    def __init__(self, env_fn):
        sample_env = gym.make(ENV)
        policy = Policy(sample_env)
        self._policy = policy
        cpu_policy = copy.deepcopy(policy)
        cpu_policy.cpu()
        for param in cpu_policy.parameters():
            param.requires_grad = False
        sample_env.close()

        kill_pipe, cpipe = multiprocessing.Pipe()

        self.optimizer = torch.optim.SGD(policy.parameters(), lr=1e-3)

    def _sync(self, policy):
        for global_param, param in zip(self._policy.parameters(),
                                       policy.parameters()):
            param.copy_(global_param)

    def train(self):
        run_queue = multiprocessing.Queue()
        procs = [
            multiprocessing.Process(
                target=train_runner,
                args=(copy.deepcopy(cpu_policy), cpipe, run_queue, env_fn,
                      self._sync)) for _ in range(WORKERS)
        ]

        # Starting processes
        for p in procs:
            p.start()

        runs = []
        for i in range(BATCH_SIZE):
            runs.append(run_queue.get())

        while True:
            pass


def main():
    env_fn = lambda: gym.make(ENV)
    trainer = ImpalaTrainer(env_fn)


if __name__ == '__main__':
    main()
