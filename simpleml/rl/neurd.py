"""
Neural replicator dynamics
Arxiv: https://arxiv.org/pdf/1906.00190.pdf
"""
from .kuhn import TabularModel, State
from simpleml.utils import sample_from_logits
from torch.nn import functional as F
import itertools
import random


def sample_trajectories(policy, num_traj=100):
    trajectories = []
    for _ in range(num_traj):
        state = State.sample_state()
        rollout = []
        while not state.is_end():
            logits = policy.get_move_probs(state)
            action_ind = sample_from_logits(logits)
            action = state.actions()[action_ind]
            new_state = state.make_action(action)

            rollout.append((state, action))
            state = new_state
        trajectories.append(rollout)
    return trajectories


def main():
    timesteps = 100

    policy = TabularModel()
    qvalue = TabularModel()
    for t in range(timesteps):
        trajectories = sample_trajectories(policy)
        all_state_action_pairs = list(itertools.chain.from_iterable(trajectories))
        random.shuffle(all_state_action_pairs)
        print([s.cur_player_rep() for (s, _) in all_state_action_pairs[:10]])


if __name__ == "__main__":
    main()
