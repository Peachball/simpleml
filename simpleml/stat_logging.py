"""
Quick and dirty metric graphing + logging utilities

Do not use this for things other than quick queries
"""
from collections import defaultdict

GLOBAL_DATASET = defaultdict(dict)
ITER = 0


def log_metric(name, value):
    GLOBAL_DATASET[ITER][name] = value


def advance_iter():
    global ITER
    ITER += 1


def get_dataset_as_list(key=None):
    keys = sorted(GLOBAL_DATASET.keys())

    def index_fn(v):
        if key is None:
            return v
        else:
            return v[key]

    return [index_fn(GLOBAL_DATASET[ind]) for ind in keys]
