import torch
from torch import nn
from pathlib import Path
import string
from simpleml.pbt import PBTScheduler
from simpleml.supervised.bptt import Model
import random
import numpy as np
from tqdm import tqdm
from torch.nn import functional as F
import itertools
import math

DATA = Path("./data/tale-of-two-cities.txt")
MODEL_DIR = Path("./data/bptt_pbt_v4")
dev = torch.device("cuda")
EVAL_STEPS = 10**4


def hyperparam_sampler():
    return {
        "lr": 10 ** np.random.uniform(-5, 0),
        "seq_len": random.choice(list(range(32, 256, 16))),
        "batch_size": random.choice(list(range(32, 256, 16))),
    }


def hyperparam_perturber(params, _):
    def random_shift(existing, value):
        index = existing.index(value)
        index += random.choice([-1, 0, 1])
        index = max(0, min(index, len(existing) - 1))
        return existing[index]

    return {
        "lr": random.choice([0.8, 1.25]) * params["lr"],
        "seq_len": random_shift(list(range(32, 256, 16)), params["seq_len"]),
        "batch_size": random_shift(list(range(32, 256, 16)), params["batch_size"]),
    }


def create_hyperparam_functions_from_config(config):
    config_keys = list(config.keys())
    def sampler():
        params = {}
        for k, v in config.items():
            if isinstance(v, tuple):
                params[k] = 10 ** random.uniform(v[0], v[1])
            elif isinstance(v, list):
                params[k] = random.choice(v)
            else:
                raise ValueError("Unknown config type")
        return params

    def perturber(prev_params, all_agents):
        params = {}
        for k, v in prev_params.items():
            if isinstance(config[k], tuple):
                params[k] = random.choice([0.8, 1.25]) * v
            elif isinstance(config[k], list):
                index = config[k].index(v)
                index += random.choice([-1, 0, 1])
                index = max(0, min(index, len(config[k]) - 1))
                params[k] = config[k][index]
            else:
                raise ValueError("Unknown config type")
        return params

    return sampler, perturber


def main():
    layers = 1
    config = {
        "fc": (-5, 0),
        "normalize_grad": [False, True],
        "seq_len": list(range(32, 256, 16)),
        "batch_size": list(range(32, 256, 16)),
    }
    config.update({f"lr{i}": (-5, 0) for i in range(layers + 1)})
    sampler, perturber = create_hyperparam_functions_from_config(config)
    scheduler = PBTScheduler(40, sampler, MODEL_DIR, perturb_fn=perturber)

    with DATA.open() as f:
        contents = f.read()
        rind = {k: i for i, k in enumerate(string.printable)}
        contents = [rind[c] for c in contents if c in string.printable]

    model = Model(4).to(dev)

    def lstm_params(layer):
        for name, weight in model.lstm.named_parameters():
            if name.endswith("l{}".format(layer)):
                yield weight

    all_param_groups = [
        {
            "params": itertools.chain(
                model.embedding.parameters(), model.linear.parameters()
            ),
        }
    ]
    all_param_groups.extend([{"params": lstm_params(i)} for i in range(layers)])
    opt = torch.optim.Adam(
        all_param_groups,
        lr=1e-3,
    )

    while True:
        agent = scheduler.get_next_run()
        model_file = agent.save_dir / "model.pkl"

        def save():
            state = {"model": model.state_dict(), "opt": opt.state_dict()}
            torch.save(state, model_file)

        if model_file.exists():
            params = torch.load(model_file)
            model.load_state_dict(params["model"])
            opt.load_state_dict(params["opt"])
        else:
            save()

        for i in range(layers+1):
            opt.param_groups[i]["lr"] = agent.hyperparams[f"lr{i}"]
        normalize_grad = agent.hyperparams["normalize_grad"]
        seq_len = agent.hyperparams["seq_len"]
        batch_size = agent.hyperparams["batch_size"]
        # steps = 400000 // (seq_len * batch_size)
        steps = 100000 // (seq_len * batch_size)
        print("Training:", agent.hyperparams)
        print("Steps:", steps)
        model.train()
        for _ in tqdm(range(steps)):
            inp = torch.zeros((batch_size, seq_len), dtype=torch.int64, device=dev)
            for batch_ind in range(batch_size):
                start = random.randint(0, len(contents) - seq_len)
                for seq_ind in range(seq_len):
                    inp[batch_ind, seq_ind] = contents[seq_ind + start]
            labels = inp[:, 1:]
            opt.zero_grad()
            predicted, _ = model(inp[:, :-1].to(dev), None)
            error = F.cross_entropy(predicted.transpose(1, 2), labels)
            error.backward()

            if normalize_grad:
                nn.utils.clip_grad_norm_(model.parameters(), 1)
            opt.step()
        save()
        total_cross_entropy = 0
        eval_seq_len = 16384
        with torch.no_grad():
            model.eval()
            hidden = model.init_hidden(1)
            actual_seq_len = min(EVAL_STEPS, len(contents) - 1)
            for batch_start in tqdm(range(0, actual_seq_len, eval_seq_len)):
                inp = torch.zeros((1, eval_seq_len + 1), dtype=torch.int64, device=dev)
                inp[0, :] = torch.tensor(
                    contents[batch_start : batch_start + eval_seq_len + 1]
                )
                predicted, hidden = model(inp[:, :-1], hidden)
                total_cross_entropy += F.cross_entropy(
                    predicted[0], inp[0, 1:], reduction="sum"
                )
        average_cross_entropy = total_cross_entropy.item() / actual_seq_len
        if math.isnan(average_cross_entropy):
            average_cross_entropy = float("inf")
        print("Result:", average_cross_entropy)
        scheduler.record_updated(agent, -average_cross_entropy)


if __name__ == "__main__":
    main()
