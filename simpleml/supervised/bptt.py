import torch
from torch import nn
from torch.nn import functional as F
from torch.utils.tensorboard import SummaryWriter
from pathlib import Path
import matplotlib.pyplot as plt
from tqdm import tqdm
import numpy as np
import math
import string

DATA = Path('./data/tale-of-two-cities.txt')
SAVE_PATH = Path('./data/bptt.pkl')
dev = torch.device('cuda')
HIDDEN_DIM = 256


class Model(nn.Module):
    def __init__(self, layers):
        super().__init__()
        self.layers = layers
        self.embedding = nn.Embedding(100, HIDDEN_DIM)
        self.lstm = nn.LSTM(HIDDEN_DIM, HIDDEN_DIM, num_layers=layers,
                batch_first=True)
        for name, param in self.lstm.named_parameters():
            if name.startswith('bias_ih_l'):
                with torch.no_grad():
                    param[HIDDEN_DIM:HIDDEN_DIM * 2] = 1
        self.linear = nn.Linear(HIDDEN_DIM, 100)

    def init_hidden(self, batch_size=1):
        v = lambda: torch.zeros((self.layers, batch_size, HIDDEN_DIM),
                                dtype=torch.float32,
                                device=dev)
        return (v(), v())

    def forward(self, x, hidden):
        x = self.embedding(x)
        output, hidden = self.lstm(x, hidden)
        return self.linear(output), hidden


def debug():
    layers = 2
    lstm = Model(layers)
    for name, _ in lstm.named_parameters():
        print(name)


def main():
    layers = 2
    lstm = Model(layers).to(dev)

    def predict(x, hidden):
        output, hidden = lstm(x, hidden)
        return output[0, 0], hidden

    def predict_from_char(c_ind, hidden):
        return predict(torch.tensor([[c_ind]], device=dev), hidden)

    opt = torch.optim.Adam(lstm.parameters(), lr=1e-2)
    max_in_memory_bptt = 100000

    with DATA.open() as f:
        contents = f.read()
        rind = {k: i for i, k in enumerate(string.printable)}
        contents = [rind[c] for c in contents if c in string.printable]

    if SAVE_PATH.is_file():
        state = torch.load(SAVE_PATH)
        opt.load_state_dict(state['opt'])
        lstm.load_state_dict(state['model'])

    step = 0
    while True:
        opt.zero_grad()
        hidden = lstm.init_hidden()
        input_vec = torch.tensor([contents[:max_in_memory_bptt]], device=dev)
        output, _ = lstm(input_vec, hidden)
        loss = F.cross_entropy(
            output[0],
            torch.tensor(contents[1:max_in_memory_bptt + 1], device=dev))
        step += 1
        loss.backward()
        opt.step()
        torch.save({
            'model': lstm.state_dict(),
            'opt': opt.state_dict()
        }, './data/bptt.pkl')

        out_str = ''
        prev_char = rind['A']
        hidden = lstm.init_hidden()
        rng = np.random.default_rng()
        for _ in range(2 * max_in_memory_bptt):
            c_ind = prev_char
            out_str += string.printable[prev_char]
            with torch.no_grad():
                pred, hidden = predict_from_char(c_ind, hidden)
                probs = F.softmax(pred)
                prev_char = rng.choice(100, p=probs.cpu().numpy())
        print('\n' * 10)
        print(out_str)
        print('Loss:', loss.item())


if __name__ == '__main__':
    main()
