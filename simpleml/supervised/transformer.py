"""
Raw implementation of barebones transformer based on the paper [1]
Only the decoder is implemented, because that is what OpenAI did for GPT

[1]: https://arxiv.org/pdf/1706.03762.pdf
"""

from torch import nn
from torch.nn import functional as F
import torch
import numpy as np
import matplotlib.pyplot as plt


def get_feature_inds(batch_dim, time_dim, feature_dim, model_dim):
    """
    Arguments:
        batch_dim (int)
        time_dim (int)
        feature_dim (int)
        model_dim (int) -- just used to to scale max feature indicies. In
        general, this is equivalent to 2*feature_dim
    Returns:
        (batch_dim, time_dim, feature_dim)
    """
    feature_inds = torch.arange(feature_dim)
    linspace_inds = 2 * feature_inds / model_dim
    return torch.unsqueeze(linspace_inds, 0).expand(batch_dim, time_dim, -1)


def calculate_sinusoidal_embedding(indicies, d_model, n=10000):
    """
    Arguments:
        indicies: (i_1, i_2, ..., i_k) torch tensor or numpy array

    Output
        (i_1, i_2, ..., i_k, d_model) tensor, where the last dimemsion is the
        feature dimension
    """
    mid_ind = d_model // 2
    if d_model % 2 != 0:
        sin_ind = mid_ind + 1
    else:
        sin_ind = mid_ind

    def unsqueeze(arr, unsqueeze_fn, amount):
        for _ in range(amount):
            arr = unsqueeze_fn(arr)
        return arr

    if isinstance(indicies, torch.Tensor):
        unsqueeze_first = lambda arr: torch.unsqueeze(arr, 0)
        # shape: (i_1,..., i_k, d_model)
        sin_feature_idx = (
            2
            * unsqueeze(torch.arange(sin_ind), unsqueeze_first, len(indicies.shape))
            / d_model
        )
        cos_feature_idx = (
            2
            * unsqueeze(torch.arange(mid_ind), unsqueeze_first, len(indicies.shape))
            / d_model
        )

        # shape: (i_1, ..., i_k, 1)
        expanded_inds = torch.unsqueeze(indicies, -1)
        # shape: (i_1, ..., i_k, d_model)
        sin_comp = torch.sin(expanded_inds / torch.pow(n, sin_feature_idx))
        cos_comp = torch.cos(expanded_inds / torch.pow(n, cos_feature_idx))
        return torch.cat([sin_comp, cos_comp], dim=-1)
    else:
        raise ValueError(f"Indicies have unsupported type {type(indicies)}")


def positional_embedding(x, starting_position=0, n=10000):
    """
    Arguments:
        x, torch.Tensor, shape (N, T, D)
        starting_position: int or (N) tensor
        n: total period size

    Returns:
        x + positional embedding
    """
    d_model = x.shape[-1]
    time_dim = x.shape[1]
    batch_dim = x.shape[0]

    # shape: (N, T)
    if isinstance(starting_position, int):
        pos_idx = starting_position + torch.arange(time_dim)[None, :].expand(
            batch_dim, -1
        )
    elif isinstance(starting_position, torch.Tensor):
        pos_idx = starting_position[:, None] + torch.arange(time_dim)[None, :]
    elif isinstance(starting_position, np.ndarray):
        pos_idx = (
            torch.tensor(starting_position[:, None]) + torch.arange(time_dim)[None, :]
        )
    else:
        raise ValueError("Unrecognized starting position type")

    embedding = calculate_sinusoidal_embedding(pos_idx, d_model, n=n)
    return x + embedding


class MaskedDotProductAttention(nn.Module):
    def __init__(
        self,
        input_dim,
        query_dim,
        value_dim,
        num_heads,
        output_dim,
        attention_type="dot",
        attention_scale=None,
        use_flash_attention=True,
    ):
        super().__init__()
        self.attention_type = attention_type
        self.query_dim = query_dim
        self.value_dim = value_dim
        self.num_heads = num_heads
        self.mapping = nn.Linear(input_dim, num_heads * (2 * query_dim + value_dim))
        self.output = nn.Linear(num_heads * value_dim, output_dim)
        self.last_normalized_attention = None
        self.last_unnormalized_attention = None
        self.use_flash_attention = use_flash_attention
        if attention_scale is None:
            self.attention_scale = 1 / np.sqrt(self.query_dim)
        else:
            self.attention_scale = attention_scale

    def forward(self, x):
        """
        Let n denote number of heads in shape calculations

        Arguments
            x (N, T, D)
        """
        N = x.shape[0]
        T = x.shape[1]
        D = x.shape[2]

        Q, K, V = self.compute_qkv(x)

        if self.use_flash_attention:
            scaled_values = F.scaled_dot_product_attention(
                Q, K, V, is_causal=True, scale=self.attention_scale
            )
        else:
            attention = self.compute_attention(Q, K)

            # (N, n, T, T)
            mask = torch.triu(torch.full_like(attention, -torch.inf), diagonal=1)

            # (N, n, T, T)
            normalized_attention = F.softmax(
                (attention + mask) * self.attention_scale, dim=3
            )

            # (N, n, T, d_v)
            scaled_values = torch.matmul(normalized_attention, V)

        # (N, T, d_v * n)
        result = torch.transpose(scaled_values, 1, 2).reshape(
            N, T, self.num_heads * self.value_dim
        )
        return self.output(result)

    def generate(self, x, state):
        """
        Let n denote number of heads in shape calculations
        Let T_prev denote timesteps that state contains

        Arguments
            x (N, T, D)
            state two tensors with shapes:
                (N, n, T_prev, d_k)
                (N, n, T_prev, d_v)

        Returns
            Tuple of (final output, new state)
            final output: (N, D) tensor
            new states: (N, T+1, D) tensor
        """
        N = x.shape[0]
        T = x.shape[1]
        D = x.shape[2]

        Q, K, V = self.compute_qkv(x)

        T_prev = 0
        if state is not None:
            # expected shapes:
            # K_prev (N, n, T_prev, d_k)
            # V_prev (N, n, T_prev, d_v)
            (K_prev, V_prev) = state

            T_prev = K_prev.shape[2]

            # (N, n, T_prev + T, d_k)
            K = torch.cat([K_prev, K], dim=2)

            # (N, n, T_prev + T, d_v)
            V = torch.cat([V_prev, V], dim=2)

        if self.use_flash_attention:
            scaled_values = F.scaled_dot_product_attention(
                Q, K, V, is_causal=True, scale=self.attention_scale
            )
        else:
            attention = self.compute_attention(Q, K)
            self.last_unnormalized_attention = attention

            # (N, n, T, T_prev + T)
            mask = torch.triu(
                torch.full_like(attention, -torch.inf), diagonal=1 + T_prev
            )

            # (N, n, T, T_prev + T)
            normalized_attention = F.softmax(
                (attention + mask) * self.attention_scale, dim=3
            )
            self.last_normalized_attention = normalized_attention

            # (N, n, T, d_v)
            scaled_values = torch.matmul(normalized_attention, V)

        # (N, T, d_v * n)
        result = torch.transpose(scaled_values, 1, 2).reshape(
            N, T, self.num_heads * self.value_dim
        )
        return self.output(result), (K, V)

    def compute_qkv(self, x):
        N = x.shape[0]
        T = x.shape[1]
        D = x.shape[2]

        # (N, T, n(2*d_k + d_v))
        results = self.mapping(x)

        # (N, T, n*d_k)
        Q_all = results[:, :, : self.num_heads * self.query_dim]

        # (N, n, T, d_k)
        Q = torch.transpose(Q_all.view(N, T, self.num_heads, self.query_dim), 1, 2)

        # (N, T, n*d_k)
        K_all = results[
            :, :, self.num_heads * self.query_dim : 2 * self.num_heads * self.query_dim
        ]
        # (N, n, T, d_k)
        K = torch.transpose(K_all.view(N, T, self.num_heads, self.query_dim), 1, 2)

        # (N, T, n*d_v)
        V_all = results[:, :, 2 * self.num_heads * self.query_dim :]
        # (N, n, T, d_v)
        V = torch.transpose(V_all.view(N, T, self.num_heads, self.value_dim), 1, 2)

        return Q, K, V

    def compute_attention(self, Q, K):
        if self.attention_type == "l2_norm":
            # L2 norm attention
            # shape of Q - K: (N, n, T, T, d_k)
            # resulting shape: (N, n, T, T)
            return torch.linalg.norm(Q[:, :, :, None, :] - K[:, :, None, :, :], dim=4)
        elif self.attention_type == "neg_l2_norm":
            # L2 norm attention
            # shape of Q - K: (N, n, T, T, d_k)
            # resulting shape: (N, n, T, T)
            return -torch.linalg.norm(Q[:, :, :, None, :] - K[:, :, None, :, :], dim=4)
        elif self.attention_type == "neg_log_norm":
            # L2 norm attention
            # shape of Q - K: (N, n, T, T, d_k)
            # resulting shape: (N, n, T, T)
            return -torch.log(
                1e-3
                + torch.square(Q[:, :, :, None, :] - K[:, :, None, :, :]).sum(dim=4)
            )
        elif self.attention_type == "neg_l2_squared":
            squared_differences = torch.square(
                Q[:, :, :, None, :] - K[:, :, None, :, :]
            )
            return -torch.sum(squared_differences, 4)
        elif self.attention_type == "neg_l1_norm":
            # L2 norm attention
            # shape of Q - K: (N, n, T, T, d_k)
            # resulting shape: (N, n, T, T)
            return -torch.linalg.norm(
                Q[:, :, :, None, :] - K[:, :, None, :, :], dim=4, ord=1
            )
        elif self.attention_type == "dot":
            K_transposed = torch.transpose(K, 2, 3)
            # (N, n, T, T)
            return torch.matmul(Q, K_transposed)
        else:
            raise ValueError(f"Unknown attention type: {self.attention_type}")


class TransformerLayer(nn.Module):
    """
    Uses pre-LN instead of post-LN because of training stability, at the cost of
    slightly worse performance + simpler implementation and training methodology

    https://arxiv.org/pdf/2002.04745
    """
    def __init__(
        self,
        hidden_dim,
        query_dim,
        value_dim,
        num_heads,
        feed_forward_dim,
        attention_type="dot",
        attention_scale=None,
    ):
        super().__init__()
        self.self_attention = MaskedDotProductAttention(
            hidden_dim,
            query_dim,
            value_dim,
            num_heads,
            hidden_dim,
            attention_type=attention_type,
            attention_scale=attention_scale,
        )
        self.attention_norm = nn.LayerNorm(hidden_dim)
        self.ff_linear = nn.Sequential(
            nn.Linear(hidden_dim, feed_forward_dim),
            nn.LeakyReLU(),
            nn.Linear(feed_forward_dim, hidden_dim),
        )
        self.ff_norm = nn.LayerNorm(hidden_dim)

    def forward(self, x):
        """
        Arguments
            x (N, T, D)
        """
        x = x + self.self_attention(self.attention_norm(x))
        x = x + self.ff_linear(self.ff_norm(x))
        return x

    def generate(self, x, state):
        """
        Arguments:
            x (N, T, D)
            state: attention state
        """
        x_delta, new_state = self.self_attention.generate(self.attention_norm(x), state)
        x = x + x_delta
        x = x + self.ff_linear(self.ff_norm(x))
        return x, new_state


class GptTransformer(nn.Module):
    def __init__(
        self,
        input_dim,
        output_dim,
        hidden_dim=128,
        query_dim=32,
        value_dim=32,
        num_heads=4,
        feed_forward_dim=512,
        num_layers=3,
        transformer_layer_args={},
        positional_encoding_max=10000,
    ):
        super().__init__()
        self.num_layers = num_layers
        self.input_layer = nn.Linear(input_dim, hidden_dim)
        self.transformer_layers = nn.ModuleList(
            [
                TransformerLayer(
                    hidden_dim=hidden_dim,
                    query_dim=query_dim,
                    value_dim=value_dim,
                    num_heads=num_heads,
                    feed_forward_dim=feed_forward_dim,
                    **transformer_layer_args,
                )
                for _ in range(self.num_layers)
            ]
        )
        self.output_normalization = nn.LayerNorm(hidden_dim)
        self.output_transformation = nn.Linear(hidden_dim, output_dim)
        self.positional_encoding_max = positional_encoding_max

    def forward(self, x, starting_position=0):
        """
        N: batch size
        T: time dimension
        D: feature dimension
        Arguments:
            input (N, T, D): Tensor representing input sequence
            starting_position (optional, (N,)): Tensor representing starting ind
                of each sample

        Output:
            (N, T, D): Tensor representing prediction for each part of
            input sequence
        """
        x = self._preprocess(x, starting_position)
        for layer in self.transformer_layers:
            x = layer(x)
        return self._postprocess(x)

    def generate(self, x, state=None, starting_position=0):
        """
        A faster generation method, that keeps track of previously stored hidden
        layer kv pairs. Reduces overall complexity to O(n), for O(n^2)
        generation, as opposed to O(n^3) generation that would've happened with
        the forward() method. Assumes gradient is not tracked.

        Arguments:
            input (batch dim, time dim, feature dim)

        Returns
            Tuple of:
                Matrix with dimension (N, time dim, output_dim)
                hidden state object
        """
        x = self._preprocess(x, starting_position)
        if state is None:
            state = [None for _ in self.transformer_layers]
        new_state = []
        for layer, layer_state in zip(self.transformer_layers, state):
            x, new_layer_state = layer.generate(x, layer_state)
            new_state.append(new_layer_state)
        return self._postprocess(x), new_state

    def _preprocess(self, x, starting_position):
        x = self.input_layer(x)
        x += positional_embedding(x, starting_position, n=self.positional_encoding_max)
        return x

    def _postprocess(self, x):
        x = self.output_normalization(x)
        x = self.output_transformation(x)
        return x
