from .transformer import MaskedDotProductAttention, GptTransformer, positional_embedding, calculate_sinusoidal_embedding
import matplotlib.pyplot as plt
import torch
from collections import defaultdict


def assert_is_eq(a, b, epsilon=1e-5):
    assert torch.all(torch.abs(a - b) < epsilon)


def test_pos_embedding():
    # make sure odd works
    sample_odd = torch.zeros((12, 13, 7))
    positional_embedding(sample_odd)

    sample_even = torch.zeros((12, 13, 8))
    result_even = positional_embedding(sample_even)

    # these should form the geometric sequence
    plt.subplot(121)
    for i in range(8):
        plt.plot(result_even[0, :, i], label=str(i))

    # these should all be the same
    plt.subplot(122)
    for i in range(8):
        plt.plot(result_even[i, :, 0], label=str(i))

    plt.legend()

    # plt.show()
    assert True


def test_pos_embedding_with_n_starting_position():
    sample_odd = torch.zeros((2, 1, 3))
    time_inds = torch.tensor([0, 1], dtype=torch.int64)
    embedding = positional_embedding(sample_odd, time_inds)
    assert embedding.shape == (2, 1, 3)


def test_pos_embedding_consistency():
    sample1 = torch.zeros((2, 1, 3))
    time_inds = torch.tensor([0, 1], dtype=torch.int64)
    embedding1 = positional_embedding(sample1, time_inds)

    sample2 = torch.zeros((2, 2, 3))
    embedding2 = positional_embedding(sample2, 0)

    assert_is_eq(embedding1[0, 0, :], embedding2[0, 0, :])
    assert_is_eq(embedding1[0, 0, :], embedding2[1, 0, :])
    assert_is_eq(embedding1[1, 0, :], embedding2[0, 1, :])
    assert_is_eq(embedding1[1, 0, :], embedding2[1, 1, :])


def test_pos_embedding_consistency2():
    sample1 = torch.zeros((2, 1, 3))
    time_inds = torch.tensor([100, 175], dtype=torch.float32)
    embedding1 = positional_embedding(sample1, time_inds)

    sample2 = torch.zeros((2, 500, 3))
    embedding2 = positional_embedding(sample2, 50)

    assert_is_eq(embedding1[0, 0, :], embedding2[0, 50, :])
    assert_is_eq(embedding1[1, 0, :], embedding2[0, 125, :])


def test_transformer_layer():
    layer = MaskedDotProductAttention(3, 4, 5, 2, 6)
    sample_data = torch.ones((2, 7, 3))
    result = layer(sample_data)

    assert result.shape == (2, 7, 6)


def test_gpt_transformer():
    # Original size based on google paper
    # gpt = GptTransformer(100, 512, 100, 64, 64, 64, 8, 2048)

    # more feasible for my machine (takes around 2mb of memory)
    gpt = GptTransformer(
        input_dim=100,
        hidden_dim=128,
        output_dim=100,
        query_dim=32,
        value_dim=32,
        num_heads=4,
        feed_forward_dim=512,
        num_layers=3,
    )
    optim = torch.optim.Adam(gpt.parameters(), 1e-3)
    mem_params = sum(
        [param.nelement() * param.element_size() for param in gpt.parameters()]
    )
    mem_bufs = sum([buf.nelement() * buf.element_size() for buf in gpt.buffers()])
    mem = mem_params + mem_bufs  # in bytes

    print("gpt size", mem)
    name_to_zero_index_map = defaultdict(lambda: defaultdict(lambda: 0))
    num_runs = 20
    for _ in range(num_runs):
        sample_data = torch.randn((2, 3, 100))
        optim.zero_grad()
        result = gpt(sample_data)
        result.sum().backward()
        for name, param in gpt.named_parameters():
            has_zero = (param.grad == 0).any().item()
            if has_zero:
                zero_inds = (param.grad == 0).nonzero()
                for ind in zero_inds:
                    name_to_zero_index_map[name][tuple(ind.numpy())] += 1
    for name, indicies in name_to_zero_index_map.items():
        sorted(indicies.keys(), key=lambda k: indicies[k], reverse=True)
        assert max(indicies.values()) != num_runs

def test_sinusoidal_embedding_shape_correct():
    indicies = torch.tensor([0, 1, 2, 3])
    dimension = 5
    embedding = calculate_sinusoidal_embedding(indicies, dimension)
    assert embedding.shape == (4, 5)
