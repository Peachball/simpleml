import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
import sklearn.preprocessing
import os


def _one_hot(lbls):
    lbl_bin = sklearn.preprocessing.LabelBinarizer()
    lbl_bin.fit(lbls)
    return lbl_bin.transform(lbls)


def _split_arr(arr, batch_size):
    n = arr.shape[0] // batch_size
    end = arr.shape[0] // n * n
    arrs = np.split(arr[:end], n)
    return arrs


def _clean_data(x, y, bs=32):
    x = x.astype('float32') / 255
    one_hot = _one_hot(y)
    return _split_arr(x, bs), _split_arr(one_hot, bs)


def data_gen(batch_size=32):
    (x_train, y_train), (x_test,
                         y_test) = tf.keras.datasets.cifar100.load_data()
    return _clean_data(x_train, y_train), _clean_data(x_test, y_test)
