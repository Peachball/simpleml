# Notes

## Description

Overall thoughts on things

## Observations

### Qualitative/Informal

- Deeper networks do not perform better if optimization is weak

- SGD training has 3 phases:

  1.  Fast learning
      In this phase, it's likely that the unused ascii charset is being
      learned away (e.g. ascii 189 is not relevant)
  2.  Slow learning
      It figures out some words, and vowel/consonant relationships here.
      Probably explains the slower decrease in loss
  3.  Plateau
      Weirdly plateaus somewhat randomly

- Adam + RMSprop APPEAR to not have the slow phase of learning
- RMSprop has weird segment where weight distribution turns bimodal, and goes
  back to unimodal
- Increasing the rank estimate consistently helps adam optimizer
- Decreasing the learning rate doesn't seem to improve loss (in ~4k epochs at
  least). Maybe the loss is a poor metric to optimize?
- Word embeddings have significantly different scale than the weights
  - Might be good, because embeddings have different meaning than weights
    anyways

### Quantitative

1.  Unnamed
    - Config
      - hidden config: [256, 256, 256]
      - custom weight initialization
      - learning rate: 2e-3
      - RMSprop
      - Rank 1 estimates
    - Observations:
      - Instability around iteration 6431 and 14346 when learning rate decreased
      - error ended at around 2.8
      - error was decreasing slightly
1.  "success"
    - Config
      - hidden config: [256, 256, 256]
      - default weight initialization
      - learning rate: ??
      - Rank 32 estimates
      - Adam
    - Observations:
      - lowest learning rate was

## Questions

### Unanswered

- Does RMSprop result in better error rates than Adam?
- Does SGD result in better error rates than RMSprop
- When the loss plateaus, is it actually plateauing or is the learning just much
  slower?
- Does using higher rank estimates result in error rates that cannot be achieved
  by any means with lower rank estimates?

### Answered

Nothing yet!

## Improvements?

- store training data state
  - either store location
  - or randomize where data starts from
- add test set stats
  - current statistics likely biased as weights are changing in conjunction with
    error calculation
- Memory truncated backprop
  - Likely will refine gradients, much like the higher rank estimate
  - Original paper suggests that this only speeds up early training
    - actually this would be useful either way, don't think i'm reaching
      optimal solution anyways
  - Accurate k length gradients will make this more similar to the original
    char-rnn, which was pretty successful
- Weight initialization scheme
  - Unclear justification
  - Hard to test, due to many possible ways to initialize weights
- More patient scheduler
  - Fits with intuition that loss scale changes during training
  - Unclear how patient scheduler should be
