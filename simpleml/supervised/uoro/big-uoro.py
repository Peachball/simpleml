import argparse
import itertools
import json
import os
import random
import re
import uuid
from pathlib import Path

import numpy as np
import torch
from torch.utils.tensorboard import SummaryWriter

DATA_DIR = '/home/peachball/Downloads/Gutenberg/'
DEFAULT_LOGDIR = 'logs/'
SAVEFILE_NAME = 'train_state.pkl'


class TrainSettings:
    def __init__(self):
        self.lr = 2e-3
        self.rank_estimate = 1
        self.optimizer = 'rmsprop'
        self.hidden_config = [256, 256, 256]
        self.scheduler_patience = 50000
        self.scheduler_factor = np.cbrt(.1)
        self.batch_size = 256
        self.alpha = 0.0001
        self.logdir = DEFAULT_LOGDIR + str(uuid.uuid4())

    def load(self, settings):
        for key, value in settings.items():
            if hasattr(self, key):
                setattr(self, key, value)


def get_weight_initializer(config):
    def _init(w):
        def initializer(v):
            return torch.nn.init.xavier_uniform_(v, gain=np.sqrt(1.8 / 6))

        if isinstance(w, torch.nn.Linear):
            initializer(w.weight)
            w.bias.fill_(0.0)
        elif isinstance(w, torch.nn.modules.rnn.LSTMCell):
            special = config.get('special_init', None)
            for name, param in w.named_parameters():
                if re.search('^weight_', name):
                    hidden_size = param.size()[0] // 4
                    if special is None:
                        input_size = param.size()[1]
                        mag = np.sqrt(6 / (hidden_size + input_size))
                        torch.nn.init.uniform_(param, a=-mag, b=mag)
                    if special == 'orthogonal':
                        if re.search('weight_ih', name):
                            torch.nn.init.orthogonal_(param)
                        elif re.search('weight_hh', name):
                            identity = np.eye(hidden_size)
                            param.copy_(
                                torch.from_numpy(
                                    np.tile(identity,
                                            (4, 1)).astype('float32')))
                elif re.search('bias_ih', name):
                    # Ignore this bias param because it duplicates bias_hh
                    param.fill_(0.0)
                elif re.search('bias_hh', name):
                    param.fill_(0.0)
                    gate_size = param.size()[0] // 4
                    # Initialize forget gate here
                    # LSTM Variables doc:
                    # https://pytorch.org/docs/stable/nn.html#torch.nn.LSTM
                    param[gate_size:gate_size * 2].fill_(1.0)

    return _init


class UOROLanguage(torch.nn.Module):
    def __init__(self, total_inds, embedding_dim=32, hconfig=[80, 60]):
        super(UOROLanguage, self).__init__()
        self.total_inds = total_inds
        self.embeddings = torch.nn.Embedding(total_inds, embedding_dim)
        layer_sizes = [embedding_dim] + hconfig
        self.lstm = torch.nn.ModuleList([
            torch.nn.LSTMCell(layer_sizes[i], layer_sizes[i + 1])
            for i in range(len(layer_sizes) - 1)
        ])
        self.initial_h = self._get_hidden_list(layer_sizes[1:])
        self.initial_c = self._get_hidden_list(layer_sizes[1:])
        self.output = torch.nn.Linear(layer_sizes[-1], embedding_dim)

        with torch.no_grad():
            self.apply(get_weight_initializer({}))

    def _get_hidden_list(self, sizes):
        return [torch.zeros((hsize, )) for hsize in sizes]

    def get_init_hidden(self, batch_size):
        device = next(self.parameters()).device
        return [(h[None, :].expand(batch_size, -1).to(device),
                 c[None, :].expand(batch_size, -1).to(device))
                for h, c in zip(self.initial_h, self.initial_c)]

    def forward(self, x, state):
        x = self.embeddings(x)
        new_state = []
        for l, lstate in zip(self.lstm, state):
            x, ns = l(x, lstate)
            new_state.append((x, ns))
        pred_embed = self.output(x)
        return torch.nn.functional.linear(pred_embed,
                                          self.embeddings.weight), new_state


def book_iter(data_dir=DATA_DIR):
    data_dir = Path(data_dir)
    files = list((data_dir / 'txt').iterdir())
    random.shuffle(files)

    while True:
        for f in files:
            with f.open('r') as fobj:
                try:
                    yield fobj.read()
                except UnicodeDecodeError:
                    pass


def batched_iter(batch_size=256):
    data_gen = book_iter()
    batches = [[next(data_gen), -1] for _ in range(batch_size)]

    while True:
        for i in range(len(batches)):
            batches[i][1] += 1
            while len(batches[i][0]) <= batches[i][1]:
                batches[i] = [next(data_gen), 0]

        yield np.array([min(ord(d[i]), 256) for d, i in batches])


def get_sample(model, sample_length, temperature=1):
    chars = [ord('a')]
    state = model.get_init_hidden(1)
    with torch.no_grad():
        for _ in range(sample_length):
            pred, state = model(
                torch.tensor(chars[-1])[None].to(
                    next(model.parameters()).device), state)

            # Sample
            if temperature != 0:
                probs = torch.nn.functional.softmax(
                    pred / temperature, dim=-1)[0, :].cpu().numpy()
                if probs.min() < 0:
                    return
                chars.append(np.random.choice(len(probs), p=probs))
            else:
                chars.append(pred[0, :].argmax().item())

    return ''.join([chr(c) for c in chars])


class Trainer:
    def __init__(self, device, settings):
        self.settings = settings
        self.device = device
        self.batch_size = settings.batch_size
        self.epsilon = 1e-7
        self.logdir = settings.logdir
        self.sw = SummaryWriter(self.logdir)
        setting_str = json.dumps(settings.__dict__)
        self.sw.add_text('settings', setting_str)
        with open(self.logdir + '/settings.txt', 'w') as f:
            f.write(setting_str)

        self.model = UOROLanguage(257, hconfig=settings.hidden_config)
        self.model.to(self.device)
        if settings.optimizer == 'rmsprop':
            self.optimizer = torch.optim.RMSprop(self.model.parameters(),
                                                 settings.lr)
        elif settings.optimizer == 'adam':
            self.optimizer = torch.optim.Adam(self.model.parameters(),
                                              settings.lr)
        elif settings.optimizer == 'sgd':
            self.optimizer = torch.optim.SGD(self.model.parameters(),
                                             settings.lr)
        else:
            raise ValueError('Invalid optimizer: {}'.format(
                settings.optimizer))
        self.scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(
            self.optimizer,
            patience=settings.scheduler_patience,
            verbose=True,
            factor=settings.scheduler_factor)
        self.rank_approx = settings.rank_estimate
        self.step = 0

    def train(self):
        state = self.model.get_init_hidden(self.batch_size)

        def _create(v):
            return torch.zeros_like(v).unsqueeze(0).repeat(
                self.rank_approx, *[1 for _ in v.shape])

        state_p = [(_create(h), _create(c)) for h, c in state]
        theta_p = [_create(p) for p in self.model.parameters()]
        data_gen = batched_iter(self.batch_size)
        prev_b = next(data_gen)
        loss_fn = torch.nn.CrossEntropyLoss()
        ema_loss = 6
        alpha = self.settings.alpha
        torch.autograd.set_detect_anomaly(True)
        for b in data_gen:
            self.optimizer.zero_grad()
            x = torch.from_numpy(prev_b).to(self.device)

            for h, c in state:
                h.requires_grad_(True)
                c.requires_grad_(True)

            pred, new_state = self.model(x, state)

            loss = loss_fn(pred, torch.from_numpy(b).to(self.device))
            loss.backward(retain_graph=True)

            state_dot_prod = 0
            for (h, c), (hp, cp) in zip(state, state_p):

                def _prod(a, b):
                    return (
                        a * b).sum(dim=tuple(range(1, len(a.shape)))).mean()

                state_dot_prod += _prod(h.grad, hp) + _prod(c.grad, cp)

            for p, g in zip(self.model.parameters(), theta_p):
                p.grad = state_dot_prod * g.mean(dim=0) + p.grad

            def _collapse(a):
                return a.reshape(-1, *a.shape[2:])

            def _expand(a):
                return a.reshape(self.rank_approx, -1, *a.shape[1:])

            def _combine(a, b):
                return (a.repeat(self.rank_approx, 1) +
                        self.epsilon * _collapse(b))

            _, state_diff = self.model(
                x.repeat(self.rank_approx), [(_combine(h, hp), _combine(c, cp))
                                             for (h, c),
                                             (hp, cp) in zip(state, state_p)])

            def _calculate_diff(d, org):
                return _expand(d -
                               org.repeat(self.rank_approx, 1)) / self.epsilon

            state_p = [(_calculate_diff(hd, h), _calculate_diff(cd, c))
                       for (h, c), (hd, cd) in zip(new_state, state_diff)]

            flatten = itertools.chain.from_iterable
            all_signs = []
            all_random_grads = []
            for _ in range(self.rank_approx):
                signs = [(torch.sign(torch.randn(*h.shape)).to(self.device),
                          torch.sign(torch.randn(*c.shape)).to(self.device))
                         for h, c in state]
                param_grads = torch.autograd.grad(
                    flatten(new_state),
                    list(self.model.parameters()),
                    grad_outputs=flatten(signs),
                    allow_unused=True,
                    retain_graph=True)
                param_grads = [
                    torch.zeros_like(p) if g is None else g
                    for p, g in zip(self.model.parameters(), param_grads)
                ]
                all_signs.append(signs)
                all_random_grads.append(param_grads)

            random_signs = [(torch.stack([s[i][0] for s in all_signs]),
                             torch.stack([s[i][1] for s in all_signs]))
                            for i in range(len(state))]
            random_param_grads = [
                torch.stack([s[i] for s in all_random_grads])
                for i in range(len(theta_p))
            ]

            def norm(v):
                return torch.sqrt(
                    sum([(vp**2).sum(dim=tuple(range(1, len(vp.shape))))
                         for vp in v]))

            with torch.no_grad():
                rho0 = torch.sqrt(
                    norm(theta_p) /
                    (norm(flatten(state_p)) + self.epsilon)) + self.epsilon
                rho1 = torch.sqrt(
                    norm(random_param_grads) / (norm(flatten(random_signs)) +
                                                self.epsilon)) + self.epsilon

                def _align(v, a):
                    res = v
                    for _ in range(len(a.shape) - 1):
                        res = res.unsqueeze(-1)
                    return res

                state_p = [(h * _align(rho0, h) + vh * _align(rho1, vh),
                            c * _align(rho0, c) + vc * _align(rho1, vc))
                           for (h, c), (vh, vc) in zip(state_p, random_signs)]
                theta_p = [
                    tp / _align(rho0, tp) + dp / _align(rho1, dp)
                    for tp, dp in zip(theta_p, random_param_grads)
                ]

            state = [(h.detach(), c.detach()) for h, c in new_state]
            prev_b = b

            torch.nn.utils.clip_grad_norm_(self.model.parameters(), 10)
            self.optimizer.step()

            ema_loss = (1 - alpha) * ema_loss + alpha * loss.item()
            self.scheduler.step(ema_loss)
            self.step += 1
            self.sw.add_scalar('ema_loss', ema_loss, self.step)
            self.sw.add_scalar('loss', loss.item(), self.step)
            if self.step % 1000 == 0:
                self.log()
                self.save()

    def log(self):
        self.sw.add_embedding(
            self.model.embeddings.weight,
            metadata=[str(i) + '_' + chr(i) for i in range(257)],
            tag='vocab',
            global_step=self.step)
        for t in [0, .5, 1]:
            sample_text = get_sample(self.model, 100, temperature=t)
            self.sw.add_text('generated_text/temp=' + str(t), sample_text,
                             self.step)

    def save(self, filename=None):
        """
        Saves training state. Will automatically existing file and add .old
        extension to it if filename exists already

        Arguments:
            filename (str or list[str]): Places to save model to. By default
                saves model to logdir
        """
        state = {
            'model': self.model.state_dict(),
            'optimizer': self.optimizer.state_dict(),
            'scheduler': self.scheduler.state_dict(),
            'step': self.step,
            'settings': self.settings
        }
        if filename is None:
            filename = os.path.join(self.logdir, SAVEFILE_NAME)

        if os.path.exists(filename):
            os.rename(filename, filename + '.old')

        with open(filename, 'wb') as f:
            torch.save(state, f)

    @classmethod
    def load(cls, filename, device='cuda'):
        with open(filename, 'rb') as f:
            state = torch.load(f)
        model = cls(device, state['settings'])
        model.model.load_state_dict(state['model'])
        model.optimizer.load_state_dict(state['optimizer'])
        model.scheduler.load_state_dict(state['scheduler'])
        model.step = state['step']
        return model


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--settings', type=json.loads, help='training settings',
                        default='{}')

    return parser.parse_args()


def main():
    args = parse_args()

    settings = TrainSettings()
    settings.rank_estimate = 32
    settings.hidden_config = [256, 256, 256]
    settings.lr = 2e-2
    settings.optimizer = 'sgd'

    settings.load(args.settings)
    existing_train_state = Path(settings.logdir) / SAVEFILE_NAME
    if existing_train_state.exists():
        trainer = Trainer.load(str(existing_train_state))
    else:
        trainer = Trainer(torch.device('cuda'), settings)
    trainer.train()


if __name__ == '__main__':
    main()
