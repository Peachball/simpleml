#!/usr/bin/env bash
# Runs + reloads uoro, allowing for longer training times
optirun python big-uoro.py --settings '{"logdir": "logs/continuous"}'
