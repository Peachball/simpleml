#!/usr/bin/env bash
pkill tensorboard
sleep 0.5
mv logs/* history/
rm -rf logs
tensorboard --logdir logs &
optirun python big-uoro.py
