import torch
from torch import nn
from torch.nn import functional as F
from torch.utils.tensorboard import SummaryWriter
from pathlib import Path
import matplotlib.pyplot as plt
from tqdm import tqdm
import numpy as np
import math
import string
import copy

DATA = Path('./data/tale-of-two-cities.txt')
SAVE_PATH = Path('./data/bptt.pkl')
dev = torch.device('cpu')
HIDDEN_DIM = 256


class Model(nn.Module):
    def __init__(self, layers):
        super().__init__()
        self.layers = layers
        self.embedding = nn.Embedding(100, HIDDEN_DIM)
        self.lstm = nn.LSTM(HIDDEN_DIM,
                            HIDDEN_DIM,
                            num_layers=layers,
                            batch_first=True)
        for name, param in self.lstm.named_parameters():
            if name.startswith('bias_ih_l'):
                with torch.no_grad():
                    param[HIDDEN_DIM:HIDDEN_DIM * 2] = 1
        self.linear = nn.Linear(HIDDEN_DIM, 100)

    def init_hidden(self, batch_size=1, requires_grad=False):
        v = lambda: torch.zeros((self.layers, batch_size, HIDDEN_DIM),
                                dtype=torch.float32,
                                device=dev,
                                requires_grad=requires_grad)
        return (v(), v())

    def forward(self, x, hidden):
        x = self.embedding(x)
        output, hidden = self.lstm(x, hidden)
        return self.linear(output), hidden


def debug():
    layers = 2
    lstm = Model(layers)
    for name, _ in lstm.named_parameters():
        print(name)


def main():
    layers = 2
    lstm = Model(layers).to(dev)
    temp_lstm = copy.deepcopy(lstm)

    def predict(x, hidden):
        output, hidden = lstm(x, hidden)
        return output[0, 0], hidden

    def predict_from_char(c_ind, hidden):
        return predict(torch.tensor([[c_ind]], device=dev), hidden)

    def print_info(loss):
        out_str = ''
        prev_char = rind['A']
        hidden = lstm.init_hidden()
        rng = np.random.default_rng()
        for _ in range(10 * max_in_memory_bptt):
            c_ind = prev_char
            out_str += string.printable[prev_char]
            with torch.no_grad():
                pred, hidden = predict_from_char(c_ind, hidden)
                probs = F.softmax(pred)
                prev_char = rng.choice(100, p=probs.cpu().numpy())
        print('\n' * 10)
        print(out_str)
        print('Loss:', loss)

    def compute_gradient():
        # Calculate gradient through normal way
        input_vec = torch.tensor([contents[:-1]], device=dev)
        labels = torch.tensor(contents[1:], device=dev)
        output, _ = lstm(input_vec, lstm.init_hidden())
        bptt_loss = F.cross_entropy(output[0], labels, reduction='sum')
        bptt_loss.backward()

    opt = torch.optim.RMSprop(lstm.parameters(), lr=1e-3)
    fake_opt = torch.optim.SGD(temp_lstm.parameters(), lr=1)
    max_in_memory_bptt = 100

    with DATA.open() as f:
        contents = f.read()
        rind = {k: i for i, k in enumerate(string.printable)}
        contents = [rind[c] for c in contents if c in string.printable]
        contents = contents[:1000]

    # if SAVE_PATH.is_file():
    # state = torch.load(SAVE_PATH)
    # opt.load_state_dict(state['opt'])
    # lstm.load_state_dict(state['model'])

    step = 0
    while True:
        hidden = lstm.init_hidden()
        hidden_states = [hidden]
        starts = list(range(0, len(contents) - 1, max_in_memory_bptt))
        total_loss = 0

        # Update temp_lstm weights with most recent
        with torch.no_grad():
            for (tp, p) in zip(temp_lstm.parameters(), lstm.parameters()):
                tp.copy_(p)

        # Forward pass
        for start in tqdm(starts):
            end = min(start + max_in_memory_bptt, len(contents) - 1)
            with torch.no_grad():
                input_vec = torch.tensor([contents[start:end]], device=dev)
                output, hidden = temp_lstm(input_vec, hidden_states[-1])
                hidden_states.append(hidden)
        # backwards pass
        hidden_gradients = [torch.zeros_like(h) for h in hidden]
        # total_gradients = [torch.zeros_like(p) for p in lstm.parameters()]
        for start, hidden in tqdm(zip(starts[::-1], hidden_states[-2::-1]),
                                  total=len(starts)):
            fake_opt.zero_grad()
            end = min(start + max_in_memory_bptt, len(contents) - 1)
            input_vec = torch.tensor([contents[start:end]], device=dev)
            hidden_with_grad = temp_lstm.init_hidden(requires_grad=True)
            with torch.no_grad():
                # copy values to hidden_with_grad
                for (v, vp) in zip(hidden_with_grad, hidden):
                    v.copy_(vp)
            output, new_hidden = temp_lstm(input_vec, hidden_with_grad)
            labels = torch.tensor(contents[start + 1:end + 1], device=dev)
            all_losses = F.cross_entropy(output[0], labels, reduction='none')
            supplementary_loss = sum(
                torch.sum(hg * h) + torch.sum(cg * c)
                for ((hg, cg), (h, c)) in zip(hidden_gradients, new_hidden))
            actual_loss = all_losses.sum()
            loss = actual_loss + supplementary_loss
            total_loss += actual_loss.item()
            loss.backward()

            # update using adam
            opt.zero_grad()
            for (p, gp) in zip(lstm.parameters(), temp_lstm.parameters()):
                p.grad = gp.grad
            opt.step()

            # keep track of hidden gradients
            for (hg, h) in zip(hidden_gradients, hidden_with_grad):
                hg.copy_(h.grad)

        ## compare calculations
        #opt.zero_grad()

        ## Copy over calculated gradients
        #for (tp, p) in zip(total_gradients, lstm.parameters()):
        #    p.grad = tp

        #opt.step()
        loss = total_loss / (len(contents) - 1)
        print('Total average loss:', loss)
        step += 1
        torch.save({
            'model': lstm.state_dict(),
            'opt': opt.state_dict()
        }, './data/bptt.pkl')
        print_info(loss)


if __name__ == '__main__':
    main()
