"""
Label refinement, but the end goal is to recreate "Born Again Networks" from:
https://arxiv.org/pdf/1805.04770v1.pdf
"""
import os

import matplotlib.pyplot as plt
import numpy as np
import sklearn.preprocessing
import tensorflow as tf

from .utils import data_gen


def get_prediction(img):
    net = img
    kernel_sizes = [32, 32, 32, 32]
    kernel_std = [3] + kernel_sizes[:-1]
    for ksize, kstd in zip(kernel_sizes, kernel_std):
        net = tf.layers.conv2d(
            net,
            ksize,
            3,
            1,
            'same',
            kernel_initializer=tf.random_normal_initializer(
                stddev=np.sqrt(1 / (9 * kstd))))
        net = tf.nn.selu(net)
        net = tf.layers.max_pooling2d(net, 2, 2)
    net = tf.layers.flatten(net)
    net = tf.layers.dense(net, 100)
    return net


def main():
    inp = tf.placeholder(tf.float32, [None, 32, 32, 3])
    label = tf.placeholder(tf.float32, [None, 100])
    pred = get_prediction(inp)
    cross_entropy = tf.nn.softmax_cross_entropy_with_logits_v2(
        labels=label, logits=pred)
    error = tf.reduce_mean(cross_entropy)

    global_step = tf.Variable(0, trainable=False)
    lr = tf.constant(1e-3)
    train_op = tf.train.MomentumOptimizer(lr, .9).minimize(
        error, global_step=global_step)

    (imgs, lbls), (test_img, test_lbl) = data_gen(batch_size=32)

    train_lbls = lbls
    with tf.Session() as sess:
        print('Training')

        # Train phase
        while True:
            prev_error = 100
            prev_accuracy = 0
            learning_rate = 1e-2
            sess.run(tf.global_variables_initializer())
            while True:
                # Training step
                total_error = 0
                for x, y in zip(imgs, train_lbls):
                    fetches = {
                        'error': error,
                        'train': train_op,
                        'global_step': global_step
                    }
                    result = sess.run(
                        fetches,
                        feed_dict={
                            inp: x,
                            label: y,
                            lr: learning_rate
                        })
                    total_error += result['error']
                avg_error = total_error / len(imgs)
                print('Average error:', avg_error)

                # Evaluation step
                total_correct = 0
                total = 0
                for x, y in zip(test_img, test_lbl):
                    guess = sess.run(pred, feed_dict={inp: x})
                    total_correct += np.sum(
                        np.argmax(guess, axis=1) == np.argmax(y, axis=1))
                    total += x.shape[0]
                accuracy = (total_correct / total)
                print('Accuracy: %f' % accuracy)
                if accuracy > prev_accuracy:
                    prev_accuracy = accuracy
                else:
                    if learning_rate < 1e-5:
                        break
                    learning_rate /= 2
                    print('Decreased learning rate:', learning_rate)

            # Reborn phase
            print('Refining labels')
            train_lbls = []
            for x, y in zip(imgs, lbls):
                new_labels = sess.run(pred, feed_dict={inp: x})

                # apply softmax
                e_x = np.exp(
                    np.clip(
                        new_labels - np.median(new_labels, axis=1)[:, None],
                        -50, 50))
                new_labels = e_x / e_x.sum(axis=1)[:, None]
                diffs = np.argmax(new_labels, axis=1) == np.argmax(y, axis=1)
                diffs = diffs[:, None]
                train_lbls.append(
                    diffs * new_labels + np.logical_not(diffs) * y)


if __name__ == '__main__':
    main()
