import os
import gzip
import numpy as np
import zipfile
import urllib.request


def download_to_file(url, path):
    """
    Downloads url to path if no file exists at path already
    """
    if not path.exists():
        print(f'Downloading {url} to {path}')
        path.absolute().parent.mkdir(parents=True, exist_ok=True)
        urllib.request.urlretrieve(url, path)


def load_fashion_mnist(path, kind="train"):
    """Load MNIST data from `path`"""
    labels_path = path / ("%s-labels-idx1-ubyte.gz" % kind)
    images_path = path / ("%s-images-idx3-ubyte.gz" % kind)

    if not images_path.exists():
        print("Downloading images...")
        images_path.absolute().parent.mkdir(parents=True, exist_ok=True)
        urllib.request.urlretrieve(
            "http://fashion-mnist.s3-website.eu-central-1.amazonaws.com/train-images-idx3-ubyte.gz",
            images_path,
        )
    if not labels_path.exists():
        print("Downloading labels...")
        labels_path.absolute().parent.mkdir(parents=True, exist_ok=True)
        urllib.request.urlretrieve(
            "http://fashion-mnist.s3-website.eu-central-1.amazonaws.com/train-labels-idx1-ubyte.gz",
            labels_path,
        )

    with gzip.open(labels_path, "rb") as lbpath:
        labels = np.frombuffer(lbpath.read(), dtype=np.uint8, offset=8)

    with gzip.open(images_path, "rb") as imgpath:
        images = np.frombuffer(imgpath.read(), dtype=np.uint8, offset=16).reshape(
            len(labels), 784
        )

    return images, labels


def load_enwiki9(path, limit=-1):
    """
    Load enwiki9 dataset from zip file into memory as numpy uint8 array
    representing bytes

    Loads everything into memory

    References:
      - http://mattmahoney.net/dc/textdata.html
      - https://www.kaggle.com/datasets/jamesmcguigan/hutter-prize?resource=download
      - http://prize.hutter1.net/
    """
    download_to_file("http://mattmahoney.net/dc/enwik9.zip", path)
    with zipfile.ZipFile(path, "r") as archive:
        with archive.open("enwik9", 'r') as f:
            return np.frombuffer(f.read(limit), dtype=np.uint8)
