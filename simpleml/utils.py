import unittest
from typing import Generic, TypeVar

import numpy as np
import torch
from torch.nn import functional as F
import time
from collections import namedtuple

T = TypeVar("T")


def flatten(tensor: Generic[T]) -> T:
    """Flattens last 3 dimensions of tensor"""
    assert isinstance(tensor, torch.Tensor)
    shape = tensor.size()
    end_dim_size = np.prod(shape[-3:])
    return tensor.view(*shape[:-3], end_dim_size)


def sample_from_logits(logits):
    if isinstance(logits, torch.Tensor):
        probs = F.softmax(logits, dim=0).numpy()
        return np.random.choice(logits.size()[0], p=probs)
    raise ValueError("Invalid logit type supported")


class TestUtils(unittest.TestCase):
    def testFlatten(self):
        sample_shapes = [
            ((123, 24, 24, 3), (123, 24 * 24 * 3)),
            ((5, 6, 7, 7, 7, 2), (5, 6, 7, 7 * 7 * 2)),
        ]
        for s1, s2 in sample_shapes:
            sample_arr = torch.zeros(*s1)
            res = flatten(sample_arr)
            self.assertTupleEqual(s2, tuple(res.size()), msg="Shapes not equal")


Event = namedtuple("Event", ["time", "label"])


class Stopwatch:
    def __init__(self):
        self.timestamps = []

    def lap(self, label=None):
        label_with_default = label
        if label is None:
            label_with_default = f"t{len(self.timestamps)}"

        self.timestamps.append(Event(time=time.time(), label=label_with_default))

    def print_debug(self):
        total_time = self.timestamps[-1].time - self.timestamps[0].time
        for i in range(len(self.timestamps) - 1):
            t_cur = self.timestamps[i]
            t_next = self.timestamps[i + 1]
            print(
                'Time from "{}" to "{}": {:.2f} {}'.format(
                    t_cur.label,
                    t_next.label,
                    (t_next.time - t_cur.time) / total_time,
                    t_next.time - t_cur.time,
                )
            )
