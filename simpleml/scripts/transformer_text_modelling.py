"""
Run transformer on some basic code

Copied from transformer_shuffle_experiements
"""
import random
import collections
from dataclasses import dataclass
from typing import List, Optional
from simpleml.supervised.transformer import GptTransformer
from torch.nn import functional as F
import torch
import numpy as np
import matplotlib.pyplot as plt
import string
from pathlib import Path
from tqdm import tqdm
from simpleml import stat_logging as log
import time

DATA_PATH = Path("./data/tale-of-two-cities.txt")
CHARS = string.printable
SAVE_FILE = Path("./data/totc_transformer_save.pkl")


def load_batched_data(seq_len, batch_size):
    """
    Returns
        A (total batches, batch size, time) dimention array representing the
        batched data
    """
    with DATA_PATH.open() as f:
        contents = f.read()
    seq = np.zeros((len(contents),), dtype=np.int32)
    pos = 0
    for c in contents:
        ind = CHARS.find(c)
        if ind < 0:
            continue
        seq[pos] = ind
        pos += 1
    batch_elements = seq_len * batch_size
    num_batches = pos // batch_elements
    elements_to_use = num_batches * batch_elements
    return seq[:elements_to_use].reshape((num_batches, batch_size, seq_len))


def main():
    NUM_ITER = 10
    BATCH_SIZE = 32
    TRUNCATE_DATA_RATIO = 1
    SEQ_LENGTH = 64
    LR = 1e-3
    GPT_PARAMS = {}
    # None means no schedule
    LR_SCHEDULE = None
    DEVICE = torch.device("cuda")

    data = load_batched_data(SEQ_LENGTH, BATCH_SIZE)
    data = data[: int(TRUNCATE_DATA_RATIO * data.shape[0])]
    rng = np.random.default_rng()
    rng.shuffle(data)

    cross_entropy_history = []
    with DEVICE:
        transformer = GptTransformer(len(CHARS), len(CHARS), **GPT_PARAMS)
        optim = torch.optim.Adam(transformer.parameters(), LR)

        # if SAVE_FILE.exists():
            # print("loading!!")
            # state_dict = torch.load(SAVE_FILE)
            # transformer.load_state_dict(state_dict["model"])
            # optim.load_state_dict(state_dict["optim"])

        def set_lr(lr):
            for g in optim.param_groups:
                g["lr"] = lr

        for i in range(NUM_ITER):
            for batch_ind in tqdm(range(data.shape[0])):
                log.advance_iter()
                if LR_SCHEDULE is not None:
                    set_lr(LR_SCHEDULE(i))

                data_tensor = F.one_hot(
                    torch.tensor(data[batch_ind, :, :], dtype=torch.int64),
                    num_classes=len(CHARS),
                )
                shifted_input = data_tensor[:, :-1, :]
                expected_output = data_tensor[:, 1:, :]

                optim.zero_grad()
                predictions = transformer(shifted_input.type(torch.float32))

                cross_entropy = F.cross_entropy(
                    torch.transpose(predictions, 1, 2),
                    torch.transpose(expected_output.type(torch.float32), 1, 2),
                    reduction="none",
                )
                average_cross_entropy = cross_entropy.mean()
                cross_entropy_history.append(average_cross_entropy.item())
                average_cross_entropy.backward()
                optim.step()

            state_dict = {
                "model": transformer.state_dict(),
                "optim": optim.state_dict(),
            }
            torch.save(state_dict, SAVE_FILE)
    plt.plot(cross_entropy_history)
    plt.show()


if __name__ == "__main__":
    main()
