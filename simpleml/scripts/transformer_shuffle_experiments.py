"""
Compares LSTM with Transformer on a variety of subtasks
"""
import random
import collections
from dataclasses import dataclass
from typing import List, Optional
from simpleml.supervised.transformer import GptTransformer
from torch.nn import functional as F
import torch
import numpy as np
import matplotlib.pyplot as plt

CHARS = "0123456789x=."


@dataclass
class DataSample:
    inp: List
    out: List


@dataclass
class Dataset:
    data: List
    charset: List
    endchar: Optional[int]


def generate_multiplication_data(num_samples=1000, integer_length=5) -> Dataset:
    samples = []
    chars = "0123456789x=."
    for _ in range(num_samples):
        a = random.randint(1, 10**integer_length - 1)
        b = random.randint(1, 10**integer_length - 1)
        samples.append(DataSample(inp=f"{a}x{b}=", out=f"{a}x{b}={a*b}."))
    return Dataset(data=samples, charset=chars, endchar=".")


def generate_shuffle_data(num_samples=1000, sample_length=10) -> Dataset:
    samples = []
    for _ in range(num_samples):
        seq = list(range(sample_length))
        random.shuffle(seq)
        samples.append(seq)
    return Dataset(data=samples, charset=list(range(sample_length)), endchar=None)


def expected_random_loss(length):
    return np.log(length)


def expected_optimal_loss(length):
    return sum(np.log(i + 1) for i in range(length - 1)) / (length - 1)

def create_step_function_lr(lr_cutoffs, default):
    def _func(i):
        for (ind, lr) in lr_cutoffs[::-1]:
            if i >= ind:
                return lr
        return default
    return _func


def main():
    LENGTH = 10
    NUM_ITER = 1000
    BATCH_SIZE = 64
    LR = 1e-3
    GPT_PARAMS = {
            }
    # None means no schedule
    LR_SCHEDULE = None


    best_case_loss = expected_optimal_loss(LENGTH)
    worst_case_loss = expected_random_loss(LENGTH)
    print(f"Worst case loss: {expected_random_loss(LENGTH)}")
    print(f"Best case loss: {expected_optimal_loss(LENGTH)}")
    cross_entropy_history = []
    lowest_loss = float('inf')
    with torch.device("cuda"):
        transformer = GptTransformer(LENGTH, LENGTH, **GPT_PARAMS)
        optim = torch.optim.Adam(transformer.parameters(), LR)

        def set_lr(lr):
            for g in optim.param_groups:
                g['lr'] = lr


        for i in range(NUM_ITER):
            if LR_SCHEDULE is not None:
                set_lr(LR_SCHEDULE(i))

            raw_shuf_data = generate_shuffle_data(
                num_samples=BATCH_SIZE, sample_length=LENGTH
            )
            shuf_tensor = F.one_hot(torch.tensor(raw_shuf_data.data, dtype=torch.int64))
            shifted_input = shuf_tensor[:, :-1, :]
            expected_output = shuf_tensor[:, 1:, :]

            optim.zero_grad()
            predictions = transformer(shifted_input.type(torch.float32))

            cross_entropy = F.cross_entropy(
                torch.transpose(predictions, 1, 2),
                torch.transpose(expected_output.type(torch.float32), 1, 2),
                reduction="none",
            )
            average_cross_entropy = cross_entropy.mean()
            cross_entropy_history.append(average_cross_entropy.item())
            average_cross_entropy.backward()
            optim.step()

            lowest_loss = min(lowest_loss, average_cross_entropy.item())
            print(f"Cross entropy: {average_cross_entropy.item()}")
            if i == NUM_ITER - 1:
                with torch.no_grad():
                    print('Lowest cross entropy', lowest_loss)
                    print("Actual cross entropy", cross_entropy[0].cpu().numpy())
                    print(
                        "Best case cross entropy",
                        [np.log(i) for i in range(LENGTH - 1, 0, -1)],
                    )
    plt.plot(cross_entropy_history)
    plt.axhline(y=worst_case_loss, color='red')
    plt.axhline(y=best_case_loss, color='red')
    plt.show()


if __name__ == "__main__":
    main()
