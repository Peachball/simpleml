from simpleml.supervised.transformer import GptTransformer
from .transformer_text_modelling import CHARS
import torch
from torch.nn import functional as F
import time
from pathlib import Path
import numpy as np
from tqdm import tqdm
import matplotlib.pyplot as plt

# Uses models with same format as those saved by
# simpleml.scripts.transformer_text_modelling
MODEL_FILE = Path("./data/totc_transformer_save.pkl")


def sample_random(model, length=500, generation_strategy="stateful", starting_char="a"):
    starting_char_ind = CHARS.find(starting_char)
    with torch.no_grad():
        if generation_strategy == "stateful":
            sequence = [starting_char]
            starting_char = torch.tensor([[starting_char_ind]], dtype=torch.int64)
            x = F.one_hot(starting_char, num_classes=len(CHARS))
            state = None
            for i in range(length):
                logits, state = model.generate(
                    x.type(torch.float32), state, starting_position=i + 1
                )
                # logits dimension: (N, 1, output_dim)
                probs = F.softmax(logits, dim=-1)
                next_char = torch.multinomial(probs[0, 0], 1)
                sequence.append(CHARS[next_char[0].item()])
                x = F.one_hot(next_char[None, :], num_classes=len(CHARS))
            return "".join(sequence)
        elif generation_strategy == "brute_force":
            sequence = [starting_char]
            x = torch.zeros(1, length + 1, len(CHARS), dtype=torch.float32)
            x[0, 0, starting_char_ind] = 1
            for i in range(length):
                logits = model(x[: i + 1, :, :])
                # logits dimension: (N, 1, output_dim)
                probs = F.softmax(logits, dim=-1)
                next_char = torch.multinomial(probs[0, 0], 1)
                x[0, i + 1, next_char] = 1
                sequence.append(CHARS[next_char[0].item()])
            return "".join(sequence)
        else:
            raise ValueError(f"Unrecognized generation strategy: {generation_strategy}")


def main():
    DEVICE = torch.device("cuda")

    with DEVICE:
        transformer = GptTransformer(len(CHARS), len(CHARS))

        state_dict = torch.load(MODEL_FILE)
        transformer.load_state_dict(state_dict["model"])

        brute_force_times = []
        stateful_times = []
        all_seqlen = np.logspace(1, 4, num=15)
        for raw_seqlen in all_seqlen:
            seqlen = int(raw_seqlen)
            print(seqlen)
            # brute_force_generation_start = time.time()
            # brute_force_output = sample_random(
            # transformer, length=seqlen, generation_strategy="brute_force"
            # )
            # print('BRUTE FORCE')
            # print(brute_force_output)
            # brute_force_generation_end = time.time()
            # brute_force_times.append(
            # brute_force_generation_end - brute_force_generation_start
            # )

            stateful_generation_start = time.time()
            stateful_output = sample_random(
                transformer, length=seqlen, generation_strategy="stateful"
            )
            print("STATEFUL")
            print(stateful_output)
            stateful_generation_end = time.time()
            stateful_times.append(stateful_generation_end - stateful_generation_start)
        # plt.plot(all_seqlen, stateful_times, marker="o", label="stateful")
        plt.plot(
            all_seqlen,
            [t / s for s, t in zip(all_seqlen, stateful_times)],
            label="time per token",
        )
        # plt.plot(all_seqlen, brute_force_times, marker="o", label="brute force")
        plt.legend()
        plt.show()


if __name__ == "__main__":
    main()
