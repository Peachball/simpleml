"""
Script to help visualize population based training progress (from simpleml.pbt)
"""
import argparse
import collections
from pathlib import Path
import matplotlib.pyplot as plt
import json


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "savedir",
        type=Path,
        help="Directory passed as savedir to population based trainier",
    )
    args = parser.parse_args()
    history_file = args.savedir / 'history.json'
    with history_file.open() as f:
        history = json.load(f)
    def plot_stats(xkey, ykey):
        data = [(h[xkey], ykey(h)) for h in history]
        plt.scatter(*zip(*data), alpha=0.1)
    plot_stats('step', lambda h: h['metric'])
    plt.show()
    for i in range(2):
        plt.subplot(3, 3, i+1)
        plot_stats('step', lambda h: h['hyperparams'][f'lr{i}'])
        plt.yscale('log')
        plt.gca().set_title(f'lr{i}')
    plt.subplot(3, 3, 6)
    plot_stats('step', lambda h: h['hyperparams']['seq_len'])
    plt.yscale('log')
    plt.gca().set_title('seq_len')
    plt.subplot(3, 3, 7)
    plot_stats('step', lambda h: h['hyperparams']['batch_size'])
    plt.gca().set_title('batch_size')
    grouped_by_step = collections.defaultdict(list)
    for h in history:
        grouped_by_step[h['step']].append(h)
    num_normalized_per_step = [
            (k, sum([e['hyperparams']['normalize_grad'] for e in v]))
            for k, v in grouped_by_step.items()]
    plt.subplot(3, 3, 8)
    plt.plot(*zip(*num_normalized_per_step))
    plt.gca().set_title('num normalized per step')
    plt.subplot(3, 3, 9)
    plot_stats('step', lambda h: h['hyperparams']['fc'])
    plt.yscale('log')
    plt.gca().set_title('fc')
    plt.show()
    max_vals = [(k, max([e['metric'] for e in v])) for k, v in
            grouped_by_step.items()]
    plt.plot(*zip(*max_vals))
    plt.title('Max values')
    plt.show()

    # view best hyperparameters at each step
    best_data = [(k, max(v, key=lambda e: e['metric'])['hyperparams']['fc']) for k, v in
            grouped_by_step.items()]
    plt.plot(*zip(*best_data))
    plt.title('Best fc value at each step')
    plt.yscale('log')
    plt.show()

    # View hyperparams, coloring the best to worst
    points = []
    colors = []
    for k, v in grouped_by_step.items():
        sorted_values = sorted(v, key=lambda e: e['metric'])
        points.extend([(k, s['hyperparams']['fc']) for s in sorted_values])
        colors.extend([x/len(sorted_values) for x in range(len(sorted_values))])
    plt.scatter(*zip(*points), cmap='viridis', c=colors)
    plt.title('Colored fc')
    plt.yscale('log')
    plt.show()

    # Print best hyperparameters at each step
    best_params = sorted([(k, max(v, key=lambda e: e['metric'])) for k, v in
        grouped_by_step.items()], key=lambda e: e[0])
    for e in best_params:
        print(e)


if __name__ == "__main__":
    main()
