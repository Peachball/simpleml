Experiment Summary Guide
========================

The overall structure of an experiment should be the following:

What
   Describe what is the new idea

Inspiration
   Why did I want to do this?

Existing Papers/Results
   Has this idea already been done before? How successful was it?

Results
   This section is free form, as every experiment is likely different
