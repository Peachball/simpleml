UORO
====
Unbiased Online Recurrent Optimization

What
----
Optimizing RNN's is hard, and it would be much nicer if there were some way to
optimize them in such a way such that the entire history could be used. This
way, longer dependencies could be learned.

Inspiration
-----------
Currently, RNN's are typically optimized thorugh BPTT, and the only online way
to optimize them is through Real Time Recurrent Learning (RTRL), which is
extremely memory inefficient. Being able to approximate RTRL with a low rank
approximation is great, as the overal optimization will still be unbiased

Existing Paper
--------------
https://arxiv.org/abs/1702.05043

Results
-------
It's been a long time since I've worked with the code. What I remember is that
the network seemed to converge on two different timescales.
