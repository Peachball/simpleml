Using Inverse Dynamics to Learn World Model
===========================================

What
----
Maybe it would be possible to completely train a RL agent on a world model of an
environment instead of the actual environment. This would drastically improve
sample efficiency, and even speed, depending on how fast the actual environment
is. It also generalizes RL to more domains, as likely more domains can be
simulated.

The overall workflow will be as follows

.. digraph:: workflow

   "State" -> "Visual Encoder" -> "Encoder LSTM" -> "state rep";
   "state rep" -> "next action";
   "state rep" -> "next state";
   "state rep" -> "current state";

Inspiration
-----------
Seeing AlphaZero made me realize that being able to imagine different
trajectories with MCTS would be super beneficial. However, currently MCTS isn't
practical with most RL environments, thus requiring a powerful and somewhat
accurate simulator.

Additionally, the main reason why not all experiences can be used is that
eventually, the experiences are outdated. Experiences too far in that past
aren't useful for policy optimization, due to how the policy changes over time.
However, they can still be used to improve the model of an environment.

Existing Papers
---------------
Model-based rl with a couple of cool tips:
   https://bair.berkeley.edu/blog/2017/11/30/model-based-rl/

World Models for faster RL
   https://arxiv.org/pdf/1803.10122.pdf

Curiosity Driven RL using Inverse Dynamics
   https://pathak22.github.io/noreward-rl/resources/icml17.pdf

The closest paper was model-based rl from Berkley, but they didn't use inverse
dynamics, and their control algorithm was completely based off the train model
of the environment (which I don't think is necessary).

Results
-------
